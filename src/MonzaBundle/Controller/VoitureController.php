<?php

namespace MonzaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MonzaBundle\Entity\brand;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use MonzaBundle\Entity\circuit;

class VoitureController extends Controller
{
     public function brandsAction(Request $request)
    {

# php bin/console doctrine:generate:entity
# php bin/console doctrine:generate:entities AcmeMyBundle:Customer
# php bin/console doctrine:schema:update --force

        $brands = $this->getDoctrine()
        ->getRepository(brand::class)
        ->findAll();
        if (!$brands) {
            throw $this->createNotFoundException(
                'No products found'
            );
        }

       // $repository = $this->getDoctrine()
       //  ->getRepository(brand::class);

       //  $query = $repository->createQueryBuilder('p')
       //  ->select('DISTINCT p.brandName')
       //  ->getQuery();

       //  $filteredBrands = $query->getResult();

        $logger = $this->get('logger');

        $filteredBrands = array();
        foreach ($brands as $value) {    

             //$logger->info('test2 ' . $value->getId());
            if(!in_array($value->getBrandName(), $filteredBrands))
            {
                array_push($filteredBrands, $value->getBrandName());
            }
        }

          foreach ($filteredBrands as $value) { 
            $logger->info('test ' . $value);
            $logger->info('test2 ' . $value);
          }



        $circuitId = $request->query->get('circuitId');

        // objet circuit qui contient toutes les informations du circuits récupéré en bdd
        $circuit = $this->getDoctrine()
        ->getRepository(circuit::class)
        ->find($circuitId);
        
        // $url = $this->get('router')->generate('voitures_global', [
        //     'brandId' => $brandId,
        //     'circuitId' => $circuitId
        // ]);
        // /brands?circuitId=4

        if (!$circuit) {
            throw $this->createNotFoundException(
                'No product found for id '.$productId
            );
        }


        return $this->render('@Monza/Default/brands.html.twig', array('brands' => $brands, 'filteredBrands' => $filteredBrands,  'circuit' => $circuit));
    }

     public function brandAction($brandId, Request $request)
    {

        $brand = $this->getDoctrine()
        ->getRepository(brand::class)
        ->find($brandId);

        if (!$brand) {
            throw $this->createNotFoundException(
                'No product found for id '.$productId
            );
        }

        $circuitId = $request->query->get('circuitId');

        $circuit = $this->getDoctrine()
        ->getRepository(circuit::class)
        ->find($circuitId);

        return $this->render('@Monza/Default/brand.html.twig', array('brand' => $brand, 'circuit' => $circuit));
    }

    public function gallerieAction()
    {
        $brand = $this->getDoctrine()
        ->getRepository(brand::class)
        ->find($brandImage);

        if (!$brand) {
            throw $this->createNotFoundException(
                'No product found for id '.$productId
            );
        }
        return $this->render('@Monza/Default/brand.html.twig', array('brand' => $brand));
    }
}