<?php

namespace MonzaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MonzaBundle\Entity\circuit;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class CircuitsController extends Controller
{
    public function circuitsAction()
    {
    	$circuits = $this->getDoctrine()
        ->getRepository(circuit::class)
        ->findAll();

        if (!$circuits) {
            throw $this->createNotFoundException(
                'No products found'
            );
        }

        $logger = $this->get('logger');

        $filteredCircuits = array();
        foreach ($circuits as $value) {    
            if(!in_array($value->getCircuitPays(), $filteredCircuits))
            {
                array_push($filteredCircuits, $value->getCircuitPays());
            }
        }

          foreach ($filteredCircuits as $value) { 
            $logger->info('test ' . $value);
          }
        return $this->render('@Monza/Default/circuits.html.twig', array('circuits' => $circuits, 'filteredCircuits' => $filteredCircuits));
    }

     public function CircuitAction($circuitId)
    {
        $circuit = $this->getDoctrine()
        ->getRepository(circuit::class)
        ->find($circuitId);

        if (!$circuit) {
            throw $this->createNotFoundException(
                'No product found for id '.$productId
            );
        }

        return $this->render('@Monza/Default/circuit.html.twig', array('circuit' => $circuit));
    }
}