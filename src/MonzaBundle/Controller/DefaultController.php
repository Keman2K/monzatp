<?php

namespace MonzaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MonzaBundle\Form\RechercheType;
use Symfony\Component\HttpFoundation\Request;
use MonzaBundle\Entity\circuit;
use MonzaBundle\Entity\brand;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use MonzaBundle\Entity\Panier;
use MonzaBundle\Entity\LignePanier;
use Symfony\Component\HttpFoundation\RedirectResponse;


class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('@Monza/Default/index.html.twig');
    }

    public function EquipeAction()
    {
        return $this->render('@Monza/Default/equipe.html.twig');
    }

    public function FaqAction()
    {
    	return $this->render('@Monza/Default/faq.html.twig');
    }

    public function ServicesAction()
    {
    	return $this->render('@Monza/Default/services.html.twig');
    }

    public function ContactAction()
    {
    	return $this->render('@Monza/Default/contact.html.twig');
    }

    public function PanierAction(Request $request)
    { //utiliser KnpSnappyBundle pour éditer la facture en pdf et par mail.

        $brandId = $request->query->get('brandId');
        $circuitId = $request->query->get('circuitId');

         $user = $this->getUser();

        $entityManager = $this->getDoctrine()->getManager();

        if ($brandId != null && $circuitId != null){

            $brand = $this->getDoctrine()
            ->getRepository(brand::class)
            ->find($brandId); 

            //On récupère en Base l'enregistrement de la table brand dont la valeur de l'id est égal à BrandId.

            if (!$brand) {
                throw $this->createNotFoundException(
                    'No product found for id '.$brandId
                );
            }

            $circuit = $this->getDoctrine()
            ->getRepository(circuit::class)
            ->find($circuitId);

            //On récupère en Base l'enregistrement de la table Circuit dont la valeur de l'id est égal à CircuitId. 

            if (!$circuit) {
                throw $this->createNotFoundException(
                    'No product found for id '.$circuitId
                );
            }

        $panier = new Panier();
        $panier->setIdBrand($brandId);
        $panier->setIdCircuit($circuitId);
        $panier->setIdFOSUser($user->getId());

        // tells Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($panier);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();
        }

        // récupere la liste de tous les éléments du panier
        //Je récupère tous les enregistrements de la table panier qui ont pour idFOSUser l'id de l'utilisateur connecté.
        $query = $entityManager->createQuery('SELECT p FROM MonzaBundle\Entity\Panier p WHERE p.idFOSUser = ' . $user->getId());
        $tableauPanier = $query->getResult();// ça me retourne un array contenant toutes les lignes du panier de l'user connecté.

        $tableauLignePanier = array();
        $prixTotal = 0;

        foreach ($tableauPanier as $valuePanier) {

            $brand2 = $this->getDoctrine()
                ->getRepository(brand::class)
                ->find($valuePanier->getIdBrand());

            $prixTotal = $prixTotal + $brand2->getbrandPrix();

            $circuit2 = $this->getDoctrine()
            ->getRepository(circuit::class)
            ->find($valuePanier->getIdCircuit());

            $lignePanier = new LignePanier();
            $lignePanier->setId($valuePanier->getId());
            $lignePanier->setbrand($brand2);
            $lignePanier->setcircuit($circuit2);
            $lignePanier->setuser($user);

            array_push($tableauLignePanier, $lignePanier);
        }

        //return $this->render('@Monza/Default/panier.html.twig', array('brand' => $brand, 'circuit' => $circuit, 'user' => $user));
        return $this->render('@Monza/Default/panier.html.twig', array('tableauLignePanier' => $tableauLignePanier, 'prixTotal' => $prixTotal ));

    }

    public function DeleteAction($panierId)
    {

var_dump($panierId);
        $panier = $this->getDoctrine()
        ->getRepository(Panier::class)
        ->find($panierId);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($panier);
        $entityManager->flush();

        return new RedirectResponse($this->generateUrl('monza_13'));
    }

    public function CgvAction()
    {
        return $this->render('@Monza/Default/CGV.html.twig');
    }

    public function CompteAction()
    {
        return $this->render('@Monza/Default/compte.html.twig');
    }

    public function ConfirmationAction()
    {
        return $this->render('@Monza/Default/confirmation.html.twig');
    }

    public function AvisAction()
    {

        $avis = $this->getDoctrine()
        ->getRepository(avis::class)
        ->find($avisId);

        return $this->render('@Monza/Default/avis.html.twig', array('avis' => $avis));
    }

    public function NouvellesAction()
    {
        return $this->render('@Monza/Default/nouvelles.html.twig');
    }
    
    public function CommandesAction( Request $request)
    {
        $brandId = $request->query->get('brandId');
        $circuitId = $request->query->get('circuitId');
        $user = $this->getUser();

        $circuit = $this->getDoctrine()
        ->getRepository(circuit::class)
        ->find($circuitId);

        if (!$circuit) {
            throw $this->createNotFoundException(
                'No product found for id '.$productId
            );
        }

        $brand = $this->getDoctrine()
        ->getRepository(brand::class)
        ->find($brandId);

        if (!$brand) {
            throw $this->createNotFoundException(
                'No product found for id '.$productId
            );
        }

        $circuitId = $request->query->get('circuitId');

        $circuit = $this->getDoctrine()
        ->getRepository(circuit::class)
        ->find($circuitId);

        return $this->render('@Monza/Default/commandes.html.twig', array('circuits' => $circuits, 'brand' => $brand));
    }
    
    public function PlanAction()
    {
        return $this->render('@Monza/Default/plan.html.twig');
    }
}