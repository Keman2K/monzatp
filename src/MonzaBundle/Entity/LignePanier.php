<?php

namespace MonzaBundle\Entity;

use MonzaBundle\Entity\circuit;
use MonzaBundle\Entity\brand;

/**
 * Panier
 */
class LignePanier
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $user;

    /**
     * @var int
     */
    private $brand;

    /**
     * @var int
     */
    private $circuit;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

  /**
     * Set user
     *
     * @param integer $user
     *
     * @return Panier
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set user
     *
     * @param integer $user
     *
     * @return Panier
     */
    public function setuser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return int
     */
    public function getuser()
    {
        return $this->user;
    }

    /**
     * Set brand
     *
     * @param integer $brand
     *
     * @return Panier
     */
    public function setbrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return int
     */
    public function getbrand()
    {
        return $this->brand;
    }

    /**
     * Set circuit
     *
     * @param integer $circuit
     *
     * @return Panier
     */
    public function setcircuit($circuit)
    {
        $this->circuit = $circuit;

        return $this;
    }

    /**
     * Get circuit
     *
     * @return int
     */
    public function getcircuit()
    {
        return $this->circuit;
    }
}
