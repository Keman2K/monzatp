<?php

namespace MonzaBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * KemanUser
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="MonzaBundle\Repository\KemanUserRepository")
 */
class KemanUser extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
