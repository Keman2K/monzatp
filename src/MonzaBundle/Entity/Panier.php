<?php

namespace MonzaBundle\Entity;

/**
 * Panier
 */
class Panier
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $idFOSUser;

    /**
     * @var int
     */
    private $idBrand;

    /**
     * @var int
     */
    private $idCircuit;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idFOSUser
     *
     * @param integer $idFOSUser
     *
     * @return Panier
     */
    public function setIdFOSUser($idFOSUser)
    {
        $this->idFOSUser = $idFOSUser;

        return $this;
    }

    /**
     * Get idFOSUser
     *
     * @return int
     */
    public function getIdFOSUser()
    {
        return $this->idFOSUser;
    }

    /**
     * Set idBrand
     *
     * @param integer $idBrand
     *
     * @return Panier
     */
    public function setIdBrand($idBrand)
    {
        $this->idBrand = $idBrand;

        return $this;
    }

    /**
     * Get idBrand
     *
     * @return int
     */
    public function getIdBrand()
    {
        return $this->idBrand;
    }

    /**
     * Set idCircuit
     *
     * @param integer $idCircuit
     *
     * @return Panier
     */
    public function setIdCircuit($idCircuit)
    {
        $this->idCircuit = $idCircuit;

        return $this;
    }

    /**
     * Get idCircuit
     *
     * @return int
     */
    public function getIdCircuit()
    {
        return $this->idCircuit;
    }
}
