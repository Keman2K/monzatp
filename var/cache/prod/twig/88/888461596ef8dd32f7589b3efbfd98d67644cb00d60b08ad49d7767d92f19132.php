<?php

/* base.html.twig */
class __TwigTemplate_3a13319b594405d1cdc56cf9b289cf438c2e2d100ac4953c1d1d994204decf1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr\">
<head>
  <meta charset=\"utf-8\">
  <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
  ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 23
        echo "
";
        // line 24
        $this->displayBlock('header', $context, $blocks);
        // line 92
        $this->displayBlock('body', $context, $blocks);
        // line 95
        echo "
";
        // line 96
        $this->displayBlock('footer', $context, $blocks);
        // line 154
        echo "
";
        // line 155
        $this->displayBlock('javascripts', $context, $blocks);
        // line 173
        echo "    </body>
</html>
";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Bienvenue chez Keman Racing !";
    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 7
        echo "  <meta content=\"width=device-width, initial-scale=1.0\" name=\"viewport\">
  <meta content=\"\" name=\"keywords\">
  <meta content=\"\" name=\"description\">
  <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/img/logo.png"), "html", null, true);
        echo "\" rel=\"icon\">
  <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.6.3/css/all.css\" integrity=\"sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/\" crossorigin=\"anonymous\">
  <link href=\"https://fonts.googleapis.com/css?family=Ruda:400,900,700\" rel=\"stylesheet\">
  <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
  <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
  <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/prettyphoto/css/prettyphoto.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
  <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/hover/hoverex-all.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
  <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/jetmenu/jetmenu.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
  <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/owl-carousel/owl-carousel.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
  <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
  <link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/css/colors/blue.css"), "html", null, true);
        echo "\">
</head>
";
    }

    // line 24
    public function block_header($context, array $blocks = array())
    {
        // line 25
        echo "  <div class=\"topbar clearfix\">
    <div class=\"container\">
      <div class=\"col-lg-12 text-right\">
        <div class=\"social_buttons\">
          <a href=\"https://fr-fr.facebook.com/\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Facebook\"><i class=\"fa fa-facebook\"></i></a>
          <a href=\"https://twitter.com/?lang=fr\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Twitter\"><i class=\"fa fa-twitter\"></i></a>
          <a href=\"https://plus.google.com/discover\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Google+\"><i class=\"fa fa-google-plus\"></i></a>
          <a href=\"https://dribbble.com/\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Dribbble\"><i class=\"fa fa-dribbble\"></i></a>
          <a href=\"https://fr.wikipedia.org/wiki/RSS\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"RSS\"><i class=\"fa fa-rss\"></i></a>
        </div>
      </div>
    </div>
  </div>

  <header class=\"header\">
    <div class=\"container\">
      <div class=\"site-header clearfix\">
        <div class=\"col-lg-3 col-md-3 col-sm-12 title-area\">
          <div class=\"site-title\" id=\"title\">
            <a href=\"/\" title=\"\">
              <h4>Keman<span>Racing !</span></h4>
            </a>
          </div>
        </div>
        <div class=\"col-lg-9 col-md-12 col-sm-12\">
          <div id=\"nav\" class=\"right\">
            <div class=\"container clearfix\">
              <ul id=\"jetmenu\" class=\"jetmenu blue\">
                <li class=\"active\"><a href=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/"), "html", null, true);
        echo "\">Acceuil</a>
                </li>
                <li><a href=\"/\">Nous connaître</a>
                  <ul class=\"dropdown\">
                    <li><a href=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/qsn"), "html", null, true);
        echo "\">Qui sommes-nous ?</a></li>
                    <li><a href=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/services"), "html", null, true);
        echo "\">Nos services</a></li>
                    <li><a href=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/equipe"), "html", null, true);
        echo "\">Notre équipe</a></li>
                    <li><a href=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/faq"), "html", null, true);
        echo "\">FAQ</a></li>
                    <li><a href=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/contact"), "html", null, true);
        echo "\">Nous Contacter</a></li>
                    ";
        // line 62
        if ($this->getAttribute(($context["app"] ?? null), "user", array())) {
            // line 63
            echo "                    <li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/avis"), "html", null, true);
            echo "\">Déposer un avis</a></li>
                    ";
        }
        // line 65
        echo "                  </ul>
                </li>
                <li><a href=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/circuits"), "html", null, true);
        echo "\">Nos circuits</a></li>
                <li><a href=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/register"), "html", null, true);
        echo "\">Votre compte</a>
                  <ul class=\"dropdown\">
                    ";
        // line 70
        if ($this->getAttribute(($context["app"] ?? null), "user", array())) {
            // line 71
            echo "                    ";
        } elseif ( !$this->getAttribute(($context["app"] ?? null), "user", array())) {
            // line 72
            echo "                    <li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/login"), "html", null, true);
            echo "\">Connexion</a></li>
                    <li><a href=\"";
            // line 73
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/register"), "html", null, true);
            echo "\">Inscription</a></li>
                    ";
        }
        // line 75
        echo "                    ";
        if ($this->getAttribute(($context["app"] ?? null), "user", array())) {
            // line 76
            echo "                      <li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/panier"), "html", null, true);
            echo "\">Votre panier</a></li>
                      <li><a href=\"";
            // line 77
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/commandes"), "html", null, true);
            echo "\">Mes commandes</a></li>
                      <li><a href=\"";
            // line 78
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/compte"), "html", null, true);
            echo "\">Mon compte</a></li>
                      <li><a href=\"";
            // line 79
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/logout"), "html", null, true);
            echo "\">Deconnexion</a></li>
                    ";
        } elseif ( !$this->getAttribute(        // line 80
($context["app"] ?? null), "user", array())) {
            // line 81
            echo "                    ";
        }
        // line 82
        echo "                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
";
    }

    // line 92
    public function block_body($context, array $blocks = array())
    {
        // line 93
        echo "
";
    }

    // line 96
    public function block_footer($context, array $blocks = array())
    {
        // line 97
        echo "  <footer class=\"footer\">
    <div class=\"container\">
      <div class=\"widget col-lg-3 col-md-3 col-sm-12\">
        <h4 class=\"title\">A propos de nous</h4>
        <p>Keman Racing ! est une entreprise fictive dans le but d'un projet noté. Rien de tout ça est vrai !</p>
      </div>
      <div class=\"widget col-lg-3 col-md-3 col-sm-12\">
        <h4 class=\"title\">Dernières nouveautés</h4>
        <ul class=\"recent_posts\">
          <li>
            <a href=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/home1"), "html", null, true);
        echo "\">
            <img src=\"";
        // line 108
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/recent_post_01.png"), "html", null, true);
        echo "\" alt=\"mobile\" /> Bientôt une nouvelle application mobile !</a>
            <a class=\"readmore\" href=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/#"), "html", null, true);
        echo "\">En savoir plus</a>
          </li>
          <li>
            <a href=\"";
        // line 112
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/home1"), "html", null, true);
        echo "#\">
            <img src=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/recent_post_02.png"), "html", null, true);
        echo "\" alt=\"Nouvelles\" /> Bientôt de nouveaux modèles !</a>
            <a class=\"readmore\" href=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/nouvelles"), "html", null, true);
        echo "\">En savoir plus</a>
          </li>
        </ul>
      </div>
      <div class=\"widget col-lg-3 col-md-3 col-sm-12\">
        <h4 class=\"title\"> Restons en contact</h4>
        <ul class=\"contact_details\">
          <li><i class=\"fa fa-envelope-o\"></i> info@kemanracing.com</li>
          <li><i class=\"fa fa-phone-square\"></i> 01 12 23 45 56</li>
          <li><i class=\"fa fa-home\"></i> Siège social, 2 rue Jean Macé, 78360 Montesson</li>
        </ul>
      </div>
      <div class=\"widget col-lg-3 col-md-3 col-sm-12\">
        <h4 class=\"title\">Nous trouver</h4>
<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d655.5617232372005!2d2.1535200292831567!3d48.91068819870577!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e663b1dbf34669%3A0x68a58f40089ea408!2s2+Rue+Jean+Mac%C3%A9%2C+78360+Montesson!5e0!3m2!1sfr!2sfr!4v1549376809503\" width=\"250\" height=\"200\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
      </div>
    </div>
    <div class=\"copyrights\">
      <div class=\"container\">
        <div class=\"col-lg-6 col-md-6 col-sm-12 columns footer-left\">
          <p>Copyright © 2018 - Tous droits réservés.</p>
          <div class=\"credits\">
            Crée pour un examen à l'<a href=\"https://www.ifocop.fr/\" target=\"blank\">IFOCOP</a>, PARIS.
          </div>
        </div>
        <div class=\"col-lg-6 col-md-6 col-sm-12 columns text-right\">
          <div class=\"footer-menu right\">
            <ul class=\"menu\">
              <li><a href=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/"), "html", null, true);
        echo "\">Acceuil</a></li>
              <li><a href=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/qsn"), "html", null, true);
        echo "\">A propos</a></li>
              <li><a href=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/plan"), "html", null, true);
        echo "\">Plan du site</a></li>
              <li><a href=\"";
        // line 145
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/CGV"), "html", null, true);
        echo "\">CGV</a></li>
              <li><a href=\"";
        // line 146
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/contact"), "html", null, true);
        echo "\">Contact</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
";
    }

    // line 155
    public function block_javascripts($context, array $blocks = array())
    {
        // line 156
        echo "  <div class=\"dmtop\">Scroll to Top</div>
  <script src=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/jquery/jquery.min.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 158
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/php-mail-form/validate.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/prettyphoto/js/prettyphoto.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 161
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/isotope/isotope.min.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 162
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/hover/hoverdir.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/hover/hoverex.min.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 164
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/unveil-effects/unveil-effects.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/owl-carousel/owl-carousel.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 166
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/jetmenu/jetmenu.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/animate-enhanced/animate-enhanced.min.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/jigowatt/jigowatt.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/easypiechart/easypiechart.min.js"), "html", null, true);
        echo "\"></script>

  <script src=\"";
        // line 171
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/js/main.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  412 => 171,  407 => 169,  403 => 168,  399 => 167,  395 => 166,  391 => 165,  387 => 164,  383 => 163,  379 => 162,  375 => 161,  371 => 160,  367 => 159,  363 => 158,  359 => 157,  356 => 156,  353 => 155,  341 => 146,  337 => 145,  333 => 144,  329 => 143,  325 => 142,  294 => 114,  290 => 113,  286 => 112,  280 => 109,  276 => 108,  272 => 107,  260 => 97,  257 => 96,  252 => 93,  249 => 92,  236 => 82,  233 => 81,  231 => 80,  227 => 79,  223 => 78,  219 => 77,  214 => 76,  211 => 75,  206 => 73,  201 => 72,  198 => 71,  196 => 70,  191 => 68,  187 => 67,  183 => 65,  177 => 63,  175 => 62,  171 => 61,  167 => 60,  163 => 59,  159 => 58,  155 => 57,  148 => 53,  118 => 25,  115 => 24,  108 => 20,  104 => 19,  100 => 18,  96 => 17,  92 => 16,  88 => 15,  84 => 14,  80 => 13,  74 => 10,  69 => 7,  66 => 6,  60 => 5,  54 => 173,  52 => 155,  49 => 154,  47 => 96,  44 => 95,  42 => 92,  40 => 24,  37 => 23,  35 => 6,  31 => 5,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base.html.twig", "C:\\Monza\\app\\Resources\\views\\base.html.twig");
    }
}
