<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_b305e281ed543a459d156fe844bef65cf9e9b4966d99ac038f80397b365c008c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " Keman Racing";
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "
  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Inscription</li>
        </ul>
        <h2>INSCRIPTION</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>
  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
        <div class=\"col-lg-4 col-md-4 col-sm-12\">
          <h4 class=\"title\">
             <span>Pourquoi s'inscrire ?</span>
          </h4>
          <p>Pour pouvoir choisir votre voiture ou le circuit sur lequel vous voulez rouler, il faut vous s'inscrire, mais ce n'est pas tout ! En vous inscrivant chez nous, vous pourrez bénéficier de nombreuses réduction sur l'ensemble de nos produits en boutique !</p>
          <p>Alors, qu'attendez-vous ? Ça ne prends que quelques minutes et le tour est joué !</p>
        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-12\">
          <h4 class=\"title\">
             <span>Avantages</span>
          </h4>
          <ul class=\"check\">
            <li>Choix multiples de voitures</li>
            <li>Des réductions sur notre boutique</li>
            <li>Inscription à la Newsletter </li>
            <li>Soirées privées pour tenter de gagner des prix !</li>
          </ul>
        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-12\">
          <h4 class=\"title\">
             <span>Je m'enregistre</span>
          </h4>

\t\t\t\t";
        // line 53
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 56
        echo "
        </div>
      </div>
    </div>
  </section>

";
    }

    // line 53
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 54
        echo "\t\t\t\t    ";
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 54)->display($context);
        // line 55
        echo "\t\t\t\t";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 55,  104 => 54,  101 => 53,  91 => 56,  89 => 53,  39 => 5,  36 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Registration:register.html.twig", "C:\\Monza\\app/Resources/FOSUserBundle/views/Registration/register.html.twig");
    }
}
