<?php

/* @Monza/Default/brand.html.twig */
class __TwigTemplate_cff4cb237a0ae3949b40197cd801b3c0fd6613048629be53febc0526ac25207d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@Monza/Default/brand.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>";
        // line 9
        echo $this->getAttribute(($context["brand"] ?? null), "getBrandName", array(), "method");
        echo " ";
        echo $this->getAttribute(($context["brand"] ?? null), "getBrandModel", array(), "method");
        echo "</li>
        </ul>
        <h2>";
        // line 11
        echo $this->getAttribute(($context["brand"] ?? null), "getBrandName", array(), "method");
        echo " ";
        echo $this->getAttribute(($context["brand"] ?? null), "getBrandModel", array(), "method");
        echo ", vous avez choisi le circuit ";
        echo $this->getAttribute(($context["circuit"] ?? null), "getCircuitName", array(), "method");
        echo "</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>
    <section class=\"marketplace-top\">
    <div id=\"market-wrapper\">
      <div class=\"item_image\">
         <img data-effect=\"fade\" class=\"aligncenter\" width=\"600\" height=\"400\" src=\"";
        // line 28
        echo $this->getAttribute(($context["brand"] ?? null), "getbrandImage", array());
        echo "\">
      </div>
    </div>
  </section>
  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
        <div class=\"general-title text-center\">
          <h3> ";
        // line 36
        echo $this->getAttribute(($context["brand"] ?? null), "getBrandName", array(), "method");
        echo " ";
        echo $this->getAttribute(($context["brand"] ?? null), "getbrandModel", array(), "method");
        echo "</h3>
          <p>";
        // line 37
        echo $this->getAttribute(($context["brand"] ?? null), "getBrandTitle", array());
        echo " </p>
          <hr>
        </div>
        <div class=\"divider\"></div>
        <div class=\"item_details\">
          <div class=\"col-lg-3 col-md-3 col-sm-12\">
            <div class=\"theme_details\">
              <div class=\"details_section\">
                <h3>En détails : </h3>
                <ul>
                  <li class=\"version\"> Puissance : <span>";
        // line 47
        echo $this->getAttribute(($context["brand"] ?? null), "getbrandPower", array(), "method");
        echo "</span></li>
                  <li class=\"designer\"> 0 - 100 : <span>";
        // line 48
        echo $this->getAttribute(($context["brand"] ?? null), "getbrandChrono", array(), "method");
        echo "</span></li>
                  <li class=\"release\"> Vitesse Max : <span>";
        // line 49
        echo $this->getAttribute(($context["brand"] ?? null), "getbrandVmax", array(), "method");
        echo "</span></li>
                </ul>
              </div>
            </div>
          </div>
      <div class=\"col-lg-6 col-md-6 col-sm-12\">
        <div class=\"theme_details\">
          <div class=\"item-description\">
            ";
        // line 57
        echo $this->getAttribute(($context["brand"] ?? null), "getBrandHistory", array());
        echo "
          </div>
        </div>
      </div>
      <div class=\"col-lg-3 col-md-3 col-sm-12\">
        <div class=\"theme_details\">
          <div class=\"item_price\">
            <h4>Son prix est de ";
        // line 64
        echo $this->getAttribute(($context["brand"] ?? null), "getBrandPrix", array());
        echo " €</h4>
          </div>
              <hr>
              <div class=\"buttons\">
                <a class=\"button btn-block large\" href=\"/panier?circuitId=";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute(($context["circuit"] ?? null), "getId", array(), "method"), "html", null, true);
        echo "&brandId=";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["brand"] ?? null), "getId", array(), "method"), "html", null, true);
        echo "\"></i>Continuer vers le panier</a>
              </div>
              <hr>
            </div>
          </div>
        </div>
    <div class=\"clearfix\"></div>
</div>
</div>
</section>
  <div class=\"item\">
    <div id=\"owl-demo\" class=\"owl-carousel\">

  ";
        // line 81
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 9));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 82
            echo "  <a data-rel=\"prettyPhoto\" href=\"";
            echo twig_escape_filter($this->env, (($this->getAttribute(($context["brand"] ?? null), "getBrandImages", array()) . $context["i"]) . ".jpg"), "html", null, true);
            echo "\">
    <div class=\"item\">
        <img src=\"";
            // line 84
            echo twig_escape_filter($this->env, (($this->getAttribute(($context["brand"] ?? null), "getBrandImages", array()) . $context["i"]) . ".jpg"), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["brand"] ?? null), "getBrandModel", array()), "html", null, true);
            echo "\">
    </div>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 87
        echo "      </a>
  </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "@Monza/Default/brand.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  174 => 87,  163 => 84,  157 => 82,  153 => 81,  135 => 68,  128 => 64,  118 => 57,  107 => 49,  103 => 48,  99 => 47,  86 => 37,  80 => 36,  69 => 28,  45 => 11,  38 => 9,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Monza/Default/brand.html.twig", "C:\\Monza\\src\\MonzaBundle\\Resources\\views\\Default\\brand.html.twig");
    }
}
