<?php

/* @Monza/Default/daniel.html.twig */
class __TwigTemplate_16438b248ce4c62ab1bb2baa88a8e41f1eddd00c6998d1d9b1decd409877c5ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@Monza/Default/daniel.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "<h1>Voila la variable : ";
        echo twig_escape_filter($this->env, ($context["circuitsId"] ?? null), "html", null, true);
        echo "</h1>


";
    }

    public function getTemplateName()
    {
        return "@Monza/Default/daniel.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  28 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Monza/Default/daniel.html.twig", "C:\\Monza\\src\\MonzaBundle\\Resources\\views\\Default\\daniel.html.twig");
    }
}
