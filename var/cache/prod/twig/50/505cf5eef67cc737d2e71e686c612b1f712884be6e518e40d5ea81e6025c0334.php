<?php

/* MonzaBundle:Default:equipe.html.twig */
class __TwigTemplate_d9de6ecbf3bba4d732fed81ebd1f50d57c437308f5761c2ece548f79bc6837ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "MonzaBundle:Default:equipe.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " Keman Racing";
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "
  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Notre équipe</li>
        </ul>
        <h2>NOTRE ÉQUIPE</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>
  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
        <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12\">
          <div class=\"teammembers\">
            <div class=\"he-wrap tpl2\">
              <img src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/img/team_01.png"), "html", null, true);
        echo "\" alt=\"\">
              <div class=\"he-view\">
                <div class=\"bg a0\" data-animate=\"fadeIn\">
                  <div class=\"center-bar\">
                    <a href=\"https://twitter.com/?lang=fr\" class=\"twitter a0\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                    <a href=\"https://fr-fr.facebook.com/\" class=\"facebook a1\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                    <a href=\"https://plus.google.com/discover\" class=\"google a2\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                  </div>
                </div>
              </div>
            </div>
            <div class=\"teammembers-meta\">
              <h4>Simon Semaan</h4>
            </div>
            <p>Simon est un ancien pilote de Formule 2, c'est un expert en matière de trajectoire et de réglages auto.</p>
            <small>Expert en Formule 2</small>
            <div class=\"teamskills\">
              <div class=\"progress\">
                <div data-effect=\"slide-left\" class=\"progress-bar progress-bar-danger\" style=\"width: 90%\"><span>Conduite sur piste</span></div>
              </div>
              <div class=\"progress\">
                <div data-effect=\"slide-left\" class=\"progress-bar progress-bar-danger\" style=\"width: 70%\"><span>Trajectoire</span></div>
              </div>
              <div class=\"progress\">
                <div data-effect=\"slide-left\" class=\"progress-bar progress-bar-danger\" style=\"width: 60%\"><span>Réglages véhicule</span></div>
              </div>
            </div>
          </div>
        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12\">
          <div class=\"teammembers\">
            <div class=\"he-wrap tpl2\">
              <img src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/img/team_02.png"), "html", null, true);
        echo "\" alt=\"\">
              <div class=\"he-view\">
                <div class=\"bg a0\" data-animate=\"fadeIn\">
                  <div class=\"center-bar\">
                    <a href=\"https://twitter.com/?lang=fr\" class=\"twitter a0\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                    <a href=\"https://fr-fr.facebook.com/\" class=\"facebook a1\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                    <a href=\"https://plus.google.com/discover\" class=\"google a2\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                  </div>
                </div>
              </div>
            </div>
            <div class=\"teammembers-meta\">
              <h4>Christophe Lalonde</h4>
            </div>
            <p>Christophe connait tous les circuits sur le bout des doigts. Son expèrience en DTM vous sera grandement utile.</p>
            <small>Ancien pilote DTM</small>
            <div class=\"teamskills\">
              <div class=\"progress\">
                <div data-effect=\"slide-left\" class=\"progress-bar progress-bar-danger\" style=\"width: 95%\"><span>Conduite sur piste</span></div>
              </div>
              <div class=\"progress\">
                <div data-effect=\"slide-left\" class=\"progress-bar progress-bar-danger\" style=\"width: 60%\"><span>Trajectoire</span></div>
              </div>
              <div class=\"progress\">
                <div data-effect=\"slide-left\" class=\"progress-bar progress-bar-danger\" style=\"width: 70%\"><span>Réglages véhicule</span></div>
              </div>
            </div>
          </div>
        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12\">
          <div class=\"teammembers\">
            <div class=\"he-wrap tpl2\">
              <img src=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/img/team_03.png"), "html", null, true);
        echo "\" alt=\"\">
              <div class=\"he-view\">
                <div class=\"bg a0\" data-animate=\"fadeIn\">
                  <div class=\"center-bar\">
                    <a href=\"https://twitter.com/?lang=fr\" class=\"twitter a0\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                    <a href=\"https://fr-fr.facebook.com/\" class=\"facebook a1\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                    <a href=\"https://plus.google.com/discover\" class=\"google a2\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                  </div>
                </div>
              </div>
            </div>
            <div class=\"teammembers-meta\">
              <h4>Noémie Poulin</h4>
            </div>
            <p>Notre championne ! Excellente à tous les niveaux, elle vous fera aimer à coups sûr la conduite sportive !</p>
            <small>Pilote d'essai en Formule 1</small>
            <div class=\"teamskills\">
              <div class=\"progress\">
                <div data-effect=\"slide-left\" class=\"progress-bar progress-bar-danger\" style=\"width: 90%\"><span>Conduite sur piste</span></div>
              </div>
              <div class=\"progress\">
                <div data-effect=\"slide-left\" class=\"progress-bar progress-bar-danger\" style=\"width: 80%\"><span>Trajectoire</span></div>
              </div>
              <div class=\"progress\">
                <div data-effect=\"slide-left\" class=\"progress-bar progress-bar-danger\" style=\"width: 70%\"><span>Réglages véhicule</span></div>
              </div>
            </div>
          </div>
        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12\">
          <div class=\"teammembers\">
            <div class=\"he-wrap tpl2\">
              <img src=\"";
        // line 129
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/img/team_04.png"), "html", null, true);
        echo "\" alt=\"\">
              <div class=\"he-view\">
                <div class=\"bg a0\" data-animate=\"fadeIn\">
                  <div class=\"center-bar\">
                    <a href=\"https://twitter.com/?lang=fr\" class=\"twitter a0\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                    <a href=\"https://fr-fr.facebook.com/\" class=\"facebook a1\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                    <a href=\"https://plus.google.com/discover\" class=\"google a2\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                  </div>
                </div>
              </div>
            </div>
            <div class=\"teammembers-meta\">
              <h4>Dominique Bernier</h4>
            </div>
            <p>Dominique est un ancien pilote de Formule 3, il est très rapide sur piste, surtout dans les virages rapides. Un pilote très apprécié de tous.</p>
            <small>Ancien pilote Formule 3</small>
            <div class=\"teamskills\">
              <div class=\"progress\">
                <div data-effect=\"slide-left\" class=\"progress-bar progress-bar-danger\" style=\"width: 76%\"><span>Conduite sur piste</span></div>
              </div>
              <div class=\"progress\">
                <div data-effect=\"slide-left\" class=\"progress-bar progress-bar-danger\" style=\"width: 88%\"><span>Trajectoire</span></div>
              </div>
              <div class=\"progress\">
                <div data-effect=\"slide-left\" class=\"progress-bar progress-bar-danger\" style=\"width: 32%\"><span>Réglages véhicule</span></div>
              </div>
            </div>
          </div>
        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12\">
          <div class=\"teammembers\">
            <div class=\"he-wrap tpl2\">
              <img src=\"";
        // line 161
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/img/team_05.png"), "html", null, true);
        echo "\" alt=\"\">
              <div class=\"he-view\">
                <div class=\"bg a0\" data-animate=\"fadeIn\">
                  <div class=\"center-bar\">
                    <a href=\"https://twitter.com/?lang=fr\" class=\"twitter a0\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                    <a href=\"https://fr-fr.facebook.com/\" class=\"facebook a1\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                    <a href=\"https://plus.google.com/discover\" class=\"google a2\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                  </div>
                </div>
              </div>
            </div>
            <div class=\"teammembers-meta\">
              <h4>Stéphanie Laux</h4>
            </div>
            <p>Stéphanie est notre dernière recrue, elle vient de finir son championnat du monde d'endurance où elle a finit 5 ème avec son équipe.</p>
            <small>Pilote d'endurance</small>
            <div class=\"teamskills\">
              <div class=\"progress\">
                <div data-effect=\"slide-left\" class=\"progress-bar progress-bar-danger\" style=\"width: 60%\"><span>Conduite sur piste</span></div>
              </div>
              <div class=\"progress\">
                <div data-effect=\"slide-left\" class=\"progress-bar progress-bar-danger\" style=\"width: 73%\"><span>Trajectoire</span></div>
              </div>
              <div class=\"progress\">
                <div data-effect=\"slide-left\" class=\"progress-bar progress-bar-danger\" style=\"width: 56%\"><span>Réglages véhicule</span></div>
              </div>
            </div>
          </div>
        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12\">
          <div class=\"teammembers\">
            <div class=\"he-wrap tpl2\">
              <img src=\"";
        // line 193
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/img/team_06.png"), "html", null, true);
        echo "\" alt=\"\">
              <div class=\"he-view\">
                <div class=\"bg a0\" data-animate=\"fadeIn\">
                  <div class=\"center-bar\">
                    <a href=\"https://twitter.com/?lang=fr\" class=\"twitter a0\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                    <a href=\"https://fr-fr.facebook.com/\" class=\"facebook a1\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                    <a href=\"https://plus.google.com/discover\" class=\"google a2\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                  </div>
                </div>
              </div>
            </div>
            <div class=\"teammembers-meta\">
              <h4>Frank Ferdinand</h4>
            </div>
            <p>Franck Ferdinand est toujours à bord d'un véhicule sur piste, puisqu'il est le pilote officiel de la Safety Car sur les championnats de F1.</p>
            <small>Pilote de safety car</small>
            <div class=\"teamskills\">
              <div class=\"progress\">
                <div data-effect=\"slide-left\" class=\"progress-bar progress-bar-danger\" style=\"width: 67%\"><span>Conduite sur piste</span></div>
              </div>
              <div class=\"progress\">
                <div data-effect=\"slide-left\" class=\"progress-bar progress-bar-danger\" style=\"width: 77%\"><span>Trajectoire</span></div>
              </div>
              <div class=\"progress\">
                <div data-effect=\"slide-left\" class=\"progress-bar progress-bar-danger\" style=\"width: 43%\"><span>Réglages véhicule</span></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

";
    }

    public function getTemplateName()
    {
        return "MonzaBundle:Default:equipe.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  243 => 193,  208 => 161,  173 => 129,  138 => 97,  103 => 65,  68 => 33,  38 => 5,  35 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "MonzaBundle:Default:equipe.html.twig", "C:\\Monza\\src\\MonzaBundle/Resources/views/Default/equipe.html.twig");
    }
}
