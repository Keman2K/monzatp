<?php

/* MonzaBundle:Default:qsn.html.twig */
class __TwigTemplate_60ecfbb00d5ef76fe35cb02c68107f5235dd601b2e4853dbf3dd25b5a9027816 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "MonzaBundle:Default:qsn.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " Keman Racing";
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "
  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Qui sommes-nous ?</li>
        </ul>
        <h2>QUI SOMMES-NOUS ?</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>
  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
        <div class=\"general-title text-center\">
          <h3>A Propos de nous</h3>
          <p>On vous dit tout !</p>
          <hr>
        </div>
        <div class=\"divider\"></div>
        <div class=\"col-lg-6 col-md-6 col-sm-7 col-xs-12\">
          <div class=\"he-wrap tpl6\">
            <img src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/about.jpg"), "html", null, true);
        echo "\" alt=\"qsn\" class=\"img-responsive\">
            <div class=\"he-view\">
              <div class=\"bg a0\" data-animate=\"fadeIn\">
                <div class=\"center-bar\">
                  <a href=\"https://twitter.com/?lang=fr\" class=\"twitter a0\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                  <a href=\"https://fr-fr.facebook.com/\" class=\"facebook a1\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                  <a href=\"https://plus.google.com/discover\" class=\"google a2\" data-animate=\"elasticInDown\" target=\"blank\"></a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
          <p>Keman Racing ! a été crée par des passionnés pour des passionnés !</p>
          <p>C'est lors d'une course de karting en mai 2007, que Michel Bou Antoun décida de créer la société Keman Racing !, une compagnie qui a pour vocation d'apprendre en toute sécurité la conduite sportive.</p>
          <p>105 personnes travaillent pour Keman Racing !, que ce soit en tant que formateurs, mécaniciens et ingénieurs...</p>
          <p>Nous allons tout faire pour vous proposer davantage de circuits mais aussi davantages de voitures !</p>
        </div>
      </div>
    </div>
  </section>
  <div class=\"clearfix\"></div>
  <div class=\"divider\"></div>
  <div class=\"container\">
    <div class=\"general-title text-center\">
      <h3>CE QUE NOUS FAISONS</h3>
      <p>Nos spécialités</p>
      <hr>
    </div>
    <div class=\"skills text-center\">
      <div class=\"col-lg-3 col-md-3 col-sm-6 col-xs-12\">
        <span class=\"chart\" data-percent=\"90\">
        <span class=\"percent\"></span>
        </span>
        <h4 class=\"title\">Conduite sur pistes</h4>
      </div>
      <div class=\"col-lg-3 col-md-3 col-sm-6 col-xs-12\">
        <span class=\"chart\" data-percent=\"75\">
        <span class=\"percent\"></span>
        </span>
        <h4 class=\"title\">Apprentissage</h4>
      </div>
      <div class=\"col-lg-3 col-md-3 col-sm-6 col-xs-12\">
        <span class=\"chart\" data-percent=\"65\">
        <span class=\"percent\"></span>
        </span>
        <h4 class=\"title\">Monoplaces</h4>
      </div>
      <div class=\"col-lg-3 col-md-3 col-sm-6 col-xs-12\">
        <span class=\"chart\" data-percent=\"40\">
        <span class=\"percent\"></span>
        </span>
        <h4 class=\"title\">Courses de côtes</h4>
      </div>
    </div>
  </div>
  <div class=\"clearfix\"></div>
  <div class=\"divider\"></div>
  <section class=\"section1\">
    <div class=\"container\">
      <div class=\"general-title text-center\">
        <h3>QUELQUES STATS</h3>
        <p>Informations importantes à propos de nous</p>
        <hr>
      </div>
      <div class=\"stat f-container\">
        <div class=\"f-element col-lg-3 col-md-3 col-sm-6 col-xs-12\">
          <i class=\"fa fa-film fa-4x\"></i>
          <div class=\"milestone-counter\">
            <span class=\"stat-count highlight\">150</span>
            <div class=\"milestone-details\">Montages vidéos</div>
          </div>
        </div>
        <div class=\"f-element col-lg-3 col-md-3 col-sm-6 col-xs-12\">
          <i class=\"fas fa-thumbs-up\"></i>
          <div class=\"milestone-counter\">
            <span class=\"stat-count highlight\">517</span>
            <div class=\"milestone-details\">Personnes qui font partis de Keman Racing !</div>
          </div>
        </div>
        <div class=\"f-element col-lg-3 col-md-3 col-sm-6 col-xs-12\">
          <i class=\"fas fa-globe-europe\"></i>
          <div class=\"milestone-counter\">
            <span class=\"stat-count highlight\">25</span>
            <div class=\"milestone-details\">Circuits à venir prochainement</div>
          </div>
        </div>
        <div class=\"f-element col-lg-3 col-md-3 col-sm-6 col-xs-12\">
          <!-- <i class=\"fa fa-car-alt-o fa-4x\"></i> --><i class=\"fas fa-car-alt\"></i>
          <div class=\"milestone-counter\">
            <span class=\"stat-count highlight\">300</span>
            <div class=\"milestone-details\">Voitures avant fin 2019</div>
          </div>
        </div>
      </div>
    </div>
  </section>

";
    }

    public function getTemplateName()
    {
        return "MonzaBundle:Default:qsn.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 38,  38 => 5,  35 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "MonzaBundle:Default:qsn.html.twig", "C:\\Monza\\src\\MonzaBundle/Resources/views/Default/qsn.html.twig");
    }
}
