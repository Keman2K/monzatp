<?php

/* MonzaBundle:Default:nouvelles.html.twig */
class __TwigTemplate_6f5b7f220dc4dda02d4348895b18e7e7bb71f9e7480d57ba7f3270242e41016f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "MonzaBundle:Default:nouvelles.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Nouvelles voitures</li>
        </ul>
        <h2>NOUVELLES VOITURES</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>

            <div class=\"portfolio-item col-lg-4 col-md-4 col-sm-4 col-xs-12 mockups\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"img/portfolio_01.jpg\" alt=\"\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                    <h3 class=\"big a1\" data-animate=\"fadeInDown\">Project Name Here</h3>
                    <a data-rel=\"prettyPhoto\" href=\"img/portfolio_01.jpg\" class=\"dmbutton a2\" data-animate=\"bounceInLeft\"><i class=\"fa fa-search\"></i></a>
                    <a href=\"#\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                    <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                    </div>
                    <!-- portfolio_category -->
                  </div>
                  <!-- he bg -->
                </div>
                <!-- he view -->
              </div>
              <!-- he wrap -->
              <h3 class=\"title\">Toyota Eagle GTP Mk II</h3>

            </div>
            <!-- end col-4 -->

            <div class=\"portfolio-item col-lg-4 col-md-4 col-sm-4 col-xs-12 web-design graphic-design\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"img/portfolio_02.jpg\" alt=\"\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                    <h3 class=\"big a1\" data-animate=\"fadeInDown\">Project Name Here</h3>
                    <a data-rel=\"prettyPhoto\" href=\"img/portfolio_02.jpg\" class=\"dmbutton a2\" data-animate=\"bounceInLeft\"><i class=\"fa fa-search\"></i></a>
                    <a href=\"#\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                    <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                    </div>
                    <!-- portfolio_category -->
                  </div>
                  <!-- he bg -->
                </div>
                <!-- he view -->
              </div>
              <!-- he wrap -->
              <h3 class=\"title\">Project Name - 2</h3>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\"s standard dummy..</p>
            </div>
            <!-- end col-4 -->

            <div class=\"portfolio-item col-lg-4 col-md-4 col-sm-4 col-xs-12 graphic-design\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"img/portfolio_03.jpg\" alt=\"\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                    <h3 class=\"big a1\" data-animate=\"fadeInDown\">Project Name Here</h3>
                    <a data-rel=\"prettyPhoto\" href=\"img/portfolio_03.jpg\" class=\"dmbutton a2\" data-animate=\"bounceInLeft\"><i class=\"fa fa-search\"></i></a>
                    <a href=\"#\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                    <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                    </div>
                    <!-- portfolio_category -->
                  </div>
                  <!-- he bg -->
                </div>
                <!-- he view -->
              </div>
              <!-- he wrap -->
              <h3 class=\"title\">Project Name - 3</h3>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\"s standard dummy..</p>
            </div>
            <!-- end col-4 -->

            <div class=\"portfolio-item col-lg-4 col-md-4 col-sm-4 col-xs-12 mockups\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"img/portfolio_04.jpg\" alt=\"\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                    <h3 class=\"big a1\" data-animate=\"fadeInDown\">Project Name Here</h3>
                    <a data-rel=\"prettyPhoto\" href=\"img/portfolio_04.jpg\" class=\"dmbutton a2\" data-animate=\"bounceInLeft\"><i class=\"fa fa-search\"></i></a>
                    <a href=\"#\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                    <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                    </div>
                    <!-- portfolio_category -->
                  </div>
                  <!-- he bg -->
                </div>
                <!-- he view -->
              </div>
              <!-- he wrap -->
              <h3 class=\"title\">Project Name - 4</h3>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\"s standard dummy..</p>
            </div>
            <!-- end col-12 -->

            <div class=\"portfolio-item col-lg-4 col-md-4 col-sm-4 col-xs-12 web-design\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"img/portfolio_05.jpg\" alt=\"\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                    <h3 class=\"big a1\" data-animate=\"fadeInDown\">Project Name Here</h3>
                    <a data-rel=\"prettyPhoto\" href=\"img/portfolio_05.jpg\" class=\"dmbutton a2\" data-animate=\"bounceInLeft\"><i class=\"fa fa-search\"></i></a>
                    <a href=\"#\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                    <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                    </div>
                    <!-- portfolio_category -->
                  </div>
                  <!-- he bg -->
                </div>
                <!-- he view -->
              </div>
              <!-- he wrap -->
              <h3 class=\"title\">Project Name - 5</h3>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\"s standard dummy..</p>
            </div>
            <!-- end col-12 -->

            <div class=\"portfolio-item col-lg-4 col-md-4 col-sm-4 col-xs-12 web-design\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"img/portfolio_06.jpg\" alt=\"\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                    <h3 class=\"big a1\" data-animate=\"fadeInDown\">Project Name Here</h3>
                    <a data-rel=\"prettyPhoto\" href=\"img/portfolio_06.jpg\" class=\"dmbutton a2\" data-animate=\"bounceInLeft\"><i class=\"fa fa-search\"></i></a>
                    <a href=\"#\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                    <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                    </div>
                    <!-- portfolio_category -->
                  </div>
                  <!-- he bg -->
                </div>
                <!-- he view -->
              </div>
              <!-- he wrap -->
              <h3 class=\"title\">Project Name - 6</h3>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\"s standard dummy..</p>
            </div>
            <!-- end col-12 -->

          </div>
          <!-- portfolio -->
        </div>
        <!-- portfolio container -->
        <div class=\"divider\"></div>
      </div>
      <!-- end container -->
  </section>

";
    }

    public function getTemplateName()
    {
        return "MonzaBundle:Default:nouvelles.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "MonzaBundle:Default:nouvelles.html.twig", "C:\\Monza\\src\\MonzaBundle/Resources/views/Default/nouvelles.html.twig");
    }
}
