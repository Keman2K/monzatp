<?php

/* @Monza/Default/plan.html.twig */
class __TwigTemplate_c89971a8d89717fe24b1a224dfe927f4ef59b9ad87b025ec2301f1144ea4c1af extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@Monza/Default/plan.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " Keman Racing";
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "
  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Plan du site</li>
        </ul>
        <h2>PLAN DU SITE</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>


<section class=\"section1\">
  <div class=\"container clearfix\">
    <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
      <div class=\"col-lg-6 col-md-6 col-sm-12\">
        <div class=\"theme_details\">
          <div class=\"item-description\">
            <a href=\"/\"><li>Acceuil</li></a>
            <br/>
            <a href=\"/qsn\"><li>Qui sommes-nous ?</li></a>
            <a href=\"/services\"><li>Nos services</li></a>
            <a href=\"/equipe\"><li>Notre équipe</li></a>
            <a href=\"/faq\"><li>F.A.Q</li></a>
            <a href=\"/contact\"><li>Nous contacter</li></a>
            <a href=\"/avis\"><li>Déposer un avis</li></a>
            <br/>
            <a href=\"/circuits\"><li>Nos circuits</li></a>
            <br/>
            <a href=\"/panier\"><li>Votre panier</li></a>
            <a href=\"/commnades\"><li>Mes commandes</li></a>
            <a href=\"/compte\"><li>Mon compte</li></a>
            <br/>
            <a href=\"/CGV\"><li>Conditions générales de ventes</li></a>
            <br/>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>



";
    }

    public function getTemplateName()
    {
        return "@Monza/Default/plan.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 5,  35 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Monza/Default/plan.html.twig", "C:\\Monza\\src\\MonzaBundle\\Resources\\views\\Default\\plan.html.twig");
    }
}
