<?php

/* @Monza/Default/commandes.html.twig */
class __TwigTemplate_0abfb01b46b254c95534016c141cb34446de89bd8c245ebbc83f2e4767722926 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@Monza/Default/commandes.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " Keman Racing";
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "
  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Mes commandes</li>
        </ul>
        <h2>MES COMMANDES</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <!-- search -->
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
        <!-- / end div .search-bar -->
      </div>
    </div>
  </section>
  <!-- end post-wrapper-top -->

  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-8 col-md-8 col-sm-8 col-xs-12 clearfix\">
        <table class=\"table table-striped\" data-effect=\"fade\">
          <thead>
            <tr>
              <th>Mes circuits</th>
              <th>Mes voitures</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Mettre quelque chose ici</td>
              <td>Mettre quelque chose ici</td>
            </tr>
            <tr>
              <td>Mettre quelque chose ici</td>
              <td>Mettre quelque chose ici</td>
            </tr>
          </tbody>
        </table>

        <div class=\"clearfix\"></div>
        <hr>

        <table class=\"table table-striped\" data-effect=\"fade\">
          <thead>
            <tr>
              <th>Numéro</th>
              <th>Dates</th>
              <th>Montants</th>
              <th>Consulter mes avis</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>01</td>
              <td>Mettre la date</td>
              <td>Mettre l'heure</td>
              <td><a href=\"#\">Consultez les avis</a></td>
            </tr>
            <tr>
              <td>02</td>
              <td>Mettre la date</td>
              <td>Mettre l'heure</td>
              <td><a href=\"#\">Consultez les avis</a></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
  ";
    }

    public function getTemplateName()
    {
        return "@Monza/Default/commandes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 5,  35 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Monza/Default/commandes.html.twig", "C:\\Monza\\src\\MonzaBundle\\Resources\\views\\Default\\commandes.html.twig");
    }
}
