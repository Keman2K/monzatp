<?php

/* MonzaBundle:Default:circuit.html.twig */
class __TwigTemplate_91a98ee4fd612c388fd08e2448a88ca0973dd5ee6981412d698e6a863a2d7ba2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "MonzaBundle:Default:circuit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Circuit ";
        // line 9
        echo $this->getAttribute(($context["circuit"] ?? null), "getCircuitName", array(), "method");
        echo "</li>
        </ul>
        <h2>Circuit ";
        // line 11
        echo $this->getAttribute(($context["circuit"] ?? null), "getCircuitName", array(), "method");
        echo "</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>
  <section class=\"marketplace-top\">
    <div id=\"market-wrapper\">
      <div class=\"item_image\">
         <img data-effect=\"fade\" class=\"aligncenter\" width=\"600\" height=\"400\" src=\"";
        // line 28
        echo $this->getAttribute(($context["circuit"] ?? null), "getCircuitImage", array());
        echo "\">
      </div>
    </div>
  </section>
  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
        <div class=\"general-title text-center\">
          <h3>Circuit ";
        // line 36
        echo $this->getAttribute(($context["circuit"] ?? null), "getCircuitName", array(), "method");
        echo "</h3>
          <p>";
        // line 37
        echo $this->getAttribute(($context["circuit"] ?? null), "getCircuitTitre", array());
        echo "</p>
          <hr>
        </div>
        <div class=\"divider\"></div>
        <div class=\"item_details\">
          <div class=\"col-lg-3 col-md-3 col-sm-12\">
            <div class=\"theme_details\">
              <div class=\"details_section\">
                <h3>En détails : </h3>
                <ul>
                  <li class=\"version\">Longueur : <span>";
        // line 47
        echo $this->getAttribute(($context["circuit"] ?? null), "getCircuitLongueur", array(), "method");
        echo "</span></li>
                  <li class=\"release\">Meilleur temps: <span>";
        // line 48
        echo $this->getAttribute(($context["circuit"] ?? null), "getCircuitTour", array(), "method");
        echo "</span></li>
                  <li class=\"designer\">Pilote : <span>";
        // line 49
        echo $this->getAttribute(($context["circuit"] ?? null), "getCircuitPilote", array(), "method");
        echo "</span></li>
                  <li class=\"designer\">Nombre de virages : <span>";
        // line 50
        echo $this->getAttribute(($context["circuit"] ?? null), "getCircuitVirage", array(), "method");
        echo "</li>
                </ul>
              </div>
            </div>
          </div>
      <div class=\"col-lg-6 col-md-6 col-sm-12\">
        <div class=\"theme_details\">
          <div class=\"item-description\">
            ";
        // line 58
        echo $this->getAttribute(($context["circuit"] ?? null), "getCircuitDescription", array(), "method");
        echo "
          </div>
        </div>
      </div>
      <div class=\"col-lg-3 col-md-3 col-sm-12\">
        <div class=\"theme_details\">
          <div class=\"item_price\">

          </div>
              <hr>
              <div class=\"buttons\">
                <a class=\"button btn-block large\" href=\"/brands?circuitId=";
        // line 69
        echo twig_escape_filter($this->env, $this->getAttribute(($context["circuit"] ?? null), "getId", array(), "method"), "html", null, true);
        echo "\">
                  <i class=\"fas fa-arrow-right\"></i> Continuer vers les voitures</a>
              </div>
              <hr>
            </div>
          </div>
        </div>
    <div class=\"clearfix\"></div>

</section>
";
    }

    public function getTemplateName()
    {
        return "MonzaBundle:Default:circuit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 69,  114 => 58,  103 => 50,  99 => 49,  95 => 48,  91 => 47,  78 => 37,  74 => 36,  63 => 28,  43 => 11,  38 => 9,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "MonzaBundle:Default:circuit.html.twig", "C:\\Monza\\src\\MonzaBundle/Resources/views/Default/circuit.html.twig");
    }
}
