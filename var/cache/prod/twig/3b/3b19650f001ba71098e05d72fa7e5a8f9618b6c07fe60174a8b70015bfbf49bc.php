<?php

/* MonzaBundle:Default:connexion.html.twig */
class __TwigTemplate_d1e0b88e3a4f3f95d793506e7a4d14b9c3a2247376b8e73677d52adbcdbfeb45 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "MonzaBundle:Default:connexion.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Accueil</a></li>
          <li>Connexion</li>
        </ul>
        <h2>CONNEXION</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>
  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
        <div class=\"col-lg-6 col-md-6 col-sm-12\">
          <h4 class=\"title\">
            <span>Bienvenue !</span>
            </h4>
          <p>Connectez-vous afin de bénéficiez des dernières offres promotionelles !.</p>
          <p>De nombreuses nouveautés vous attendent, n'hésitez pas à vous inscrire à notre newsletter !</p>
          <p class=\"withpadding\">Pas encore inscrit ? <a href=\"/register\">Cliquez ici</a> pour créer un compte.</p>
        </div>
        <div class=\"col-lg-6 col-md-6 col-sm-12\">
          <h4 class=\"title\">
            <span>Formulaire de connexion</span>
            </h4>
          <form id=\"loginform\" method=\"post\" name=\"loginform\" action=\"\">
            <div class=\"form-group\">
              <div class=\"input-group\">
                <span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>
                <input type=\"text\" class=\"form-control\" placeholder=\"Nom\">
              </div>
            </div>
            <div class=\"form-group\">
              <div class=\"input-group\">
                <span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>
                <input type=\"password\" class=\"form-control\" placeholder=\"Mot de passe\">
              </div>
            </div>
            <div class=\"form-group\">
              <div class=\"checkbox\">
                <label>
                  <input type=\"checkbox\"> Se souvenir de moi
                </label>
              </div>
            </div>
            <div class=\"form-group\">
              <button type=\"submit\" class=\"button\">Je me connecte</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  
";
    }

    public function getTemplateName()
    {
        return "MonzaBundle:Default:connexion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "MonzaBundle:Default:connexion.html.twig", "C:\\Monza\\src\\MonzaBundle/Resources/views/Default/connexion.html.twig");
    }
}
