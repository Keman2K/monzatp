<?php

/* @Monza/Default/brands.html.twig */
class __TwigTemplate_ce29fc4da4b09268d169e7a6d8c5254abbac09d56c27a34d04e3f70eea1209a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@Monza/Default/brands.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Nos voitures</li>
        </ul>
        <h2>Vous avez choisit le circuit ";
        // line 11
        echo $this->getAttribute(($context["circuit"] ?? null), "getCircuitName", array(), "method");
        echo "</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>
          <section class=\"section1\">
            <div class=\"container clearfix\">
                <div class=\" col-lg-12 col-md-12 col-sm-12 clearfix\">

                  <div class=\"divider\"></div>
                <h1>JE CHOISIS ENSUITE MA VOITURE !</h1>
                <nav class=\"portfolio-filter clearfix\">
                  <ul>
                    <li><a href=\"\" class=\"dmbutton2\" data-filter=\"*\">Toutes les marques </a></li>
                    ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["filteredBrands"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["brand"]) {
            // line 35
            echo "                    <li><a href=\"\" class=\"dmbutton2\" data-filter=\".";
            echo twig_escape_filter($this->env, $context["brand"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["brand"], "html", null, true);
            echo "</a></li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['brand'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "                  </ul>
                </nav>
                <div class=\"portfolio-centered\">
                <div class=\"recentitems portfolio\">

                  ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["brands"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["brand"]) {
            // line 43
            echo "
                  <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12 ";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($context["brand"], "getBrandName", array(), "method"), "html", null, true);
            echo "\">
                    <div class=\"he-wrap tpl6 market-item\">
                      <img src=\"";
            // line 46
            echo $this->getAttribute($context["brand"], "getBrandImage", array());
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["brand"], "getBrandModel", array()), "html", null, true);
            echo "\">
                        <div class=\"he-view\">
                          <div class=\"bg a0\" data-animate=\"fadeIn\">
                            <h3 class=\"a1\" data-animate=\"fadeInDown\">";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["brand"], "getBrandName", array(), "method"), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["brand"], "getBrandModel", array()), "html", null, true);
            echo "</h3>
                            <a data-rel=\"prettyPhoto\" href=\"";
            // line 50
            echo $this->getAttribute($context["brand"], "getBrandImage", array());
            echo "\"></a>
                            <a href=\"brands/";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["brand"], "getId", array(), "method"), "html", null, true);
            echo "?circuitId=";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["circuit"] ?? null), "getId", array(), "method"), "html", null, true);
            echo "\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                            <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                            <a href=\"brands/";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["brand"], "getId", array(), "method"), "html", null, true);
            echo "?circuitId=";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["circuit"] ?? null), "getId", array(), "method"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["brand"], "getBrandTitle", array()), "html", null, true);
            echo "</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['brand'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

  <div class=\"portfolio-centered\">
  <div class=\"recentitems portfolio\">
  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@Monza/Default/brands.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 60,  125 => 53,  118 => 51,  114 => 50,  108 => 49,  100 => 46,  95 => 44,  92 => 43,  88 => 42,  81 => 37,  70 => 35,  66 => 34,  40 => 11,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Monza/Default/brands.html.twig", "C:\\Monza\\src\\MonzaBundle\\Resources\\views\\Default\\brands.html.twig");
    }
}
