<?php

/* @Monza/Default/circuits.html.twig */
class __TwigTemplate_b6a588800c6dc469c7b8383453de7d592ce1ae0c3f6d7536263872cbdc34fe10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@Monza/Default/circuits.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Nos circuits</li>
        </ul>
        <h2>NOS CIRCUITS</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>


  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\" col-lg-12 col-md-12 col-sm-12 clearfix\">
        <div class=\"divider\">
        </div>
          <h1>JE CHOISIS D'ABORD MON CIRCUIT !</h1>
        <nav class=\"portfolio-filter clearfix\">
          <ul>
            <li><a href=\"#\" class=\"dmbutton2\" data-filter=\"*\">Tous les pays</a></li>
              ";
        // line 36
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["filteredCircuits"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["circuit"]) {
            // line 37
            echo "            <li><a href=\"#\" class=\"dmbutton2\" data-filter=\".";
            echo twig_escape_filter($this->env, $context["circuit"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["circuit"], "html", null, true);
            echo "</a></li>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['circuit'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "          </ul>
        </nav>
        <div class=\"portfolio-centered\">
          <div class=\"recentitems portfolio\">

            ";
        // line 44
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["circuits"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["circuit"]) {
            // line 45
            echo "            <div class=\"portfolio-item col-lg-6 col-md-6 col-sm-6 col-xs-12 ";
            echo $this->getAttribute($context["circuit"], "getCircuitPays", array(), "method");
            echo "\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"";
            // line 47
            echo $this->getAttribute($context["circuit"], "getCircuitImage", array());
            echo "\" alt=\"";
            echo $this->getAttribute($context["circuit"], "getCircuitName", array(), "method");
            echo "\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                  <h3 class=\"big a1\" data-animate=\"fadeInDown\">";
            // line 50
            echo $this->getAttribute($context["circuit"], "getCircuitName", array(), "method");
            echo "</h3>
                  <a data-rel=\"prettyPhoto\" href=\"";
            // line 51
            echo $this->getAttribute($context["circuit"], "getCircuitImage", array());
            echo "\"></a>
                  <a href=\"circuits/";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($context["circuit"], "getId", array(), "method"), "html", null, true);
            echo "\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                  <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                  <a href=\"circuits/";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($context["circuit"], "getId", array(), "method"), "html", null, true);
            echo "\">";
            echo $this->getAttribute($context["circuit"], "getCircuitTitre", array(), "method");
            echo "</a>
                  </div>
                </div>
              </div>
            </div>
            <h3 class=\"title\">";
            // line 59
            echo $this->getAttribute($context["circuit"], "getCircuitName", array(), "method");
            echo "</h3>
            </div>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['circuit'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "        <div class=\"divider\"></div>
      </div>
    </div></div></div>
  </section>

";
    }

    public function getTemplateName()
    {
        return "@Monza/Default/circuits.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 62,  128 => 59,  118 => 54,  113 => 52,  109 => 51,  105 => 50,  97 => 47,  91 => 45,  87 => 44,  80 => 39,  69 => 37,  65 => 36,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Monza/Default/circuits.html.twig", "C:\\Monza\\src\\MonzaBundle\\Resources\\views\\Default\\circuits.html.twig");
    }
}
