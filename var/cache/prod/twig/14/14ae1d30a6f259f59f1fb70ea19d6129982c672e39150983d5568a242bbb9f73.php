<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_5c21d90ec70f6296d16de5f9cc85aa98419a7860fe5ad3ac5ddf96426099e8dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "      <section class=\"post-wrapper-top\">
        <div class=\"container\">
          <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
            <ul class=\"breadcrumb\">
              <li><a href=\"/\">Accueil</a></li>
              <li>Connexion</li>
            </ul>
            <h2>CONNEXION</h2>
          </div>
          <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
            <div class=\"search-bar\">
              <form action=\"\" method=\"get\">
                <fieldset>
                  <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
                  <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
                </fieldset>
              </form>
            </div>
          </div>
        </div>
      </section>
      <section class=\"section1\">
        <div class=\"container clearfix\">
          <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
            <div class=\"col-lg-6 col-md-6 col-sm-12\">
              <h4 class=\"title\">
                <span>Bienvenue !</span>
                </h4>
              <p>Connectez-vous afin de bénéficiez des dernières offres promotionelles !.</p>
              <p>De nombreuses nouveautés vous attendent, n'hésitez pas à vous inscrire à notre newsletter !</p>
              <p class=\"withpadding\">Pas encore inscrit ? <a href=\"/register\">Cliquez ici</a> pour créer un compte.</p>
            </div>
            <div class=\"col-lg-6 col-md-6 col-sm-12\">
              <h4 class=\"title\">
                <span>Formulaire de connexion</span>
                </h4>

\t\t\t\t\t";
        // line 41
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 44
        echo "
            </div>
          </div>
        </div>
      </section>
      
    ";
    }

    // line 41
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 42
        echo "\t\t\t\t\t    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
\t\t\t\t\t";
    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 42,  83 => 41,  73 => 44,  71 => 41,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Security/login.html.twig", "C:\\Monza\\app\\Resources\\FOSUserBundle\\views\\Security\\login.html.twig");
    }
}
