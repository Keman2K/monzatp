<?php

/* FOSUserBundle:Security:login_content.html.twig */
class __TwigTemplate_dbbe736a4c0ab67de6baa60d612d93c7f1927f2d12a2794d2e8909728a3f3a79 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "

";
        // line 4
        if (($context["error"] ?? null)) {
            // line 5
            echo "    <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? null), "messageKey", array()), $this->getAttribute(($context["error"] ?? null), "messageData", array()), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 6
        echo " 



<form id=\"loginform\" method=\"post\" name=\"loginform\" action=\"";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_check");
        echo "\">
     ";
        // line 11
        if (($context["csrf_token"] ?? null)) {
            // line 12
            echo "        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            echo twig_escape_filter($this->env, ($context["csrf_token"] ?? null), "html", null, true);
            echo "\" />
    ";
        }
        // line 14
        echo "
    <div class=\"form-group\">
      <div class=\"input-group\">
        <span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>
        <input type=\"text\" class=\"form-control\" placeholder=\"Nom\" id=\"username\" name=\"_username\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, ($context["last_username"] ?? null), "html", null, true);
        echo "\" required=\"required\" autocomplete=\"username\" >
      </div>
    </div>
    <div class=\"form-group\">
      <div class=\"input-group\">
        <span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>
        <input type=\"password\" class=\"form-control\" placeholder=\"Mot de passe\" type=\"password\" id=\"password\" name=\"_password\" required=\"required\" autocomplete=\"curent-password\" >
      </div>
    </div>
    <div class=\"form-group\">
      <div class=\"checkbox\">
        <label>
          <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" > Se souvenir de moi
        </label>
      </div>
    </div>
    <div class=\"form-group\">
      <!-- <button type=\"submit\" class=\"button\">Je me connecte</button> -->
      <input type=\"submit\" id=\"_submit\" class=\"button\" name=\"_submit\" value=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
</form>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 36,  55 => 18,  49 => 14,  43 => 12,  41 => 11,  37 => 10,  31 => 6,  25 => 5,  23 => 4,  19 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Security:login_content.html.twig", "C:\\Monza\\app/Resources/FOSUserBundle/views/Security/login_content.html.twig");
    }
}
