<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_46307715f126247653b5cc98c976781afeb660f00eb4c2f5ba3df9d5e1f81d7a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "      <section class=\"post-wrapper-top\">
        <div class=\"container\">
          <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
            <ul class=\"breadcrumb\">
              <li><a href=\"/\">Accueil</a></li>
              <li>Connexion</li>
            </ul>
            <h2>CONNEXION</h2>
          </div>
          <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
            <div class=\"search-bar\">
              <form action=\"\" method=\"get\">
                <fieldset>
                  <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
                  <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
                </fieldset>
              </form>
            </div>
          </div>
        </div>
      </section>
      <section class=\"section1\">
        <div class=\"container clearfix\">
          <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
            <div class=\"col-lg-6 col-md-6 col-sm-12\">
              <h4 class=\"title\">
                <span>Bienvenue !</span>
                </h4>
              <p>Connectez-vous afin de bénéficiez des dernières offres promotionelles !.</p>
              <p>De nombreuses nouveautés vous attendent, n'hésitez pas à vous inscrire à notre newsletter !</p>
              <p class=\"withpadding\">Pas encore inscrit ? <a href=\"/register\">Cliquez ici</a> pour créer un compte.</p>
            </div>
            <div class=\"col-lg-6 col-md-6 col-sm-12\">
              <h4 class=\"title\">
                <span>Formulaire de connexion</span>
                </h4>

\t\t\t\t\t";
        // line 41
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 44
        echo "
            </div>
          </div>
        </div>
      </section>
      
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 41
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 42
        echo "\t\t\t\t\t    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
\t\t\t\t\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 42,  107 => 41,  91 => 44,  89 => 41,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

    {% block body %}
      <section class=\"post-wrapper-top\">
        <div class=\"container\">
          <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
            <ul class=\"breadcrumb\">
              <li><a href=\"/\">Accueil</a></li>
              <li>Connexion</li>
            </ul>
            <h2>CONNEXION</h2>
          </div>
          <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
            <div class=\"search-bar\">
              <form action=\"\" method=\"get\">
                <fieldset>
                  <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
                  <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
                </fieldset>
              </form>
            </div>
          </div>
        </div>
      </section>
      <section class=\"section1\">
        <div class=\"container clearfix\">
          <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
            <div class=\"col-lg-6 col-md-6 col-sm-12\">
              <h4 class=\"title\">
                <span>Bienvenue !</span>
                </h4>
              <p>Connectez-vous afin de bénéficiez des dernières offres promotionelles !.</p>
              <p>De nombreuses nouveautés vous attendent, n'hésitez pas à vous inscrire à notre newsletter !</p>
              <p class=\"withpadding\">Pas encore inscrit ? <a href=\"/register\">Cliquez ici</a> pour créer un compte.</p>
            </div>
            <div class=\"col-lg-6 col-md-6 col-sm-12\">
              <h4 class=\"title\">
                <span>Formulaire de connexion</span>
                </h4>

\t\t\t\t\t{% block fos_user_content %}
\t\t\t\t\t    {{ include('@FOSUser/Security/login_content.html.twig') }}
\t\t\t\t\t{% endblock fos_user_content %}

            </div>
          </div>
        </div>
      </section>
      
    {% endblock %}
", "@FOSUser/Security/login.html.twig", "C:\\Monza\\app\\Resources\\FOSUserBundle\\views\\Security\\login.html.twig");
    }
}
