<?php

/* MonzaBundle:Default:brands.html.twig */
class __TwigTemplate_a27206e544984f79922ad2f2e3dd5e9b2cc3988b1208a648d4e147860ad18471 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "MonzaBundle:Default:brands.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MonzaBundle:Default:brands.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MonzaBundle:Default:brands.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Nos voitures</li>
        </ul>
        <h2>Vous avez choisit le circuit ";
        // line 11
        echo $this->getAttribute(($context["circuit"] ?? $this->getContext($context, "circuit")), "getCircuitName", array(), "method");
        echo "</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>
          <section class=\"section1\">
            <div class=\"container clearfix\">
                <div class=\" col-lg-12 col-md-12 col-sm-12 clearfix\">

                  <div class=\"divider\"></div>
                <h1>JE CHOISIS ENSUITE MA VOITURE !</h1>
                <nav class=\"portfolio-filter clearfix\">
                  <ul>
                    <li><a href=\"\" class=\"dmbutton2\" data-filter=\"*\">Toutes les marques </a></li>
                    ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["filteredBrands"] ?? $this->getContext($context, "filteredBrands")));
        foreach ($context['_seq'] as $context["_key"] => $context["brand"]) {
            // line 35
            echo "                    <li><a href=\"\" class=\"dmbutton2\" data-filter=\".";
            echo twig_escape_filter($this->env, $context["brand"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["brand"], "html", null, true);
            echo "</a></li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['brand'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "                  </ul>
                </nav>
                <div class=\"portfolio-centered\">
                <div class=\"recentitems portfolio\">

                  ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["brands"] ?? $this->getContext($context, "brands")));
        foreach ($context['_seq'] as $context["_key"] => $context["brand"]) {
            // line 43
            echo "
                  <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12 ";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($context["brand"], "getBrandName", array(), "method"), "html", null, true);
            echo "\">
                    <div class=\"he-wrap tpl6 market-item\">
                      <img src=\"";
            // line 46
            echo $this->getAttribute($context["brand"], "getBrandImage", array());
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["brand"], "getBrandModel", array()), "html", null, true);
            echo "\">
                        <div class=\"he-view\">
                          <div class=\"bg a0\" data-animate=\"fadeIn\">
                            <h3 class=\"a1\" data-animate=\"fadeInDown\">";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["brand"], "getBrandName", array(), "method"), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["brand"], "getBrandModel", array()), "html", null, true);
            echo "</h3>
                            <a data-rel=\"prettyPhoto\" href=\"";
            // line 50
            echo $this->getAttribute($context["brand"], "getBrandImage", array());
            echo "\"></a>
                            <a href=\"brands/";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["brand"], "getId", array(), "method"), "html", null, true);
            echo "?circuitId=";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["circuit"] ?? $this->getContext($context, "circuit")), "getId", array(), "method"), "html", null, true);
            echo "\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                            <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                            <a href=\"brands/";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["brand"], "getId", array(), "method"), "html", null, true);
            echo "?circuitId=";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["circuit"] ?? $this->getContext($context, "circuit")), "getId", array(), "method"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["brand"], "getBrandTitle", array()), "html", null, true);
            echo "</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['brand'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

  <div class=\"portfolio-centered\">
  <div class=\"recentitems portfolio\">
  </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "MonzaBundle:Default:brands.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  160 => 60,  143 => 53,  136 => 51,  132 => 50,  126 => 49,  118 => 46,  113 => 44,  110 => 43,  106 => 42,  99 => 37,  88 => 35,  84 => 34,  58 => 11,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Nos voitures</li>
        </ul>
        <h2>Vous avez choisit le circuit {{ circuit.getCircuitName() | raw }}</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>
          <section class=\"section1\">
            <div class=\"container clearfix\">
                <div class=\" col-lg-12 col-md-12 col-sm-12 clearfix\">

                  <div class=\"divider\"></div>
                <h1>JE CHOISIS ENSUITE MA VOITURE !</h1>
                <nav class=\"portfolio-filter clearfix\">
                  <ul>
                    <li><a href=\"\" class=\"dmbutton2\" data-filter=\"*\">Toutes les marques </a></li>
                    {% for brand in filteredBrands %}
                    <li><a href=\"\" class=\"dmbutton2\" data-filter=\".{{brand}}\">{{brand}}</a></li>
                    {% endfor %}
                  </ul>
                </nav>
                <div class=\"portfolio-centered\">
                <div class=\"recentitems portfolio\">

                  {% for brand in brands %}

                  <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12 {{brand.getBrandName()}}\">
                    <div class=\"he-wrap tpl6 market-item\">
                      <img src=\"{{brand.getBrandImage | raw }}\" alt=\"{{brand.getBrandModel}}\">
                        <div class=\"he-view\">
                          <div class=\"bg a0\" data-animate=\"fadeIn\">
                            <h3 class=\"a1\" data-animate=\"fadeInDown\">{{brand.getBrandName()}} - {{brand.getBrandModel}}</h3>
                            <a data-rel=\"prettyPhoto\" href=\"{{brand.getBrandImage | raw }}\"></a>
                            <a href=\"brands/{{brand.getId()}}?circuitId={{circuit.getId()}}\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                            <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                            <a href=\"brands/{{brand.getId()}}?circuitId={{circuit.getId()}}\">{{brand.getBrandTitle}}</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  {% endfor %}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

  <div class=\"portfolio-centered\">
  <div class=\"recentitems portfolio\">
  </div>
</div>
{% endblock %}
", "MonzaBundle:Default:brands.html.twig", "C:\\Monza\\src\\MonzaBundle/Resources/views/Default/brands.html.twig");
    }
}
