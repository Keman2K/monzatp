<?php

/* @Monza/Default/CGV.html.twig */
class __TwigTemplate_452064429c8b79805e4e8b49f17698edae9e8061fdcd036cbe20eb7e010bee07 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@Monza/Default/CGV.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Monza/Default/CGV.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Monza/Default/CGV.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Conditions générales de ventes</li>
        </ul>
        <h2>CONDITIONS GÉNÉRALES DE VENTES</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>
  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
        <h2>Conditions générales de vente</h2>
          <p>Ce modèle ne se substitue pas au rôle de l’avocat. Il est donné à titre d’exemple, doit être adapté en fonction de la situation.</p>

          <p>Les présentes conditions de vente sont conclues d’une part par la société Keman Racing! au capital social de 250 000 000 € dont le siège social est situé à  , immatriculée au Registre du Commerce et des Sociétés des Yvelines. sous le numéro 012 345 678 9 ci-après dénommée \"Keman Racing!\" et gérant le site Keman Racing !, www.kemanracing.com et, d’autre part, par toute personne physique ou morale souhaitant procéder à un achat via le site internet www.kemanracing.com dénommée ci-après \" l’acheteur \".
          </p>
          <div class=\"clearfix\"></div>
          <blockquote>

            <h4>Article 1. Objet </h4>
            <p>
            Les présentes conditions de vente visent à définir les relations contractuelles entre Keman Racing! et l’acheteur et les conditions applicables à tout achat effectué par le biais du site internet www.kemanracing.com. L’acquisition d’un produit à travers le présent site implique une acceptation sans réserve par l’acheteur des présentes conditions de vente dont l’acheteur reconnaît avoir pris connaissance préalablement à sa commande. Avant toute transaction, l’acheteur déclare d’une part que l’achat de produits sur le site Keman Racing!, www.kemanracing.com est sans rapport direct avec son activité professionnelle et est limité à une utilisation strictement personnelle et d’autre part avoir la pleine capacité juridique, lui permettant de s’engager au titre des présentes conditions générales de ventes. 
            La société Keman Racing! conserve la possibilité de modifier à tout moment ces conditions de ventes, afin de respecter toute nouvelle réglementation ou dans le but d'améliorer l’utilisation de son site. De ce fait, les conditions applicables seront celles en vigueur à la date de la commande par l’acheteur.
            </p>
            <h4>Article 2. Produits </h4>
            <p>
            Les produits proposés sont ceux qui figurent sur le site www.kemanracing.com de la société Keman Racing!, dans la limite des stocks disponibles. La société Keman Racing! se réserve le droit de modifier à tout moment l’assortiment de produits. Chaque produit est présenté sur le site internet sous forme d’un descriptif reprenant ses principales caractéristiques techniques (contenance, utilisation, composition…). Les photographies sont les plus fidèles possibles mais n’engagent en rien le Vendeur. La vente des produits présentés dans le site www.kemanracing.com est destinée à tous les acheteurs résidants dans les pays qui autorisent pleinement l’entrée sur leur territoire de ces produits.
            </p>
            <h4>Article 3. Tarifs </h4>
            <p>
            Les prix figurant sur les fiches produits du catalogue internet et sont des prix en Euros (€) toutes taxes comprises (TTC) tenant compte de la TVA applicable au jour de la commande. Tout changement du taux de la TVA pourra être répercuté sur le prix des produits. La société Keman Racing! se réserve le droit de modifier ses prix à tout moment, étant toutefois entendu que le prix figurant au catalogue le jour de la commande sera le seul applicable à l’acheteur. Les prix indiqués ne comprennent pas les frais de livraison, facturés en supplément du prix des produits achetés suivant le montant total de la commande. En France métropolitaine, pour toute commande supérieure ou égale à 50 euros TTC, les frais de port sont offerts ; pour toute commande inférieure à 50 euros TTC, un forfait de participation aux frais d’expédition sera facturé à l’acheteur d'un montant de 3 euros TTC.
            </p>
            <h4>Article 4. Commande et modalités de paiement </h4>
            <p>
            Avant toute commande, l’acheteur doit créer un compte sur le site www.kemanracing.com. La rubrique de création de compte est accessible directement depuis la barre de menu latérale. A chaque visite, l’acheteur, s’il souhaite commander ou consulter son compte (état des commandes, profil…), devra s’identifier à l’aide de ces informations. La société Keman Racing! propose à l’acheteur de commander et régler ses produits en plusieurs étapes, avec 3 options de paiement au choix :
            - Paiement par chèque : l’acheteur sélectionne les produits qu’il souhaite commander dans le « panier », modifie si besoin (quantités, références…), vérifie l’adresse de livraison ou en renseigne une nouvelle. Puis, les frais de port sont calculés et soumis à l’acheteur, ainsi que le nom du transporteur. Ensuite, l’acheteur choisit le mode de paiement de son choix : « Paiement par chèque ». Enfin, la dernière étape lui propose de vérifier l’ensemble des informations, prendre connaissance et accepter les présentes conditions générales de vente en cochant la case correspondante, puis l’invite à valider sa commande en cliquant sur le bouton « Confirmer ma commande ». Ce dernier clic forme la conclusion définitive du contrat. Dès validation, l’acheteur reçoit un bon de commande confirmant l’enregistrement de sa commande. Afin de finaliser son paiement et déclencher le traitement de sa commande, l’acheteur doit imprimer ce bon de commande, accompagné de son chèque libellé à l’ordre de « Keman Racing! » à l’adresse suivante : Keman Racing!, 2B, rue Jean Macé 78360 MONTESSON. Aucun paiement en espèces ne doit être envoyé. Seuls les chèques émis par une banque française seront acceptés. Dès réception du chèque, la commande sera traitée et l’acheteur en sera informé par email. La société Keman Racing! expédiera les produits au plus tôt 2 jours ouvrés après encaissement du chèque correspondant à la commande, sous réserve de provisions.
            - Paiement par virement bancaire : l’acheteur sélectionne les produits qu’il souhaite commander dans le « panier », modifie si besoin (quantités, références…), vérifie l’adresse de livraison ou en renseigne une nouvelle. Puis, les frais de port sont calculés et soumis à l’acheteur, ainsi que le nom du transporteur. Ensuite, l’acheteur choisit le mode de paiement de son choix : « Paiement par virement ». Enfin, la dernière étape lui propose de vérifier l’ensemble des informations, prendre connaissance et accepter les présentes conditions générales de vente en cochant la case correspondante, puis l’invite à valider sa commande en cliquant sur le bouton « Confirmer ma commande ». Ce dernier clic forme la conclusion définitive du contrat. Dès validation, l’acheteur reçoit un bon de commande confirmant l’enregistrement de sa commande. Afin de finaliser son paiement et déclencher le traitement de sa commande, l’acheteur doit contacter sa banque afin d'effectuer le virement correspondant au montant de sa commande vers le compte bancaire de Keman Racing!, dont les coordonnées sont communiquées à l'acheteur. Dès réception du virement, la commande sera traitée et l’acheteur en sera informé par e-mail. La société Keman Racing! expédiera les produits au plus tôt 2 jours ouvrés après réception du virement correspondant à la commande, sous réserve de provisions.
            - Paiement sécurisé par Paypal ou carte bancaire (via le système PAYPAL) : l’acheteur sélectionne les produits qu’il souhaite commander dans le « panier », modifie si besoin, vérifie l’adresse de livraison ou en renseigne une nouvelle. Puis, les frais de port sont calculés et soumis à l’acheteur, ainsi que le nom du transporteur. Ensuite, l’acheteur choisit le mode de paiement de son choix : « Paiement par Paypal ». L’étape suivante lui propose de vérifier l’ensemble des informations, prendre connaissance et accepter les présentes conditions générales de vente en cochant la case correspondante, puis l’invite à valider sa commande en cliquant sur le bouton « Confirmer ma commande ». Enfin, l’acheteur est redirigé sur l’interface sécurisée PAYPAL afin de renseigner en toute sécurité ses références de compte Paypal ou de carte bleue personnelle. Si le paiement est accepté, la commande est enregistrée et le contrat définitivement formé. Le paiement par compte Paypal ou par carte bancaire est irrévocable. En cas d’utilisation frauduleuse de celle-ci, l’acheteur pourra exiger l’annulation du paiement par carte, les sommes versées seront alors recréditées ou restituées. La responsabilité du titulaire d’une carte bancaire n’est pas engagée si le paiement contesté a été prouvé effectué frauduleusement, à distance, sans utilisation physique de sa carte. Pour obtenir le remboursement du débit frauduleux et des éventuels frais bancaires que l’opération a pu engendrer, le porteur de la carte doit contester, par écrit, le prélèvement auprès de sa banque, dans les 70 jours suivant l’opération, voire 120 jours si le contrat le liant à celle-ci le prévoit. Les montants prélevés sont remboursés par la banque dans un délai maximum d’un mois après réception de la contestation écrite formée par le porteur. Aucun frais de restitution des sommes ne pourra être mis à la charge du titulaire.
            La confirmation d’une commande entraîne acceptation des présentes conditions de vente, la reconnaissance d’en avoir parfaite connaissance et la renonciation à se prévaloir de ses propres conditions d’achat. L’ensemble des données fournies et la confirmation enregistrée vaudront preuve de la transaction. Si l’acheteur possède une adresse électronique et s’il l’a renseignée sur son bon de commande, la société Keman Racing! lui communiquera par courrier électronique la confirmation de l’enregistrement de sa commande.
            Si l’acheteur souhaite contacter la société Keman Racing!, il peut le faire soit par courrier à l’adresse suivante : Keman Racing!, 2B, rue Jean Macé 78360 MONTESSON ; soit par email à l’adresse suivante : info@kemanracing.com, soit par téléphone au 01 12 23 45 56.
            </p>
          <h4>Article 5. Réserve de propriété </h4>
          <p>
          La société Keman Racing! conserve la propriété pleine et entière des produits vendus jusqu'au parfait encaissement du prix, en principal, frais et taxes compris.
          </p>
      </blockquote>
      <div class=\"clearfix\"></div>
       <blockquote class=\"pull-right\">
        <h4>Article 6. Rétractation </h4>
            <p>
            En vertu de l’article L121-20 du Code de la consommation, l’acheteur dispose d'un délai de quatorze jours ouvrables à compter de la livraison de leur commande pour exercer son droit de rétractation et ainsi faire retour du produit au vendeur pour échange ou remboursement sans pénalité, à l’exception des frais de retour.
            </p>
            <h4>Article 7. Livraison </h4>
            <p>
            Les livraisons sont faites à l’adresse indiquée sur le bon de commande qui ne peut être que dans la zone géographique convenue. Les commandes sont effectuées par La Poste via COLISSIMO, service de livraison avec suivi, remise sans signature. Les délais de livraison ne sont donnés qu’à titre indicatif ; si ceux-ci dépassent trente jours à compter de la commande, le contrat de vente pourra être résilié et l’acheteur remboursé. La société Keman Racing! pourra fournir par e-mail à l’acheteur le numéro de suivi de son colis. L’acheteur est livré à son domicile par son facteur. En cas d’absence de l’acheteur, il recevra un avis de passage de son facteur, ce qui lui permet de retirer les produits commandés au bureau de Poste le plus proche, pendant un délai indiqué par les services postaux. Les risques liés au transport sont à la charge de l'acquéreur à compter du moment où les articles quittent les locaux de la société Keman Racing!. L’acheteur est tenu de vérifier en présence du préposé de La Poste ou du livreur, l’état de l’emballage de la marchandise et son contenu à la livraison. En cas de dommage pendant le transport, toute protestation doit être effectuée auprès du transporteur dans un délai de trois jours à compter de la livraison.
            </p>
            <h4>Article 8. Garantie </h4>
            <p>
            Tous les produits fournis par la société Keman Racing! bénéficient de la garantie légale prévue par les articles 1641 et suivants du Code civil. En cas de non conformité d’un produit vendu, il pourra être retourné à la société Keman Racing! qui le reprendra, l’échangera ou le remboursera. Toutes les réclamations, demandes d’échange ou de remboursement doivent s’effectuer par voie postale à l’adresse suivante : Keman Racing!, 2B, rue Jean Macé 78360 MONTESSON, dans un délai de trente jours après livraison.
            </p>
            <h4>Article 9. Responsabilité </h4>
            <p>
            La société Keman Racing!, dans le processus de vente à distance, n’est tenue que par une obligation de moyens. Sa responsabilité ne pourra être engagée pour un dommage résultant de l’utilisation du réseau Internet tel que perte de données, intrusion, virus, rupture du service, ou autres problèmes involontaires.
            </p>
            <h4>Article 10. Propriété intellectuelle </h4>
            <p>
            Tous les éléments du site www.kemanracing.com sont et restent la propriété intellectuelle et exclusive de la société Keman Racing!. Personne n’est autorisé à reproduire, exploiter, ou utiliser à quelque titre que ce soit, même partiellement, des éléments du site qu’ils soient sous forme de photo, logo, visuel ou texte.
            </p>
            <h4>Article 11. Données à caractère personnel</h4>
            <p>
            La société Keman Racing! s'engage à préserver la confidentialité des informations fournies par l’acheteur, qu'il serait amené à transmettre pour l'utilisation de certains services. Toute information le concernant est soumise aux dispositions de la loi n° 78-17 du 6 janvier 1978. A ce titre, l'internaute dispose d'un droit d'accès, de modification et de suppression des informations le concernant. Il peut en faire la demande à tout moment par courrier à l’adresse suivante : Keman Racing!, 2B, rue Jean Macé 78360 MONTESSON.
            </p>
            <h4>Article 12. Règlement des litiges </h4>
            <p>
            Les présentes conditions de vente à distance sont soumises à la loi française. Pour tous litiges ou contentieux, le Tribunal compétent sera celui de Saint-Germain-En-Laye.
            </p>
              </blockquote>
          </div>
  </section>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@Monza/Default/CGV.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Conditions générales de ventes</li>
        </ul>
        <h2>CONDITIONS GÉNÉRALES DE VENTES</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>
  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
        <h2>Conditions générales de vente</h2>
          <p>Ce modèle ne se substitue pas au rôle de l’avocat. Il est donné à titre d’exemple, doit être adapté en fonction de la situation.</p>

          <p>Les présentes conditions de vente sont conclues d’une part par la société Keman Racing! au capital social de 250 000 000 € dont le siège social est situé à  , immatriculée au Registre du Commerce et des Sociétés des Yvelines. sous le numéro 012 345 678 9 ci-après dénommée \"Keman Racing!\" et gérant le site Keman Racing !, www.kemanracing.com et, d’autre part, par toute personne physique ou morale souhaitant procéder à un achat via le site internet www.kemanracing.com dénommée ci-après \" l’acheteur \".
          </p>
          <div class=\"clearfix\"></div>
          <blockquote>

            <h4>Article 1. Objet </h4>
            <p>
            Les présentes conditions de vente visent à définir les relations contractuelles entre Keman Racing! et l’acheteur et les conditions applicables à tout achat effectué par le biais du site internet www.kemanracing.com. L’acquisition d’un produit à travers le présent site implique une acceptation sans réserve par l’acheteur des présentes conditions de vente dont l’acheteur reconnaît avoir pris connaissance préalablement à sa commande. Avant toute transaction, l’acheteur déclare d’une part que l’achat de produits sur le site Keman Racing!, www.kemanracing.com est sans rapport direct avec son activité professionnelle et est limité à une utilisation strictement personnelle et d’autre part avoir la pleine capacité juridique, lui permettant de s’engager au titre des présentes conditions générales de ventes. 
            La société Keman Racing! conserve la possibilité de modifier à tout moment ces conditions de ventes, afin de respecter toute nouvelle réglementation ou dans le but d'améliorer l’utilisation de son site. De ce fait, les conditions applicables seront celles en vigueur à la date de la commande par l’acheteur.
            </p>
            <h4>Article 2. Produits </h4>
            <p>
            Les produits proposés sont ceux qui figurent sur le site www.kemanracing.com de la société Keman Racing!, dans la limite des stocks disponibles. La société Keman Racing! se réserve le droit de modifier à tout moment l’assortiment de produits. Chaque produit est présenté sur le site internet sous forme d’un descriptif reprenant ses principales caractéristiques techniques (contenance, utilisation, composition…). Les photographies sont les plus fidèles possibles mais n’engagent en rien le Vendeur. La vente des produits présentés dans le site www.kemanracing.com est destinée à tous les acheteurs résidants dans les pays qui autorisent pleinement l’entrée sur leur territoire de ces produits.
            </p>
            <h4>Article 3. Tarifs </h4>
            <p>
            Les prix figurant sur les fiches produits du catalogue internet et sont des prix en Euros (€) toutes taxes comprises (TTC) tenant compte de la TVA applicable au jour de la commande. Tout changement du taux de la TVA pourra être répercuté sur le prix des produits. La société Keman Racing! se réserve le droit de modifier ses prix à tout moment, étant toutefois entendu que le prix figurant au catalogue le jour de la commande sera le seul applicable à l’acheteur. Les prix indiqués ne comprennent pas les frais de livraison, facturés en supplément du prix des produits achetés suivant le montant total de la commande. En France métropolitaine, pour toute commande supérieure ou égale à 50 euros TTC, les frais de port sont offerts ; pour toute commande inférieure à 50 euros TTC, un forfait de participation aux frais d’expédition sera facturé à l’acheteur d'un montant de 3 euros TTC.
            </p>
            <h4>Article 4. Commande et modalités de paiement </h4>
            <p>
            Avant toute commande, l’acheteur doit créer un compte sur le site www.kemanracing.com. La rubrique de création de compte est accessible directement depuis la barre de menu latérale. A chaque visite, l’acheteur, s’il souhaite commander ou consulter son compte (état des commandes, profil…), devra s’identifier à l’aide de ces informations. La société Keman Racing! propose à l’acheteur de commander et régler ses produits en plusieurs étapes, avec 3 options de paiement au choix :
            - Paiement par chèque : l’acheteur sélectionne les produits qu’il souhaite commander dans le « panier », modifie si besoin (quantités, références…), vérifie l’adresse de livraison ou en renseigne une nouvelle. Puis, les frais de port sont calculés et soumis à l’acheteur, ainsi que le nom du transporteur. Ensuite, l’acheteur choisit le mode de paiement de son choix : « Paiement par chèque ». Enfin, la dernière étape lui propose de vérifier l’ensemble des informations, prendre connaissance et accepter les présentes conditions générales de vente en cochant la case correspondante, puis l’invite à valider sa commande en cliquant sur le bouton « Confirmer ma commande ». Ce dernier clic forme la conclusion définitive du contrat. Dès validation, l’acheteur reçoit un bon de commande confirmant l’enregistrement de sa commande. Afin de finaliser son paiement et déclencher le traitement de sa commande, l’acheteur doit imprimer ce bon de commande, accompagné de son chèque libellé à l’ordre de « Keman Racing! » à l’adresse suivante : Keman Racing!, 2B, rue Jean Macé 78360 MONTESSON. Aucun paiement en espèces ne doit être envoyé. Seuls les chèques émis par une banque française seront acceptés. Dès réception du chèque, la commande sera traitée et l’acheteur en sera informé par email. La société Keman Racing! expédiera les produits au plus tôt 2 jours ouvrés après encaissement du chèque correspondant à la commande, sous réserve de provisions.
            - Paiement par virement bancaire : l’acheteur sélectionne les produits qu’il souhaite commander dans le « panier », modifie si besoin (quantités, références…), vérifie l’adresse de livraison ou en renseigne une nouvelle. Puis, les frais de port sont calculés et soumis à l’acheteur, ainsi que le nom du transporteur. Ensuite, l’acheteur choisit le mode de paiement de son choix : « Paiement par virement ». Enfin, la dernière étape lui propose de vérifier l’ensemble des informations, prendre connaissance et accepter les présentes conditions générales de vente en cochant la case correspondante, puis l’invite à valider sa commande en cliquant sur le bouton « Confirmer ma commande ». Ce dernier clic forme la conclusion définitive du contrat. Dès validation, l’acheteur reçoit un bon de commande confirmant l’enregistrement de sa commande. Afin de finaliser son paiement et déclencher le traitement de sa commande, l’acheteur doit contacter sa banque afin d'effectuer le virement correspondant au montant de sa commande vers le compte bancaire de Keman Racing!, dont les coordonnées sont communiquées à l'acheteur. Dès réception du virement, la commande sera traitée et l’acheteur en sera informé par e-mail. La société Keman Racing! expédiera les produits au plus tôt 2 jours ouvrés après réception du virement correspondant à la commande, sous réserve de provisions.
            - Paiement sécurisé par Paypal ou carte bancaire (via le système PAYPAL) : l’acheteur sélectionne les produits qu’il souhaite commander dans le « panier », modifie si besoin, vérifie l’adresse de livraison ou en renseigne une nouvelle. Puis, les frais de port sont calculés et soumis à l’acheteur, ainsi que le nom du transporteur. Ensuite, l’acheteur choisit le mode de paiement de son choix : « Paiement par Paypal ». L’étape suivante lui propose de vérifier l’ensemble des informations, prendre connaissance et accepter les présentes conditions générales de vente en cochant la case correspondante, puis l’invite à valider sa commande en cliquant sur le bouton « Confirmer ma commande ». Enfin, l’acheteur est redirigé sur l’interface sécurisée PAYPAL afin de renseigner en toute sécurité ses références de compte Paypal ou de carte bleue personnelle. Si le paiement est accepté, la commande est enregistrée et le contrat définitivement formé. Le paiement par compte Paypal ou par carte bancaire est irrévocable. En cas d’utilisation frauduleuse de celle-ci, l’acheteur pourra exiger l’annulation du paiement par carte, les sommes versées seront alors recréditées ou restituées. La responsabilité du titulaire d’une carte bancaire n’est pas engagée si le paiement contesté a été prouvé effectué frauduleusement, à distance, sans utilisation physique de sa carte. Pour obtenir le remboursement du débit frauduleux et des éventuels frais bancaires que l’opération a pu engendrer, le porteur de la carte doit contester, par écrit, le prélèvement auprès de sa banque, dans les 70 jours suivant l’opération, voire 120 jours si le contrat le liant à celle-ci le prévoit. Les montants prélevés sont remboursés par la banque dans un délai maximum d’un mois après réception de la contestation écrite formée par le porteur. Aucun frais de restitution des sommes ne pourra être mis à la charge du titulaire.
            La confirmation d’une commande entraîne acceptation des présentes conditions de vente, la reconnaissance d’en avoir parfaite connaissance et la renonciation à se prévaloir de ses propres conditions d’achat. L’ensemble des données fournies et la confirmation enregistrée vaudront preuve de la transaction. Si l’acheteur possède une adresse électronique et s’il l’a renseignée sur son bon de commande, la société Keman Racing! lui communiquera par courrier électronique la confirmation de l’enregistrement de sa commande.
            Si l’acheteur souhaite contacter la société Keman Racing!, il peut le faire soit par courrier à l’adresse suivante : Keman Racing!, 2B, rue Jean Macé 78360 MONTESSON ; soit par email à l’adresse suivante : info@kemanracing.com, soit par téléphone au 01 12 23 45 56.
            </p>
          <h4>Article 5. Réserve de propriété </h4>
          <p>
          La société Keman Racing! conserve la propriété pleine et entière des produits vendus jusqu'au parfait encaissement du prix, en principal, frais et taxes compris.
          </p>
      </blockquote>
      <div class=\"clearfix\"></div>
       <blockquote class=\"pull-right\">
        <h4>Article 6. Rétractation </h4>
            <p>
            En vertu de l’article L121-20 du Code de la consommation, l’acheteur dispose d'un délai de quatorze jours ouvrables à compter de la livraison de leur commande pour exercer son droit de rétractation et ainsi faire retour du produit au vendeur pour échange ou remboursement sans pénalité, à l’exception des frais de retour.
            </p>
            <h4>Article 7. Livraison </h4>
            <p>
            Les livraisons sont faites à l’adresse indiquée sur le bon de commande qui ne peut être que dans la zone géographique convenue. Les commandes sont effectuées par La Poste via COLISSIMO, service de livraison avec suivi, remise sans signature. Les délais de livraison ne sont donnés qu’à titre indicatif ; si ceux-ci dépassent trente jours à compter de la commande, le contrat de vente pourra être résilié et l’acheteur remboursé. La société Keman Racing! pourra fournir par e-mail à l’acheteur le numéro de suivi de son colis. L’acheteur est livré à son domicile par son facteur. En cas d’absence de l’acheteur, il recevra un avis de passage de son facteur, ce qui lui permet de retirer les produits commandés au bureau de Poste le plus proche, pendant un délai indiqué par les services postaux. Les risques liés au transport sont à la charge de l'acquéreur à compter du moment où les articles quittent les locaux de la société Keman Racing!. L’acheteur est tenu de vérifier en présence du préposé de La Poste ou du livreur, l’état de l’emballage de la marchandise et son contenu à la livraison. En cas de dommage pendant le transport, toute protestation doit être effectuée auprès du transporteur dans un délai de trois jours à compter de la livraison.
            </p>
            <h4>Article 8. Garantie </h4>
            <p>
            Tous les produits fournis par la société Keman Racing! bénéficient de la garantie légale prévue par les articles 1641 et suivants du Code civil. En cas de non conformité d’un produit vendu, il pourra être retourné à la société Keman Racing! qui le reprendra, l’échangera ou le remboursera. Toutes les réclamations, demandes d’échange ou de remboursement doivent s’effectuer par voie postale à l’adresse suivante : Keman Racing!, 2B, rue Jean Macé 78360 MONTESSON, dans un délai de trente jours après livraison.
            </p>
            <h4>Article 9. Responsabilité </h4>
            <p>
            La société Keman Racing!, dans le processus de vente à distance, n’est tenue que par une obligation de moyens. Sa responsabilité ne pourra être engagée pour un dommage résultant de l’utilisation du réseau Internet tel que perte de données, intrusion, virus, rupture du service, ou autres problèmes involontaires.
            </p>
            <h4>Article 10. Propriété intellectuelle </h4>
            <p>
            Tous les éléments du site www.kemanracing.com sont et restent la propriété intellectuelle et exclusive de la société Keman Racing!. Personne n’est autorisé à reproduire, exploiter, ou utiliser à quelque titre que ce soit, même partiellement, des éléments du site qu’ils soient sous forme de photo, logo, visuel ou texte.
            </p>
            <h4>Article 11. Données à caractère personnel</h4>
            <p>
            La société Keman Racing! s'engage à préserver la confidentialité des informations fournies par l’acheteur, qu'il serait amené à transmettre pour l'utilisation de certains services. Toute information le concernant est soumise aux dispositions de la loi n° 78-17 du 6 janvier 1978. A ce titre, l'internaute dispose d'un droit d'accès, de modification et de suppression des informations le concernant. Il peut en faire la demande à tout moment par courrier à l’adresse suivante : Keman Racing!, 2B, rue Jean Macé 78360 MONTESSON.
            </p>
            <h4>Article 12. Règlement des litiges </h4>
            <p>
            Les présentes conditions de vente à distance sont soumises à la loi française. Pour tous litiges ou contentieux, le Tribunal compétent sera celui de Saint-Germain-En-Laye.
            </p>
              </blockquote>
          </div>
  </section>

{% endblock %}", "@Monza/Default/CGV.html.twig", "C:\\Monza\\src\\MonzaBundle\\Resources\\views\\Default\\CGV.html.twig");
    }
}
