<?php

/* @Monza/Default/brand.html.twig */
class __TwigTemplate_df6245fa6ec5ceb131d6dba46eb941075e2f17bd77667c5444122c682382f738 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@Monza/Default/brand.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Monza/Default/brand.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Monza/Default/brand.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>";
        // line 9
        echo $this->getAttribute(($context["brand"] ?? $this->getContext($context, "brand")), "getBrandName", array(), "method");
        echo " ";
        echo $this->getAttribute(($context["brand"] ?? $this->getContext($context, "brand")), "getBrandModel", array(), "method");
        echo "</li>
        </ul>
        <h2>";
        // line 11
        echo $this->getAttribute(($context["brand"] ?? $this->getContext($context, "brand")), "getBrandName", array(), "method");
        echo " ";
        echo $this->getAttribute(($context["brand"] ?? $this->getContext($context, "brand")), "getBrandModel", array(), "method");
        echo ", vous avez choisi le circuit ";
        echo $this->getAttribute(($context["circuit"] ?? $this->getContext($context, "circuit")), "getCircuitName", array(), "method");
        echo "</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>
    <section class=\"marketplace-top\">
    <div id=\"market-wrapper\">
      <div class=\"item_image\">
         <img data-effect=\"fade\" class=\"aligncenter\" width=\"600\" height=\"400\" src=\"";
        // line 28
        echo $this->getAttribute(($context["brand"] ?? $this->getContext($context, "brand")), "getbrandImage", array());
        echo "\">
      </div>
    </div>
  </section>
  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
        <div class=\"general-title text-center\">
          <h3> ";
        // line 36
        echo $this->getAttribute(($context["brand"] ?? $this->getContext($context, "brand")), "getBrandName", array(), "method");
        echo " ";
        echo $this->getAttribute(($context["brand"] ?? $this->getContext($context, "brand")), "getbrandModel", array(), "method");
        echo "</h3>
          <p>";
        // line 37
        echo $this->getAttribute(($context["brand"] ?? $this->getContext($context, "brand")), "getBrandTitle", array());
        echo " </p>
          <hr>
        </div>
        <div class=\"divider\"></div>
        <div class=\"item_details\">
          <div class=\"col-lg-3 col-md-3 col-sm-12\">
            <div class=\"theme_details\">
              <div class=\"details_section\">
                <h3>En détails : </h3>
                <ul>
                  <li class=\"version\"> Puissance : <span>";
        // line 47
        echo $this->getAttribute(($context["brand"] ?? $this->getContext($context, "brand")), "getbrandPower", array(), "method");
        echo "</span></li>
                  <li class=\"designer\"> 0 - 100 : <span>";
        // line 48
        echo $this->getAttribute(($context["brand"] ?? $this->getContext($context, "brand")), "getbrandChrono", array(), "method");
        echo "</span></li>
                  <li class=\"release\"> Vitesse Max : <span>";
        // line 49
        echo $this->getAttribute(($context["brand"] ?? $this->getContext($context, "brand")), "getbrandVmax", array(), "method");
        echo "</span></li>
                </ul>
              </div>
            </div>
          </div>
      <div class=\"col-lg-6 col-md-6 col-sm-12\">
        <div class=\"theme_details\">
          <div class=\"item-description\">
            ";
        // line 57
        echo $this->getAttribute(($context["brand"] ?? $this->getContext($context, "brand")), "getBrandHistory", array());
        echo "
          </div>
        </div>
      </div>
      <div class=\"col-lg-3 col-md-3 col-sm-12\">
        <div class=\"theme_details\">
          <div class=\"item_price\">
            <h4>Son prix est de ";
        // line 64
        echo $this->getAttribute(($context["brand"] ?? $this->getContext($context, "brand")), "getBrandPrix", array());
        echo " €</h4>
          </div>
              <hr>
              <div class=\"buttons\">
                <a class=\"button btn-block large\" href=\"/panier?circuitId=";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute(($context["circuit"] ?? $this->getContext($context, "circuit")), "getId", array(), "method"), "html", null, true);
        echo "&brandId=";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["brand"] ?? $this->getContext($context, "brand")), "getId", array(), "method"), "html", null, true);
        echo "\"></i>Continuer vers le panier</a>
              </div>
              <hr>
            </div>
          </div>
        </div>
    <div class=\"clearfix\"></div>
</div>
</div>
</section>
  <div class=\"item\">
    <div id=\"owl-demo\" class=\"owl-carousel\">

  ";
        // line 81
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 9));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 82
            echo "  <a data-rel=\"prettyPhoto\" href=\"";
            echo twig_escape_filter($this->env, (($this->getAttribute(($context["brand"] ?? $this->getContext($context, "brand")), "getBrandImages", array()) . $context["i"]) . ".jpg"), "html", null, true);
            echo "\">
    <div class=\"item\">
        <img src=\"";
            // line 84
            echo twig_escape_filter($this->env, (($this->getAttribute(($context["brand"] ?? $this->getContext($context, "brand")), "getBrandImages", array()) . $context["i"]) . ".jpg"), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["brand"] ?? $this->getContext($context, "brand")), "getBrandModel", array()), "html", null, true);
            echo "\">
    </div>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 87
        echo "      </a>
  </div>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@Monza/Default/brand.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 87,  181 => 84,  175 => 82,  171 => 81,  153 => 68,  146 => 64,  136 => 57,  125 => 49,  121 => 48,  117 => 47,  104 => 37,  98 => 36,  87 => 28,  63 => 11,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>{{ brand.getBrandName() | raw }} {{ brand.getBrandModel() | raw }}</li>
        </ul>
        <h2>{{ brand.getBrandName() | raw }} {{ brand.getBrandModel() | raw }}, vous avez choisi le circuit {{ circuit.getCircuitName() | raw }}</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>
    <section class=\"marketplace-top\">
    <div id=\"market-wrapper\">
      <div class=\"item_image\">
         <img data-effect=\"fade\" class=\"aligncenter\" width=\"600\" height=\"400\" src=\"{{ brand.getbrandImage | raw }}\">
      </div>
    </div>
  </section>
  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
        <div class=\"general-title text-center\">
          <h3> {{ brand.getBrandName() | raw }} {{ brand.getbrandModel() | raw }}</h3>
          <p>{{ brand.getBrandTitle | raw }} </p>
          <hr>
        </div>
        <div class=\"divider\"></div>
        <div class=\"item_details\">
          <div class=\"col-lg-3 col-md-3 col-sm-12\">
            <div class=\"theme_details\">
              <div class=\"details_section\">
                <h3>En détails : </h3>
                <ul>
                  <li class=\"version\"> Puissance : <span>{{ brand.getbrandPower() | raw }}</span></li>
                  <li class=\"designer\"> 0 - 100 : <span>{{ brand.getbrandChrono() | raw }}</span></li>
                  <li class=\"release\"> Vitesse Max : <span>{{ brand.getbrandVmax() | raw }}</span></li>
                </ul>
              </div>
            </div>
          </div>
      <div class=\"col-lg-6 col-md-6 col-sm-12\">
        <div class=\"theme_details\">
          <div class=\"item-description\">
            {{brand.getBrandHistory | raw }}
          </div>
        </div>
      </div>
      <div class=\"col-lg-3 col-md-3 col-sm-12\">
        <div class=\"theme_details\">
          <div class=\"item_price\">
            <h4>Son prix est de {{ brand.getBrandPrix | raw }} €</h4>
          </div>
              <hr>
              <div class=\"buttons\">
                <a class=\"button btn-block large\" href=\"/panier?circuitId={{circuit.getId()}}&brandId={{brand.getId()}}\"></i>Continuer vers le panier</a>
              </div>
              <hr>
            </div>
          </div>
        </div>
    <div class=\"clearfix\"></div>
</div>
</div>
</section>
  <div class=\"item\">
    <div id=\"owl-demo\" class=\"owl-carousel\">

  {% for i in 1..9 %}
  <a data-rel=\"prettyPhoto\" href=\"{{brand.getBrandImages ~ i ~ '.jpg' }}\">
    <div class=\"item\">
        <img src=\"{{brand.getBrandImages ~ i ~ '.jpg' }}\" alt=\"{{brand.getBrandModel}}\">
    </div>
  {% endfor %}
      </a>
  </div>
</div>

{% endblock %}", "@Monza/Default/brand.html.twig", "C:\\Monza\\src\\MonzaBundle\\Resources\\views\\Default\\brand.html.twig");
    }
}
