<?php

/* ::base.html.twig */
class __TwigTemplate_be5209feac9f6b83204900f1e2e35d6cddfb018059d1ed42a7812d8b256d3404 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr\">
<head>
  <meta charset=\"utf-8\">
  <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
  ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 23
        echo "
";
        // line 24
        $this->displayBlock('header', $context, $blocks);
        // line 91
        $this->displayBlock('body', $context, $blocks);
        // line 94
        echo "
";
        // line 95
        $this->displayBlock('footer', $context, $blocks);
        // line 153
        echo "
";
        // line 154
        $this->displayBlock('javascripts', $context, $blocks);
        // line 172
        echo "    </body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Bienvenue chez Keman Racing !";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 7
        echo "  <meta content=\"width=device-width, initial-scale=1.0\" name=\"viewport\">
  <meta content=\"\" name=\"keywords\">
  <meta content=\"\" name=\"description\">
  <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/img/logo.png"), "html", null, true);
        echo "\" rel=\"icon\">
  <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.6.3/css/all.css\" integrity=\"sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/\" crossorigin=\"anonymous\">
  <link href=\"https://fonts.googleapis.com/css?family=Ruda:400,900,700\" rel=\"stylesheet\">
  <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
  <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
  <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/prettyphoto/css/prettyphoto.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
  <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/hover/hoverex-all.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
  <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/jetmenu/jetmenu.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
  <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/owl-carousel/owl-carousel.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
  <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
  <link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/css/colors/blue.css"), "html", null, true);
        echo "\">
</head>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 24
    public function block_header($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 25
        echo "  <div class=\"topbar clearfix\">
    <div class=\"container\">
      <div class=\"col-lg-12 text-right\">
        <div class=\"social_buttons\">
          <a href=\"https://fr-fr.facebook.com/\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Facebook\"><i class=\"fa fa-facebook\"></i></a>
          <a href=\"https://twitter.com/?lang=fr\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Twitter\"><i class=\"fa fa-twitter\"></i></a>
          <a href=\"https://plus.google.com/discover\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Google+\"><i class=\"fa fa-google-plus\"></i></a>
          <a href=\"https://dribbble.com/\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Dribbble\"><i class=\"fa fa-dribbble\"></i></a>
          <a href=\"https://fr.wikipedia.org/wiki/RSS\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"RSS\"><i class=\"fa fa-rss\"></i></a>
        </div>
      </div>
    </div>
  </div>

  <header class=\"header\">
    <div class=\"container\">
      <div class=\"site-header clearfix\">
        <div class=\"col-lg-3 col-md-3 col-sm-12 title-area\">
          <div class=\"site-title\" id=\"title\">
            <a href=\"/\" title=\"\">
              <h4>Keman<span>Racing !</span></h4>
            </a>
          </div>
        </div>
        <div class=\"col-lg-9 col-md-12 col-sm-12\">
          <div id=\"nav\" class=\"right\">
            <div class=\"container clearfix\">
              <ul id=\"jetmenu\" class=\"jetmenu blue\">
                <li class=\"active\"><a href=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/"), "html", null, true);
        echo "\">Acceuil</a>
                </li>
                <li><a href=\"/\">Nous connaître</a>
                  <ul class=\"dropdown\">
                    <li><a href=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/qsn"), "html", null, true);
        echo "\">Qui sommes-nous ?</a></li>
                    <li><a href=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/services"), "html", null, true);
        echo "\">Nos services</a></li>
                    <li><a href=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/equipe"), "html", null, true);
        echo "\">Notre équipe</a></li>
                    <li><a href=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/faq"), "html", null, true);
        echo "\">FAQ</a></li>
                    <li><a href=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/contact"), "html", null, true);
        echo "\">Nous Contacter</a></li>
                    ";
        // line 62
        if ($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array())) {
            // line 63
            echo "                    <li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/avis"), "html", null, true);
            echo "\">Déposer un avis</a></li>
                    ";
        }
        // line 65
        echo "                  </ul>
                </li>
                <li><a href=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/register"), "html", null, true);
        echo "\">Votre compte</a>
                  <ul class=\"dropdown\">
                    ";
        // line 69
        if ($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array())) {
            // line 70
            echo "                    ";
        } elseif ( !$this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array())) {
            // line 71
            echo "                    <li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/login"), "html", null, true);
            echo "\">Connexion</a></li>
                    <li><a href=\"";
            // line 72
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/register"), "html", null, true);
            echo "\">Inscription</a></li>
                    ";
        }
        // line 74
        echo "                    ";
        if ($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array())) {
            // line 75
            echo "                      <li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/panier"), "html", null, true);
            echo "\">Votre panier</a></li>
                      <li><a href=\"";
            // line 76
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/panier"), "html", null, true);
            echo "\">Votre panier</a></li>
                      <li><a href=\"";
            // line 77
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/compte"), "html", null, true);
            echo "\">Mon compte</a></li>
                      <li><a href=\"";
            // line 78
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/logout"), "html", null, true);
            echo "\">Deconnexion</a></li>
                    ";
        } elseif ( !$this->getAttribute(        // line 79
($context["app"] ?? $this->getContext($context, "app")), "user", array())) {
            // line 80
            echo "                    ";
        }
        // line 81
        echo "                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 91
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 92
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 95
    public function block_footer($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 96
        echo "  <footer class=\"footer\">
    <div class=\"container\">
      <div class=\"widget col-lg-3 col-md-3 col-sm-12\">
        <h4 class=\"title\">A propos de nous</h4>
        <p>Keman Racing ! est une entreprise fictive dans le but d'un projet noté. Rien de tout ça est vrai !</p>
      </div>
      <div class=\"widget col-lg-3 col-md-3 col-sm-12\">
        <h4 class=\"title\">Dernières nouveautés</h4>
        <ul class=\"recent_posts\">
          <li>
            <a href=\"";
        // line 106
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/home1"), "html", null, true);
        echo "\">
            <img src=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/recent_post_01.png"), "html", null, true);
        echo "\" alt=\"mobile\" /> Bientôt une nouvelle application mobile !</a>
            <a class=\"readmore\" href=\"";
        // line 108
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/#"), "html", null, true);
        echo "\">En savoir plus</a>
          </li>
          <li>
            <a href=\"";
        // line 111
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/home1"), "html", null, true);
        echo "#\">
            <img src=\"";
        // line 112
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/recent_post_02.png"), "html", null, true);
        echo "\" alt=\"Nouvelles\" /> Bientôt de nouveaux modèles !</a>
            <a class=\"readmore\" href=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/nouvelles"), "html", null, true);
        echo "\">En savoir plus</a>
          </li>
        </ul>
      </div>
      <div class=\"widget col-lg-3 col-md-3 col-sm-12\">
        <h4 class=\"title\"> Restons en contact</h4>
        <ul class=\"contact_details\">
          <li><i class=\"fa fa-envelope-o\"></i> info@kemanracing.com</li>
          <li><i class=\"fa fa-phone-square\"></i> 01 12 23 45 56</li>
          <li><i class=\"fa fa-home\"></i> Siège social, 2 rue Jean Macé, 78360 Montesson</li>
        </ul>
      </div>
      <div class=\"widget col-lg-3 col-md-3 col-sm-12\">
        <h4 class=\"title\">Nous trouver</h4>
<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d655.5617232372005!2d2.1535200292831567!3d48.91068819870577!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e663b1dbf34669%3A0x68a58f40089ea408!2s2+Rue+Jean+Mac%C3%A9%2C+78360+Montesson!5e0!3m2!1sfr!2sfr!4v1549376809503\" width=\"250\" height=\"200\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
      </div>
    </div>
    <div class=\"copyrights\">
      <div class=\"container\">
        <div class=\"col-lg-6 col-md-6 col-sm-12 columns footer-left\">
          <p>Copyright © 2018 - Tous droits réservés.</p>
          <div class=\"credits\">
            Crée pour un examen à l'<a href=\"https://www.ifocop.fr/\" target=\"blank\">IFOCOP</a>, PARIS.
          </div>
        </div>
        <div class=\"col-lg-6 col-md-6 col-sm-12 columns text-right\">
          <div class=\"footer-menu right\">
            <ul class=\"menu\">
              <li><a href=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/"), "html", null, true);
        echo "\">Acceuil</a></li>
              <li><a href=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/qsn"), "html", null, true);
        echo "\">A propos</a></li>
              <li><a href=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/plan"), "html", null, true);
        echo "\">Plan du site</a></li>
              <li><a href=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/CGV"), "html", null, true);
        echo "\">CGV</a></li>
              <li><a href=\"";
        // line 145
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/contact"), "html", null, true);
        echo "\">Contact</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 154
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 155
        echo "  <div class=\"dmtop\">Scroll to Top</div>
  <script src=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/jquery/jquery.min.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 158
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/php-mail-form/validate.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/prettyphoto/js/prettyphoto.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/isotope/isotope.min.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 161
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/hover/hoverdir.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 162
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/hover/hoverex.min.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/unveil-effects/unveil-effects.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 164
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/owl-carousel/owl-carousel.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/jetmenu/jetmenu.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 166
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/animate-enhanced/animate-enhanced.min.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/jigowatt/jigowatt.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/lib/easypiechart/easypiechart.min.js"), "html", null, true);
        echo "\"></script>

  <script src=\"";
        // line 170
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/assets/js/main.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  486 => 170,  481 => 168,  477 => 167,  473 => 166,  469 => 165,  465 => 164,  461 => 163,  457 => 162,  453 => 161,  449 => 160,  445 => 159,  441 => 158,  437 => 157,  433 => 156,  430 => 155,  421 => 154,  403 => 145,  399 => 144,  395 => 143,  391 => 142,  387 => 141,  356 => 113,  352 => 112,  348 => 111,  342 => 108,  338 => 107,  334 => 106,  322 => 96,  313 => 95,  302 => 92,  293 => 91,  274 => 81,  271 => 80,  269 => 79,  265 => 78,  261 => 77,  257 => 76,  252 => 75,  249 => 74,  244 => 72,  239 => 71,  236 => 70,  234 => 69,  229 => 67,  225 => 65,  219 => 63,  217 => 62,  213 => 61,  209 => 60,  205 => 59,  201 => 58,  197 => 57,  190 => 53,  160 => 25,  151 => 24,  138 => 20,  134 => 19,  130 => 18,  126 => 17,  122 => 16,  118 => 15,  114 => 14,  110 => 13,  104 => 10,  99 => 7,  90 => 6,  72 => 5,  60 => 172,  58 => 154,  55 => 153,  53 => 95,  50 => 94,  48 => 91,  46 => 24,  43 => 23,  41 => 6,  37 => 5,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"fr\">
<head>
  <meta charset=\"utf-8\">
  <title>{% block title %}Bienvenue chez Keman Racing !{% endblock %}</title>
  {% block stylesheets %}
  <meta content=\"width=device-width, initial-scale=1.0\" name=\"viewport\">
  <meta content=\"\" name=\"keywords\">
  <meta content=\"\" name=\"description\">
  <link href=\"{{ asset('/assets/img/logo.png') }}\" rel=\"icon\">
  <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.6.3/css/all.css\" integrity=\"sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/\" crossorigin=\"anonymous\">
  <link href=\"https://fonts.googleapis.com/css?family=Ruda:400,900,700\" rel=\"stylesheet\">
  <link href=\"{{ asset('/assets/lib/bootstrap/css/bootstrap.min.css') }}\" rel=\"stylesheet\">
  <link href=\"{{ asset('/assets/lib/font-awesome/css/font-awesome.min.css') }}\" rel=\"stylesheet\">
  <link href=\"{{ asset('/assets/lib/prettyphoto/css/prettyphoto.css') }}\" rel=\"stylesheet\">
  <link href=\"{{ asset('/assets/lib/hover/hoverex-all.css') }}\" rel=\"stylesheet\">
  <link href=\"{{ asset('/assets/lib/jetmenu/jetmenu.css') }}\" rel=\"stylesheet\">
  <link href=\"{{ asset('/assets/lib/owl-carousel/owl-carousel.css') }}\" rel=\"stylesheet\">
  <link href=\"{{ asset('/assets/css/style.css') }}\" rel=\"stylesheet\">
  <link rel=\"stylesheet\" href=\"{{ asset('/assets/css/colors/blue.css') }}\">
</head>
{% endblock %}

{% block header %}
  <div class=\"topbar clearfix\">
    <div class=\"container\">
      <div class=\"col-lg-12 text-right\">
        <div class=\"social_buttons\">
          <a href=\"https://fr-fr.facebook.com/\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Facebook\"><i class=\"fa fa-facebook\"></i></a>
          <a href=\"https://twitter.com/?lang=fr\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Twitter\"><i class=\"fa fa-twitter\"></i></a>
          <a href=\"https://plus.google.com/discover\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Google+\"><i class=\"fa fa-google-plus\"></i></a>
          <a href=\"https://dribbble.com/\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Dribbble\"><i class=\"fa fa-dribbble\"></i></a>
          <a href=\"https://fr.wikipedia.org/wiki/RSS\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"RSS\"><i class=\"fa fa-rss\"></i></a>
        </div>
      </div>
    </div>
  </div>

  <header class=\"header\">
    <div class=\"container\">
      <div class=\"site-header clearfix\">
        <div class=\"col-lg-3 col-md-3 col-sm-12 title-area\">
          <div class=\"site-title\" id=\"title\">
            <a href=\"/\" title=\"\">
              <h4>Keman<span>Racing !</span></h4>
            </a>
          </div>
        </div>
        <div class=\"col-lg-9 col-md-12 col-sm-12\">
          <div id=\"nav\" class=\"right\">
            <div class=\"container clearfix\">
              <ul id=\"jetmenu\" class=\"jetmenu blue\">
                <li class=\"active\"><a href=\"{{ asset('/') }}\">Acceuil</a>
                </li>
                <li><a href=\"/\">Nous connaître</a>
                  <ul class=\"dropdown\">
                    <li><a href=\"{{ asset('/qsn') }}\">Qui sommes-nous ?</a></li>
                    <li><a href=\"{{ asset('/services') }}\">Nos services</a></li>
                    <li><a href=\"{{ asset('/equipe') }}\">Notre équipe</a></li>
                    <li><a href=\"{{ asset('/faq') }}\">FAQ</a></li>
                    <li><a href=\"{{ asset('/contact') }}\">Nous Contacter</a></li>
                    {% if app.user %}
                    <li><a href=\"{{ asset('/avis') }}\">Déposer un avis</a></li>
                    {% endif %}
                  </ul>
                </li>
                <li><a href=\"{{ asset('/register') }}\">Votre compte</a>
                  <ul class=\"dropdown\">
                    {% if app.user %}
                    {% elseif not app.user %}
                    <li><a href=\"{{ asset('/login') }}\">Connexion</a></li>
                    <li><a href=\"{{ asset('/register') }}\">Inscription</a></li>
                    {% endif %}
                    {% if app.user %}
                      <li><a href=\"{{ asset('/panier') }}\">Votre panier</a></li>
                      <li><a href=\"{{ asset('/panier') }}\">Votre panier</a></li>
                      <li><a href=\"{{ asset('/compte') }}\">Mon compte</a></li>
                      <li><a href=\"{{ asset('/logout') }}\">Deconnexion</a></li>
                    {% elseif not app.user %}
                    {% endif %}
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
{% endblock %}
{% block body %}

{% endblock %}

{% block footer %}
  <footer class=\"footer\">
    <div class=\"container\">
      <div class=\"widget col-lg-3 col-md-3 col-sm-12\">
        <h4 class=\"title\">A propos de nous</h4>
        <p>Keman Racing ! est une entreprise fictive dans le but d'un projet noté. Rien de tout ça est vrai !</p>
      </div>
      <div class=\"widget col-lg-3 col-md-3 col-sm-12\">
        <h4 class=\"title\">Dernières nouveautés</h4>
        <ul class=\"recent_posts\">
          <li>
            <a href=\"{{ asset('/home1') }}\">
            <img src=\"{{ asset('assets/img/recent_post_01.png') }}\" alt=\"mobile\" /> Bientôt une nouvelle application mobile !</a>
            <a class=\"readmore\" href=\"{{ asset('/#') }}\">En savoir plus</a>
          </li>
          <li>
            <a href=\"{{ asset('/home1') }}#\">
            <img src=\"{{ asset('assets/img/recent_post_02.png') }}\" alt=\"Nouvelles\" /> Bientôt de nouveaux modèles !</a>
            <a class=\"readmore\" href=\"{{ asset('/nouvelles') }}\">En savoir plus</a>
          </li>
        </ul>
      </div>
      <div class=\"widget col-lg-3 col-md-3 col-sm-12\">
        <h4 class=\"title\"> Restons en contact</h4>
        <ul class=\"contact_details\">
          <li><i class=\"fa fa-envelope-o\"></i> info@kemanracing.com</li>
          <li><i class=\"fa fa-phone-square\"></i> 01 12 23 45 56</li>
          <li><i class=\"fa fa-home\"></i> Siège social, 2 rue Jean Macé, 78360 Montesson</li>
        </ul>
      </div>
      <div class=\"widget col-lg-3 col-md-3 col-sm-12\">
        <h4 class=\"title\">Nous trouver</h4>
<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d655.5617232372005!2d2.1535200292831567!3d48.91068819870577!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e663b1dbf34669%3A0x68a58f40089ea408!2s2+Rue+Jean+Mac%C3%A9%2C+78360+Montesson!5e0!3m2!1sfr!2sfr!4v1549376809503\" width=\"250\" height=\"200\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
      </div>
    </div>
    <div class=\"copyrights\">
      <div class=\"container\">
        <div class=\"col-lg-6 col-md-6 col-sm-12 columns footer-left\">
          <p>Copyright © 2018 - Tous droits réservés.</p>
          <div class=\"credits\">
            Crée pour un examen à l'<a href=\"https://www.ifocop.fr/\" target=\"blank\">IFOCOP</a>, PARIS.
          </div>
        </div>
        <div class=\"col-lg-6 col-md-6 col-sm-12 columns text-right\">
          <div class=\"footer-menu right\">
            <ul class=\"menu\">
              <li><a href=\"{{ asset('/') }}\">Acceuil</a></li>
              <li><a href=\"{{ asset('/qsn') }}\">A propos</a></li>
              <li><a href=\"{{ asset('/plan') }}\">Plan du site</a></li>
              <li><a href=\"{{ asset('/CGV') }}\">CGV</a></li>
              <li><a href=\"{{ asset('/contact') }}\">Contact</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
{% endblock %}

{% block javascripts %}
  <div class=\"dmtop\">Scroll to Top</div>
  <script src=\"{{ asset('/assets/lib/jquery/jquery.min.js') }}\"></script>
  <script src=\"{{ asset('/assets/lib/bootstrap/js/bootstrap.min.js') }}\"></script>
  <script src=\"{{ asset('/assets/lib/php-mail-form/validate.js') }}\"></script>
  <script src=\"{{ asset('/assets/lib/prettyphoto/js/prettyphoto.js') }}\"></script>
  <script src=\"{{ asset('/assets/lib/isotope/isotope.min.js') }}\"></script>
  <script src=\"{{ asset('/assets/lib/hover/hoverdir.js') }}\"></script>
  <script src=\"{{ asset('/assets/lib/hover/hoverex.min.js') }}\"></script>
  <script src=\"{{ asset('/assets/lib/unveil-effects/unveil-effects.js') }}\"></script>
  <script src=\"{{ asset('/assets/lib/owl-carousel/owl-carousel.js') }}\"></script>
  <script src=\"{{ asset('/assets/lib/jetmenu/jetmenu.js') }}\"></script>
  <script src=\"{{ asset('/assets/lib/animate-enhanced/animate-enhanced.min.js') }}\"></script>
  <script src=\"{{ asset('/assets/lib/jigowatt/jigowatt.js') }}\"></script>
  <script src=\"{{ asset('/assets/lib/easypiechart/easypiechart.min.js') }}\"></script>

  <script src=\"{{ asset('/assets/js/main.js') }}\"></script>
{% endblock %}
    </body>
</html>
", "::base.html.twig", "C:\\Monza\\app/Resources\\views/base.html.twig");
    }
}
