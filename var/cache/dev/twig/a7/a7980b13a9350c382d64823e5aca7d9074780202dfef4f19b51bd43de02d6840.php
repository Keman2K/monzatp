<?php

/* MonzaBundle:Default:index.html.twig */
class __TwigTemplate_f8c4725ad9c822ead8c34a61b82ce61306ba9a70637a5f713da0fc7bdad49da3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "MonzaBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MonzaBundle:Default:index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MonzaBundle:Default:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " Keman Racing";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "  <section id=\"intro\">
    <div class=\"container\">
      <div class=\"ror\">
        <div class=\"col-md-8 col-md-offset-2\">
          <h1>Bienvenue sur Keman Racing !</h1>
          <p>Afin de pouvoir réserver votre stage de pilotage, vous devez d'abord choisir un circuit puis la voiture ! Bonne route !</p>
        </div>
      </div>
    </div>
  </section>
  <section class=\"section1\">
    <div class=\"container\">
      <div class=\"col-lg-4 col-md-4 col-sm-4\">
        <div class=\"servicebox text-center\">
          <div class=\"service-icon\">
            <div class=\"dm-icon-effect-1\" data-effect=\"slide-left\">
              <a href=\"/circuits\" class=\"\"> <img src=\"https://img.icons8.com/ios/100/000000/new-zealand-south-island.png\"> </a>
            </div>
            <div class=\"servicetitle\">
              <h4>Les plus beaux circuits !</h4>
              <hr>
            </div>
            <p>Vous aurez la chance de rouler sur les plus beaux tracés ! Que ce soit Monza, Imola, Brands Hatch, Spa Francorchamps, Donington, Fiorano, Nordschleife, Paul Ricard, et bien d'autres !</p>
          </div>
        </div>
      </div>
      <div class=\"col-lg-4 col-md-4 col-sm-4\">
        <div class=\"servicebox text-center\">
          <div class=\"service-icon\">
            <div class=\"dm-icon-effect-1\" data-effect=\"slide-bottom\">
              <a href=\"/brands\" class=\"\"> <img src=\"https://img.icons8.com/color/100/000000/f1-race-car-side-view.png\"> </a>
            </div>
            <div class=\"servicetitle\">
              <h4>Les plus belles voitures !</h4>
              <hr>
            </div>
            <p>Prenez place au bord d'une monoplace Ferrari, d'une GT3 Lamborghini de rêve et des plus puissantes véhicules comme la Koenigsegg one:1 ! </p>
          </div>
        </div>
      </div>
      <div class=\"col-lg-4 col-md-4 col-sm-4\">
        <div class=\"servicebox text-center\">
          <div class=\"service-icon\">
            <div class=\"dm-icon-effect-1\" data-effect=\"slide-right\">
              <a href=\"/equipe\" class=\"\"> <img src=\"https://img.icons8.com/ios/100/000000/headset-filled.png\"> </a>
            </div>
            <div class=\"servicetitle\">
              <h4>Des tuteurs professionnels !</h4>
              <hr>
            </div>
            <p>Notre équipe composée de professionnels passionés seront là pour vous aider lors des briefings à mieux dompter ces machines impressionnantes ! Ils connaissent leurs tracés sur le bouts des doigts ! </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class=\"section5\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-12 columns\">
        <div class=\"widget\" data-effect=\"slide-left\">
          <img src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/casque.jpg"), "html", null, true);
        echo "\" alt=\"\">
        </div>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-12 columns\">
        <div class=\"widget clearfix\">
          <div class=\"services_lists\">
            <div class=\"services_lists_boxes clearfix\">
              <div class=\"col-lg-3 col-md-3 col-sm-12\">
                <div class=\"services_lists_boxes_icon\" data-effect=\"slide-bottom\">
                  <a href=\"/login\" class=\"\"> <i class=\"active dm-icon-medium fa fa-key fa-2x\"></i> </a>
                </div>
              </div>
              <div class=\"col-lg-9 col-md-9 col-sm-9\">
                <div class=\"servicetitle\">
                  <h4>Créer votre compte</h4>
                  <hr>
                </div>
                <p>Commencez par vous inscrire, remplissez le formulaire, validez le lien que vous recevrez par mail </p>
              </div>
            </div>
            <div class=\"services_lists_boxes clearfix\">
              <div class=\"col-lg-3 col-md-3 col-sm-12\">
                <div class=\"services_lists_boxes_icon\" data-effect=\"slide-bottom\">
                  <a href=\"/brands\" class=\"\"> <i class=\"active dm-icon-medium fa fa-car fa-2x\"></i> </a>
                </div>
              </div>
              <div class=\"col-lg-9 col-md-9 col-sm-9\">
                <div class=\"servicetitle\">
                  <h4>Choisissez votre tracé puis votre véhicule</h4>
                  <hr>
                </div>
                <p>Vous trouverez votre choix parmi notre large gamme de circuits a travers l'Europe (principalement Italie, Angleterre, France et Allemagne). Nous vous proposons également une grande gamme de marques (Ferrari, Mercedes-Benz, Porsche, Lamborghini, Pagani, etc...)</p>
              </div>
            </div>
            <div class=\"services_lists_boxes clearfix\">
              <div class=\"col-lg-3 col-md-3 col-sm-12\">
                <div class=\"services_lists_boxes_icon_none\" data-effect=\"slide-bottom\">
                  <a href=\"/login\" class=\"\"> <i class=\"active dm-icon-medium fa fa-dollar fa-2x\"></i> </a>
                </div>
              </div>
              <div class=\"col-lg-9 col-md-9 col-sm-9\">
                <div class=\"servicetitle\">
                  <h4>Confirmation de la date et de la réservation</h4>
                  <hr>
                </div>
                <p>Une fois la date choisie et la réservation confirmée, vous n'aurez plus qu'à vous rendre sur la piste !</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class=\"section4 text-center\">
    <div class=\"general-title\">
      <h3>Nos plus beaux modèles !</h3>
      <hr>
    </div>
    <div class=\"portfolio-wrapper\">
      <div id=\"owl-demo\" class=\"owl-carousel\">
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Mercedes-Benz/CLK-GTR/6.jpg"), "html", null, true);
        echo "\">
            <img class=\"lazyOwl\" src=\"";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Mercedes-Benz/CLK-GTR/6.jpg"), "html", null, true);
        echo "\" data-src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Mercedes-Benz/CLK-GTR/6.jpg"), "html", null, true);
        echo "\" alt=\"CLK-GTR\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Bmw/M320 Group 5/5.jpg"), "html", null, true);
        echo "\">
            <img class=\"lazyOwl\" src=\"";
        // line 132
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Bmw/M320 Group 5/5.jpg"), "html", null, true);
        echo "\" data-src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Bmw/M320 Group 5/5.jpg"), "html", null, true);
        echo "\" alt=\"M320 Group 5\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"";
        // line 136
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Ferrari/LaFerrari/1.jpg"), "html", null, true);
        echo "\">
            <img class=\"lazyOwl\" src=";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("\"assets/img/voitures/Ferrari/LaFerrari/1.jpg"), "html", null, true);
        echo "\" data-src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Ferrari/LaFerrari/1.jpg"), "html", null, true);
        echo "\" alt=\"LaFerrari\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Koenigsegg/AgeraRS/6.jpg"), "html", null, true);
        echo "\">
            <img class=\"lazyOwl\" src=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Koenigsegg/AgeraRS/6.jpg"), "html", null, true);
        echo "\" data-src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Koenigsegg/AgeraRS/6.jpg"), "html", null, true);
        echo "\" alt=\"AgeraRS\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"";
        // line 146
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Mazda/787B/5.jpg"), "html", null, true);
        echo "\">
            <img class=\"lazyOwl\" src=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Mazda/787B/5.jpg"), "html", null, true);
        echo "\" data-src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Mazda/787B/5.jpg"), "html", null, true);
        echo "\" alt=\"787B\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Audi/R18 e-tron quattro/4.jpg"), "html", null, true);
        echo "\">
            <img class=\"lazyOwl\" src=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Audi/R18 e-tron quattro/4.jpg"), "html", null, true);
        echo "\" data-src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Audi/R18 e-tron quattro/4.jpg"), "html", null, true);
        echo "\" alt=\"R18 e-tron quattro\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Lamborghini/Veneno/9.jpg"), "html", null, true);
        echo "\">
            <img class=\"lazyOwl\" src=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Lamborghini/Veneno/9.jpg"), "html", null, true);
        echo "\" data-src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Lamborghini/Veneno/9.jpg"), "html", null, true);
        echo "\" alt=\"Veneno\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"";
        // line 161
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Ferrari/SF15-T/1.jpg"), "html", null, true);
        echo "\">
            <img class=\"lazyOwl\" src=\"";
        // line 162
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Ferrari/SF15-T/1.jpg"), "html", null, true);
        echo "\" data-src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Ferrari/SF15-T/1.jpg"), "html", null, true);
        echo "\" alt=\"SF15-T\">
          </a>
        </div>
        <div class=\"item\">
          <a href=\"";
        // line 166
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/AlfaRomeo/155 V6 TI DTM/2.jpg"), "html", null, true);
        echo "\">
            <img class=\"lazyOwl\" src=\"";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/AlfaRomeo/155 V6 TI DTM/2.jpg"), "html", null, true);
        echo "\" data-src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/AlfaRomeo/155 V6 TI DTM/2.jpg"), "html", null, true);
        echo "\" alt=\"Alfa Roméo\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"";
        // line 171
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Porsche/919 Hybrid Evo/2.jpg"), "html", null, true);
        echo "\">
            <img class=\"lazyOwl\" src=\"";
        // line 172
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Porsche/919 Hybrid Evo/2.jpg"), "html", null, true);
        echo "\" data-src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Porsche/919 Hybrid Evo/2.jpg"), "html", null, true);
        echo "\" alt=\"919 Hybrid Evo\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Toyota/GT-One Road Car/4.jpg"), "html", null, true);
        echo "\">
            <img class=\"lazyOwl\" src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Toyota/GT-One Road Car/4.jpg"), "html", null, true);
        echo "\" data-src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Toyota/GT-One Road Car/4.jpg"), "html", null, true);
        echo "\" alt=\"GT-One Road Car\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"";
        // line 181
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Volkswagen/I.D. R Pikes Peak/8.jpg"), "html", null, true);
        echo "\">
            <img class=\"lazyOwl\" src=\"";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Volkswagen/I.D. R Pikes Peak/8.jpg"), "html", null, true);
        echo "\" data-src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/voitures/Volkswagen/I.D. R Pikes Peak/8.jpg"), "html", null, true);
        echo "\" alt=\"I.D. R Pikes Peak\">
          </a>
        </div>
      </div>
    </div>
  </section>
  <section class=\"section2\">
    <div class=\"container\">
      <div class=\"message text-center\">
        <h2 class=\"big-title\">RÉSERVEZ <span> VOTRE </span> VOITURE </h2>
        <p class=\"small-title\">Et le circuit !</p>
        <a class=\"button large\" href=\"/circuits\">Voir nos circuits</a>
      </div>
    </div>
  </section>
  
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "MonzaBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  341 => 182,  337 => 181,  328 => 177,  324 => 176,  315 => 172,  311 => 171,  302 => 167,  298 => 166,  289 => 162,  285 => 161,  276 => 157,  272 => 156,  263 => 152,  259 => 151,  250 => 147,  246 => 146,  237 => 142,  233 => 141,  224 => 137,  220 => 136,  211 => 132,  207 => 131,  198 => 127,  194 => 126,  130 => 65,  68 => 5,  59 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block title %} Keman Racing{% endblock %}
{% block body %}
  <section id=\"intro\">
    <div class=\"container\">
      <div class=\"ror\">
        <div class=\"col-md-8 col-md-offset-2\">
          <h1>Bienvenue sur Keman Racing !</h1>
          <p>Afin de pouvoir réserver votre stage de pilotage, vous devez d'abord choisir un circuit puis la voiture ! Bonne route !</p>
        </div>
      </div>
    </div>
  </section>
  <section class=\"section1\">
    <div class=\"container\">
      <div class=\"col-lg-4 col-md-4 col-sm-4\">
        <div class=\"servicebox text-center\">
          <div class=\"service-icon\">
            <div class=\"dm-icon-effect-1\" data-effect=\"slide-left\">
              <a href=\"/circuits\" class=\"\"> <img src=\"https://img.icons8.com/ios/100/000000/new-zealand-south-island.png\"> </a>
            </div>
            <div class=\"servicetitle\">
              <h4>Les plus beaux circuits !</h4>
              <hr>
            </div>
            <p>Vous aurez la chance de rouler sur les plus beaux tracés ! Que ce soit Monza, Imola, Brands Hatch, Spa Francorchamps, Donington, Fiorano, Nordschleife, Paul Ricard, et bien d'autres !</p>
          </div>
        </div>
      </div>
      <div class=\"col-lg-4 col-md-4 col-sm-4\">
        <div class=\"servicebox text-center\">
          <div class=\"service-icon\">
            <div class=\"dm-icon-effect-1\" data-effect=\"slide-bottom\">
              <a href=\"/brands\" class=\"\"> <img src=\"https://img.icons8.com/color/100/000000/f1-race-car-side-view.png\"> </a>
            </div>
            <div class=\"servicetitle\">
              <h4>Les plus belles voitures !</h4>
              <hr>
            </div>
            <p>Prenez place au bord d'une monoplace Ferrari, d'une GT3 Lamborghini de rêve et des plus puissantes véhicules comme la Koenigsegg one:1 ! </p>
          </div>
        </div>
      </div>
      <div class=\"col-lg-4 col-md-4 col-sm-4\">
        <div class=\"servicebox text-center\">
          <div class=\"service-icon\">
            <div class=\"dm-icon-effect-1\" data-effect=\"slide-right\">
              <a href=\"/equipe\" class=\"\"> <img src=\"https://img.icons8.com/ios/100/000000/headset-filled.png\"> </a>
            </div>
            <div class=\"servicetitle\">
              <h4>Des tuteurs professionnels !</h4>
              <hr>
            </div>
            <p>Notre équipe composée de professionnels passionés seront là pour vous aider lors des briefings à mieux dompter ces machines impressionnantes ! Ils connaissent leurs tracés sur le bouts des doigts ! </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class=\"section5\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-12 columns\">
        <div class=\"widget\" data-effect=\"slide-left\">
          <img src=\"{{ asset('assets/img/casque.jpg') }}\" alt=\"\">
        </div>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-12 columns\">
        <div class=\"widget clearfix\">
          <div class=\"services_lists\">
            <div class=\"services_lists_boxes clearfix\">
              <div class=\"col-lg-3 col-md-3 col-sm-12\">
                <div class=\"services_lists_boxes_icon\" data-effect=\"slide-bottom\">
                  <a href=\"/login\" class=\"\"> <i class=\"active dm-icon-medium fa fa-key fa-2x\"></i> </a>
                </div>
              </div>
              <div class=\"col-lg-9 col-md-9 col-sm-9\">
                <div class=\"servicetitle\">
                  <h4>Créer votre compte</h4>
                  <hr>
                </div>
                <p>Commencez par vous inscrire, remplissez le formulaire, validez le lien que vous recevrez par mail </p>
              </div>
            </div>
            <div class=\"services_lists_boxes clearfix\">
              <div class=\"col-lg-3 col-md-3 col-sm-12\">
                <div class=\"services_lists_boxes_icon\" data-effect=\"slide-bottom\">
                  <a href=\"/brands\" class=\"\"> <i class=\"active dm-icon-medium fa fa-car fa-2x\"></i> </a>
                </div>
              </div>
              <div class=\"col-lg-9 col-md-9 col-sm-9\">
                <div class=\"servicetitle\">
                  <h4>Choisissez votre tracé puis votre véhicule</h4>
                  <hr>
                </div>
                <p>Vous trouverez votre choix parmi notre large gamme de circuits a travers l'Europe (principalement Italie, Angleterre, France et Allemagne). Nous vous proposons également une grande gamme de marques (Ferrari, Mercedes-Benz, Porsche, Lamborghini, Pagani, etc...)</p>
              </div>
            </div>
            <div class=\"services_lists_boxes clearfix\">
              <div class=\"col-lg-3 col-md-3 col-sm-12\">
                <div class=\"services_lists_boxes_icon_none\" data-effect=\"slide-bottom\">
                  <a href=\"/login\" class=\"\"> <i class=\"active dm-icon-medium fa fa-dollar fa-2x\"></i> </a>
                </div>
              </div>
              <div class=\"col-lg-9 col-md-9 col-sm-9\">
                <div class=\"servicetitle\">
                  <h4>Confirmation de la date et de la réservation</h4>
                  <hr>
                </div>
                <p>Une fois la date choisie et la réservation confirmée, vous n'aurez plus qu'à vous rendre sur la piste !</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class=\"section4 text-center\">
    <div class=\"general-title\">
      <h3>Nos plus beaux modèles !</h3>
      <hr>
    </div>
    <div class=\"portfolio-wrapper\">
      <div id=\"owl-demo\" class=\"owl-carousel\">
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"{{ asset('assets/img/voitures/Mercedes-Benz/CLK-GTR/6.jpg') }}\">
            <img class=\"lazyOwl\" src=\"{{ asset('assets/img/voitures/Mercedes-Benz/CLK-GTR/6.jpg') }}\" data-src=\"{{ asset('assets/img/voitures/Mercedes-Benz/CLK-GTR/6.jpg') }}\" alt=\"CLK-GTR\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"{{ asset('assets/img/voitures/Bmw/M320 Group 5/5.jpg') }}\">
            <img class=\"lazyOwl\" src=\"{{ asset('assets/img/voitures/Bmw/M320 Group 5/5.jpg') }}\" data-src=\"{{ asset('assets/img/voitures/Bmw/M320 Group 5/5.jpg') }}\" alt=\"M320 Group 5\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"{{ asset('assets/img/voitures/Ferrari/LaFerrari/1.jpg') }}\">
            <img class=\"lazyOwl\" src={{ asset('\"assets/img/voitures/Ferrari/LaFerrari/1.jpg') }}\" data-src=\"{{ asset('assets/img/voitures/Ferrari/LaFerrari/1.jpg') }}\" alt=\"LaFerrari\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"{{ asset('assets/img/voitures/Koenigsegg/AgeraRS/6.jpg') }}\">
            <img class=\"lazyOwl\" src=\"{{ asset('assets/img/voitures/Koenigsegg/AgeraRS/6.jpg') }}\" data-src=\"{{ asset('assets/img/voitures/Koenigsegg/AgeraRS/6.jpg') }}\" alt=\"AgeraRS\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"{{ asset('assets/img/voitures/Mazda/787B/5.jpg') }}\">
            <img class=\"lazyOwl\" src=\"{{ asset('assets/img/voitures/Mazda/787B/5.jpg') }}\" data-src=\"{{ asset('assets/img/voitures/Mazda/787B/5.jpg') }}\" alt=\"787B\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"{{ asset('assets/img/voitures/Audi/R18 e-tron quattro/4.jpg') }}\">
            <img class=\"lazyOwl\" src=\"{{ asset('assets/img/voitures/Audi/R18 e-tron quattro/4.jpg') }}\" data-src=\"{{ asset('assets/img/voitures/Audi/R18 e-tron quattro/4.jpg') }}\" alt=\"R18 e-tron quattro\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"{{ asset('assets/img/voitures/Lamborghini/Veneno/9.jpg') }}\">
            <img class=\"lazyOwl\" src=\"{{ asset('assets/img/voitures/Lamborghini/Veneno/9.jpg') }}\" data-src=\"{{ asset('assets/img/voitures/Lamborghini/Veneno/9.jpg') }}\" alt=\"Veneno\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"{{ asset('assets/img/voitures/Ferrari/SF15-T/1.jpg') }}\">
            <img class=\"lazyOwl\" src=\"{{ asset('assets/img/voitures/Ferrari/SF15-T/1.jpg') }}\" data-src=\"{{ asset('assets/img/voitures/Ferrari/SF15-T/1.jpg') }}\" alt=\"SF15-T\">
          </a>
        </div>
        <div class=\"item\">
          <a href=\"{{ asset('assets/img/voitures/AlfaRomeo/155 V6 TI DTM/2.jpg') }}\">
            <img class=\"lazyOwl\" src=\"{{ asset('assets/img/voitures/AlfaRomeo/155 V6 TI DTM/2.jpg') }}\" data-src=\"{{ asset('assets/img/voitures/AlfaRomeo/155 V6 TI DTM/2.jpg') }}\" alt=\"Alfa Roméo\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"{{ asset('assets/img/voitures/Porsche/919 Hybrid Evo/2.jpg') }}\">
            <img class=\"lazyOwl\" src=\"{{ asset('assets/img/voitures/Porsche/919 Hybrid Evo/2.jpg') }}\" data-src=\"{{ asset('assets/img/voitures/Porsche/919 Hybrid Evo/2.jpg') }}\" alt=\"919 Hybrid Evo\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"{{ asset('assets/img/voitures/Toyota/GT-One Road Car/4.jpg') }}\">
            <img class=\"lazyOwl\" src=\"{{ asset('assets/img/voitures/Toyota/GT-One Road Car/4.jpg') }}\" data-src=\"{{ asset('assets/img/voitures/Toyota/GT-One Road Car/4.jpg') }}\" alt=\"GT-One Road Car\">
          </a>
        </div>
        <div class=\"item\">
          <a data-rel=\"prettyPhoto\" href=\"{{ asset('assets/img/voitures/Volkswagen/I.D. R Pikes Peak/8.jpg') }}\">
            <img class=\"lazyOwl\" src=\"{{ asset('assets/img/voitures/Volkswagen/I.D. R Pikes Peak/8.jpg') }}\" data-src=\"{{ asset('assets/img/voitures/Volkswagen/I.D. R Pikes Peak/8.jpg') }}\" alt=\"I.D. R Pikes Peak\">
          </a>
        </div>
      </div>
    </div>
  </section>
  <section class=\"section2\">
    <div class=\"container\">
      <div class=\"message text-center\">
        <h2 class=\"big-title\">RÉSERVEZ <span> VOTRE </span> VOITURE </h2>
        <p class=\"small-title\">Et le circuit !</p>
        <a class=\"button large\" href=\"/circuits\">Voir nos circuits</a>
      </div>
    </div>
  </section>
  
{% endblock %}", "MonzaBundle:Default:index.html.twig", "C:\\Monza\\src\\MonzaBundle/Resources/views/Default/index.html.twig");
    }
}
