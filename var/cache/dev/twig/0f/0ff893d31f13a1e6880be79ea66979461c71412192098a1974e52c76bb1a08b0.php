<?php

/* @Monza/Default/panier.html.twig */
class __TwigTemplate_6be79b835545608f8a52d262cbde2136670a61ad15aadc234e12d7c5930a2214 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@Monza/Default/panier.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Monza/Default/panier.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Monza/Default/panier.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " Keman Racing";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Panier</li>
        </ul>
        <h2>PANIER</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>

  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-8 col-md-8 col-sm-8 col-xs-12 clearfix\">

        <table class=\"table table-striped checkout\" data-effect=\"fade\">
          <thead>
            <tr>
              <th>Nom du produit</th>
              <th>Date et Heure</th>
              <th>Prix</th>
              <th>Supprimer</th>
            </tr>
          </thead>
          <tbody>
            ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["tableauLignePanier"] ?? $this->getContext($context, "tableauLignePanier")));
        foreach ($context['_seq'] as $context["_key"] => $context["lignePanier"]) {
            // line 43
            echo "
              <tr>
                <td><a data-rel=\"prettyPhoto\" href=\"";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["lignePanier"], "getCircuit", array(), "method"), "getCircuitImage", array(), "method"), "html", null, true);
            echo "\"><img width=\"50\" src=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["lignePanier"], "getCircuit", array(), "method"), "getCircuitImage", array(), "method"), "html", null, true);
            echo "\" alt=\"\"></a> ";
            echo $this->getAttribute($this->getAttribute($context["lignePanier"], "getCircuit", array(), "method"), "getCircuitName", array(), "method");
            echo " <br /><br />

                <a data-rel=\"prettyPhoto\" href=\"";
            // line 47
            echo $this->getAttribute($this->getAttribute($context["lignePanier"], "getBrand", array(), "method"), "getBrandImage", array(), "method");
            echo "\"><img width=\"61\" src=\"";
            echo $this->getAttribute($this->getAttribute($context["lignePanier"], "getBrand", array(), "method"), "getbrandImage", array(), "method");
            echo "\" alt=\"\"></a> ";
            echo $this->getAttribute($this->getAttribute($context["lignePanier"], "getBrand", array(), "method"), "getBrandName", array(), "method");
            echo " ";
            echo $this->getAttribute($this->getAttribute($context["lignePanier"], "getBrand", array(), "method"), "getbrandModel", array(), "method");
            echo " </td>
                <td>
                    <input type=\"date\" required >
                    <input type=\"time\" required >
                </td>
                <td>";
            // line 52
            echo $this->getAttribute($this->getAttribute($context["lignePanier"], "getBrand", array(), "method"), "getBrandPrix", array(), "method");
            echo " €</td>
                <td><a class=\"remove\" href=\"/removePanier/";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["lignePanier"], "getId", array(), "method"), "html", null, true);
            echo "\" title=\"Supprimer\"></a></td>
              </tr>

            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lignePanier'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "          </tbody>
        </table>
        <div class=\"clearfix\"></div>

        <div class=\"clearfix\"></div>

        <div class=\"well text-right\">
          <strong>
            <h4>Prix Total : ";
        // line 65
        echo twig_escape_filter($this->env, ($context["prixTotal"] ?? $this->getContext($context, "prixTotal")), "html", null, true);
        echo " </h4>
            <h4>Prix total avec 20 % de réduction : ";
        // line 66
        $context["prixReduc"] = (($context["prixTotal"] ?? $this->getContext($context, "prixTotal")) * 0.8);
        echo " ";
        echo twig_escape_filter($this->env, ($context["prixReduc"] ?? $this->getContext($context, "prixReduc")), "html", null, true);
        echo " €</h4>
          </strong>
        </div>

        <div class=\"clearfix\"></div>

        <div class=\"clearfix\"></div>
        <div class=\"divider\"></div>

        <h5 class=\"title\">INFORMATIONS PERSONNELLES</h5>

        <form id=\"personalinfo\" action=\"/confirmation\" name=\"personalinfo\" method=\"post\">
          <label for=\"email\">Adresse mail<span class=\"required\">*</span></label>
          <input type=\"email\" name=\"name\" id=\"email\" class=\"form-control\" placeholder=\"Adresse mail\" required >
          <label for=\"fname\">Prénom<span class=\"required\">*</span></label>
          <input type=\"text\" name=\"fname\" id=\"fname\" class=\"form-control\" placeholder=\"Prénom\" required >
          <label for=\"lname\">Nom de famille</label>
          <input type=\"text\" name=\"lname\" id=\"lname\" class=\"form-control\" placeholder=\"Nom\" required >
        
        <div class=\"clearfix\"></div>
        <div class=\"divider\"></div>

        <h5 class=\"title\">DÉTAIL DU PANIER</h5>

          <label for=\"cardnumber\">Numéro de Carte <span class=\"required\">*</span></label>
          <input type=\"text\" name=\"cardnumber\" id=\"cardnumber\" class=\"form-control\" placeholder=\"Votre numéro de carte\" required >
          <label for=\"cvc\">CVC <span class=\"required\">*</span></label>
          <input type=\"text\" name=\"cvc\" id=\"cvc\" class=\"form-control\" placeholder=\"Code de sécurité\" required >
          <label for=\"ncard\">Nom du propriétaire de la carte <span class=\"required\">*</span></label>
          <input type=\"text\" name=\"ncard\" id=\"ncard\" class=\"form-control\" placeholder=\"Nom du propriétaire de la carte\" required >
          <label for=\"ncard\">Expiration (Mois/Année) <span class=\"required\">*</span></label>

          <div class=\"clearfix\"></div>

          <select class=\"form-control\" style=\"width:75px; float:left; margin-right:10px;\">
\t\t\t\t\t<option>01</option>
\t\t\t\t\t<option>02</option>
\t\t\t\t\t<option>03</option>
\t\t\t\t\t<option>04</option>
\t\t\t\t\t<option>05</option>
\t\t\t\t\t<option>06</option>
\t\t\t\t\t<option>07</option>
\t\t\t\t\t<option>08</option>
\t\t\t\t\t<option>09</option>
\t\t\t\t\t<option>10</option>
\t\t\t\t\t<option>11</option>
\t\t\t\t\t<option>12</option>
\t\t\t\t</select>
          <select class=\"form-control\" style=\"width:75px;float:left\">
\t\t\t\t\t<option>18</option>
\t\t\t\t\t<option>17</option>
\t\t\t\t\t<option>19</option>
\t\t\t\t\t<option>20</option>
\t\t\t\t\t<option>21</option>
          <option>23</option>
          <option>24</option>
\t\t\t\t\t<option>25</option>
\t\t\t\t</select>
        
        <div class=\"clearfix\"></div>
        <div class=\"divider\"></div>

        <h5 class=\"title\">ADRESSE DE FACTURATION</h5>

          <label for=\"baddress\">Adresse principale</label>
          <input type=\"text\" name=\"baddress\" id=\"baddress\" class=\"form-control\" placeholder=\"Adresse principale\" required >
          <label for=\"baddress1\">Adresse secondaire (Optionel)</label>
          <input type=\"text\" name=\"baddress1\" id=\"baddress1\" class=\"form-control\" placeholder=\"Adresse secondaire \" required >
          <label for=\"bcity\">Ville</label>
          <input type=\"text\" name=\"bcity\" id=\"bcity\" class=\"form-control\" placeholder=\"Votre ville\" required >
          <label for=\"bzip\">Code postal</label>
          <input type=\"text\" name=\"bzip\" id=\"bzip\" class=\"form-control\" placeholder=\"Votre code postal\" required >

          <div class=\"clearfix\"></div>

          <select class=\"form-control\">
\t\t\t\t\t<option>France</option>
  \t\t\t\t</select>
            <select class=\"form-control\">
  \t\t\t\t\t<option>Paris</option>
  \t\t\t\t\t<option>Île de France</option>
  \t\t\t\t\t<option>Savoie</option>
  \t\t\t\t\t<option>Haute-Savoie</option>
  \t\t\t\t\t<option>Montpellier</option>
  \t\t\t\t\t<option>Toulouse</option>
  \t\t\t\t\t<option>Amiens</option>
  \t\t\t\t\t<option>Tours</option>
  \t\t\t\t</select>
          <br>
          <br>
          <br>

          <label class=\"checkbox-inline\">
            <input id=\"inlineCheckbox3\" type=\"checkbox\" value=\"option1\" class=\"required\" required >
            <strong>J'ACCEPTE LES TERMES ET LES CONDITIONS</strong>
          </label>

          <br/ ><br/ >
          <div class=\"clearfix\"></div>
          <a href=\"/CGV\"><button class=\"button\">VOIR LES TERMES</button></a>
          <div class=\"clearfix\"></div>
            <button class=\"button large btn-block\">JE CONFIRME MA COMMANDE</button>
            </form>

          
         

        
        </div>
      </div>
    </div>
  </section>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@Monza/Default/panier.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 66,  162 => 65,  152 => 57,  142 => 53,  138 => 52,  124 => 47,  115 => 45,  111 => 43,  107 => 42,  68 => 5,  59 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block title %} Keman Racing{% endblock %}
{% block body %}

  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Panier</li>
        </ul>
        <h2>PANIER</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>

  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-8 col-md-8 col-sm-8 col-xs-12 clearfix\">

        <table class=\"table table-striped checkout\" data-effect=\"fade\">
          <thead>
            <tr>
              <th>Nom du produit</th>
              <th>Date et Heure</th>
              <th>Prix</th>
              <th>Supprimer</th>
            </tr>
          </thead>
          <tbody>
            {% for lignePanier in tableauLignePanier %}

              <tr>
                <td><a data-rel=\"prettyPhoto\" href=\"{{ lignePanier.getCircuit().getCircuitImage() }}\"><img width=\"50\" src=\"{{ lignePanier.getCircuit().getCircuitImage() }}\" alt=\"\"></a> {{ lignePanier.getCircuit().getCircuitName() | raw }} <br /><br />

                <a data-rel=\"prettyPhoto\" href=\"{{lignePanier.getBrand().getBrandImage() | raw }}\"><img width=\"61\" src=\"{{ lignePanier.getBrand().getbrandImage() | raw }}\" alt=\"\"></a> {{ lignePanier.getBrand().getBrandName() | raw }} {{ lignePanier.getBrand().getbrandModel() | raw }} </td>
                <td>
                    <input type=\"date\" required >
                    <input type=\"time\" required >
                </td>
                <td>{{ lignePanier.getBrand().getBrandPrix() | raw }} €</td>
                <td><a class=\"remove\" href=\"/removePanier/{{ lignePanier.getId() }}\" title=\"Supprimer\"></a></td>
              </tr>

            {% endfor %}
          </tbody>
        </table>
        <div class=\"clearfix\"></div>

        <div class=\"clearfix\"></div>

        <div class=\"well text-right\">
          <strong>
            <h4>Prix Total : {{ prixTotal }} </h4>
            <h4>Prix total avec 20 % de réduction : {% set prixReduc = prixTotal * 0.8 %} {{ prixReduc }} €</h4>
          </strong>
        </div>

        <div class=\"clearfix\"></div>

        <div class=\"clearfix\"></div>
        <div class=\"divider\"></div>

        <h5 class=\"title\">INFORMATIONS PERSONNELLES</h5>

        <form id=\"personalinfo\" action=\"/confirmation\" name=\"personalinfo\" method=\"post\">
          <label for=\"email\">Adresse mail<span class=\"required\">*</span></label>
          <input type=\"email\" name=\"name\" id=\"email\" class=\"form-control\" placeholder=\"Adresse mail\" required >
          <label for=\"fname\">Prénom<span class=\"required\">*</span></label>
          <input type=\"text\" name=\"fname\" id=\"fname\" class=\"form-control\" placeholder=\"Prénom\" required >
          <label for=\"lname\">Nom de famille</label>
          <input type=\"text\" name=\"lname\" id=\"lname\" class=\"form-control\" placeholder=\"Nom\" required >
        
        <div class=\"clearfix\"></div>
        <div class=\"divider\"></div>

        <h5 class=\"title\">DÉTAIL DU PANIER</h5>

          <label for=\"cardnumber\">Numéro de Carte <span class=\"required\">*</span></label>
          <input type=\"text\" name=\"cardnumber\" id=\"cardnumber\" class=\"form-control\" placeholder=\"Votre numéro de carte\" required >
          <label for=\"cvc\">CVC <span class=\"required\">*</span></label>
          <input type=\"text\" name=\"cvc\" id=\"cvc\" class=\"form-control\" placeholder=\"Code de sécurité\" required >
          <label for=\"ncard\">Nom du propriétaire de la carte <span class=\"required\">*</span></label>
          <input type=\"text\" name=\"ncard\" id=\"ncard\" class=\"form-control\" placeholder=\"Nom du propriétaire de la carte\" required >
          <label for=\"ncard\">Expiration (Mois/Année) <span class=\"required\">*</span></label>

          <div class=\"clearfix\"></div>

          <select class=\"form-control\" style=\"width:75px; float:left; margin-right:10px;\">
\t\t\t\t\t<option>01</option>
\t\t\t\t\t<option>02</option>
\t\t\t\t\t<option>03</option>
\t\t\t\t\t<option>04</option>
\t\t\t\t\t<option>05</option>
\t\t\t\t\t<option>06</option>
\t\t\t\t\t<option>07</option>
\t\t\t\t\t<option>08</option>
\t\t\t\t\t<option>09</option>
\t\t\t\t\t<option>10</option>
\t\t\t\t\t<option>11</option>
\t\t\t\t\t<option>12</option>
\t\t\t\t</select>
          <select class=\"form-control\" style=\"width:75px;float:left\">
\t\t\t\t\t<option>18</option>
\t\t\t\t\t<option>17</option>
\t\t\t\t\t<option>19</option>
\t\t\t\t\t<option>20</option>
\t\t\t\t\t<option>21</option>
          <option>23</option>
          <option>24</option>
\t\t\t\t\t<option>25</option>
\t\t\t\t</select>
        
        <div class=\"clearfix\"></div>
        <div class=\"divider\"></div>

        <h5 class=\"title\">ADRESSE DE FACTURATION</h5>

          <label for=\"baddress\">Adresse principale</label>
          <input type=\"text\" name=\"baddress\" id=\"baddress\" class=\"form-control\" placeholder=\"Adresse principale\" required >
          <label for=\"baddress1\">Adresse secondaire (Optionel)</label>
          <input type=\"text\" name=\"baddress1\" id=\"baddress1\" class=\"form-control\" placeholder=\"Adresse secondaire \" required >
          <label for=\"bcity\">Ville</label>
          <input type=\"text\" name=\"bcity\" id=\"bcity\" class=\"form-control\" placeholder=\"Votre ville\" required >
          <label for=\"bzip\">Code postal</label>
          <input type=\"text\" name=\"bzip\" id=\"bzip\" class=\"form-control\" placeholder=\"Votre code postal\" required >

          <div class=\"clearfix\"></div>

          <select class=\"form-control\">
\t\t\t\t\t<option>France</option>
  \t\t\t\t</select>
            <select class=\"form-control\">
  \t\t\t\t\t<option>Paris</option>
  \t\t\t\t\t<option>Île de France</option>
  \t\t\t\t\t<option>Savoie</option>
  \t\t\t\t\t<option>Haute-Savoie</option>
  \t\t\t\t\t<option>Montpellier</option>
  \t\t\t\t\t<option>Toulouse</option>
  \t\t\t\t\t<option>Amiens</option>
  \t\t\t\t\t<option>Tours</option>
  \t\t\t\t</select>
          <br>
          <br>
          <br>

          <label class=\"checkbox-inline\">
            <input id=\"inlineCheckbox3\" type=\"checkbox\" value=\"option1\" class=\"required\" required >
            <strong>J'ACCEPTE LES TERMES ET LES CONDITIONS</strong>
          </label>

          <br/ ><br/ >
          <div class=\"clearfix\"></div>
          <a href=\"/CGV\"><button class=\"button\">VOIR LES TERMES</button></a>
          <div class=\"clearfix\"></div>
            <button class=\"button large btn-block\">JE CONFIRME MA COMMANDE</button>
            </form>

          
         

        
        </div>
      </div>
    </div>
  </section>

{% endblock %}", "@Monza/Default/panier.html.twig", "C:\\Monza\\src\\MonzaBundle\\Resources\\views\\Default\\panier.html.twig");
    }
}
