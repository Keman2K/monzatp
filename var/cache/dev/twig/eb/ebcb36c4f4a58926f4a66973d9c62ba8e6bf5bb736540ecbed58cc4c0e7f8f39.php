<?php

/* MonzaBundle:Default:avis.html.twig */
class __TwigTemplate_181349ae14bd0c509a166b35b701223cbff6a5db2e8a6dbe5e6762cc59cebbc7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "MonzaBundle:Default:avis.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MonzaBundle:Default:avis.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MonzaBundle:Default:avis.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Vos avis</li>
        </ul>
        <h2>VOS AVIS</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>

  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">

        <div class=\"general-title text-center\">
          <h3>Lire les avis</h3>
          <hr>
        </div>

        <div class=\"col-lg-6 col-md-6 col-sm-12 first\">
          <div class=\"testimonial\">
            <img data-effect=\"slide-bottom\" class=\"alignleft img-circle\" src=\"img/team_01.png\" alt=\"\">
            <p>Je m'appelle Michel.</p>
            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            <div class=\"testimonial-meta\">
              <h4>Mark Soto <small><a href=\"#\">BlackTie.co</a></small></h4>
            </div>
          </div>
          <!-- end dmbox -->
        </div>

        <div class=\"col-lg-6 col-md-6 col-sm-12 last\">
          <div class=\"testimonial\">
            <img data-effect=\"slide-bottom\" class=\"alignleft img-circle\" src=\"img/team_03.png\" alt=\"\">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen
              book.</p>
            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            <div class=\"testimonial-meta\">
              <h4>Lisa Meyer <small><a href=\"#\">BlackTie.co</a></small></h4>
            </div>
          </div>
          <!-- end dmbox -->
        </div>
        <!-- end col-lg-6 -->

        <div class=\"col-lg-4 col-md-4 col-sm-12 first\">
          <div class=\"testimonial\">
            <img data-effect=\"slide-bottom\" class=\"alignleft img-circle\" src=\"img/team_02.png\" alt=\"\">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            <div class=\"testimonial-meta\">
              <h4>Jenny DOE <small><a href=\"#\">google.com</a></small></h4>
            </div>
          </div>
          <!-- end dmbox -->
        </div>

        <div class=\"col-lg-4 col-md-4 col-sm-12\">
          <div class=\"testimonial\">
            <img data-effect=\"slide-bottom\" class=\"alignleft img-circle\" src=\"img/team_05.png\" alt=\"\">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            <div class=\"testimonial-meta\">
              <h4>John DOE <small><a href=\"#\">microsoft.com</a></small></h4>
            </div>
          </div>
          <!-- end dmbox -->
        </div>

        <div class=\"col-lg-4 col-md-4 col-sm-12 last\">
          <div class=\"testimonial\">
            <img data-effect=\"slide-bottom\" class=\"alignleft img-circle\" src=\"img/team_06.png\" alt=\"\">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            <div class=\"testimonial-meta\">
              <h4>Jenny DOE <small><a href=\"#\">yahoo.com</a></small></h4>
            </div>
          </div>
          <!-- end dmbox -->
        </div>
        <!-- end col-lg-4 -->

      </div>
      <!-- end content -->
    </div>
    <!-- end container -->
  </section>
  <!-- end section -->


  <!-- Testimonial Carousel -->
  <div class=\"container withpadding\">
    <div class=\"row\">
      <div class=\"col-lg-4 col-lg-offset-4\">
        <div class=\"general-title text-center\">
          <h3>Mais aussi...</h3>
          <hr>
        </div>
      </div>
    </div>
    <! --/row -->

    <div class=\"row\">
      <div class=\"col-lg-8 col-lg-offset-2 text-center\">
        <div id=\"carousel-example-generic\" class=\"carousel slide\" data-ride=\"carousel\">
          <!-- Wrapper for slides -->
          <div class=\"carousel-inner\">
            <div class=\"item active\">
              <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.</h4>
              <p>Paul Morrison - BlackTie.co</p>
            </div>

            <div class=\"item\">
              <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.</h4>
              <p>Mike Wellington - BlackTie.co</p>
            </div>
          </div>
          <!-- /carousel-inner -->

        </div>
        <! --/carousel-example -->
      </div>
      <!-- /col-lg-8 -->
    </div>
    <! --/row -->
  </div>
  <!-- /container -->

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "MonzaBundle:Default:avis.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Vos avis</li>
        </ul>
        <h2>VOS AVIS</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>

  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">

        <div class=\"general-title text-center\">
          <h3>Lire les avis</h3>
          <hr>
        </div>

        <div class=\"col-lg-6 col-md-6 col-sm-12 first\">
          <div class=\"testimonial\">
            <img data-effect=\"slide-bottom\" class=\"alignleft img-circle\" src=\"img/team_01.png\" alt=\"\">
            <p>Je m'appelle Michel.</p>
            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            <div class=\"testimonial-meta\">
              <h4>Mark Soto <small><a href=\"#\">BlackTie.co</a></small></h4>
            </div>
          </div>
          <!-- end dmbox -->
        </div>

        <div class=\"col-lg-6 col-md-6 col-sm-12 last\">
          <div class=\"testimonial\">
            <img data-effect=\"slide-bottom\" class=\"alignleft img-circle\" src=\"img/team_03.png\" alt=\"\">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen
              book.</p>
            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            <div class=\"testimonial-meta\">
              <h4>Lisa Meyer <small><a href=\"#\">BlackTie.co</a></small></h4>
            </div>
          </div>
          <!-- end dmbox -->
        </div>
        <!-- end col-lg-6 -->

        <div class=\"col-lg-4 col-md-4 col-sm-12 first\">
          <div class=\"testimonial\">
            <img data-effect=\"slide-bottom\" class=\"alignleft img-circle\" src=\"img/team_02.png\" alt=\"\">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            <div class=\"testimonial-meta\">
              <h4>Jenny DOE <small><a href=\"#\">google.com</a></small></h4>
            </div>
          </div>
          <!-- end dmbox -->
        </div>

        <div class=\"col-lg-4 col-md-4 col-sm-12\">
          <div class=\"testimonial\">
            <img data-effect=\"slide-bottom\" class=\"alignleft img-circle\" src=\"img/team_05.png\" alt=\"\">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            <div class=\"testimonial-meta\">
              <h4>John DOE <small><a href=\"#\">microsoft.com</a></small></h4>
            </div>
          </div>
          <!-- end dmbox -->
        </div>

        <div class=\"col-lg-4 col-md-4 col-sm-12 last\">
          <div class=\"testimonial\">
            <img data-effect=\"slide-bottom\" class=\"alignleft img-circle\" src=\"img/team_06.png\" alt=\"\">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            <div class=\"testimonial-meta\">
              <h4>Jenny DOE <small><a href=\"#\">yahoo.com</a></small></h4>
            </div>
          </div>
          <!-- end dmbox -->
        </div>
        <!-- end col-lg-4 -->

      </div>
      <!-- end content -->
    </div>
    <!-- end container -->
  </section>
  <!-- end section -->


  <!-- Testimonial Carousel -->
  <div class=\"container withpadding\">
    <div class=\"row\">
      <div class=\"col-lg-4 col-lg-offset-4\">
        <div class=\"general-title text-center\">
          <h3>Mais aussi...</h3>
          <hr>
        </div>
      </div>
    </div>
    <! --/row -->

    <div class=\"row\">
      <div class=\"col-lg-8 col-lg-offset-2 text-center\">
        <div id=\"carousel-example-generic\" class=\"carousel slide\" data-ride=\"carousel\">
          <!-- Wrapper for slides -->
          <div class=\"carousel-inner\">
            <div class=\"item active\">
              <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.</h4>
              <p>Paul Morrison - BlackTie.co</p>
            </div>

            <div class=\"item\">
              <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.</h4>
              <p>Mike Wellington - BlackTie.co</p>
            </div>
          </div>
          <!-- /carousel-inner -->

        </div>
        <! --/carousel-example -->
      </div>
      <!-- /col-lg-8 -->
    </div>
    <! --/row -->
  </div>
  <!-- /container -->

{% endblock %}
", "MonzaBundle:Default:avis.html.twig", "C:\\Monza\\src\\MonzaBundle/Resources/views/Default/avis.html.twig");
    }
}
