<?php

/* MonzaBundle:Default:inscription.html.twig */
class __TwigTemplate_29d0b246c8d5f8292ec0254f0c5c2410d105f07cbc730e4c57fb813859dc7379 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "MonzaBundle:Default:inscription.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MonzaBundle:Default:inscription.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MonzaBundle:Default:inscription.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " Keman Racing";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Inscription</li>
        </ul>
        <h2>INSCRIPTION</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>
  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
        <div class=\"col-lg-4 col-md-4 col-sm-12\">
          <h4 class=\"title\">
             <span>Pourquoi s'inscrire ?</span>
          </h4>
          <p>Pour pouvoir choisir votre voiture ou le circuit sur lequel vous voulez rouler, il faut vous s'inscrire, mais ce n'est pas tout ! En vous inscrivant chez nous, vous pourrez bénéficier de nombreuses réduction sur l'ensemble de nos produits en boutique !</p>
          <p>Alors, qu'attendez-vous ? Ça ne prends que quelques minutes et le tour est joué !</p>
        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-12\">
          <h4 class=\"title\">
             <span>Avantages</span>
          </h4>
          <ul class=\"check\">
            <li>Choix multiples de voitures</li>
            <li>Des réductions sur notre boutique</li>
            <li>Inscription à la Newsletter </li>
            <li>Soirées privées pour tenter de gagner des prix !</li>
          </ul>
        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-12\">
          <h4 class=\"title\">
             <span>Je m'enregistre</span>
          </h4>
          <form id=\"registerform\" method=\"post\" name=\"registerform\" action=\"\">
            <div class=\"form-group\">
              <input type=\"text\" class=\"form-control\" placeholder=\"Prénom\">
            </div>
            <div class=\"form-group\">
              <input type=\"text\" class=\"form-control\" placeholder=\"Nom\">
            </div>
            <div class=\"form-group\">
              <input type=\"email\" class=\"form-control\" placeholder=\"Email\">
            </div>
            <div class=\"form-group\">
              <input type=\"password\" class=\"form-control\" placeholder=\"Mot de passe\">
            </div>
            <div class=\"form-group\">
              <input type=\"text\" class=\"form-control\" placeholder=\"Confirmez votre mot de passe\">
            </div>
            <div class=\"form-group\">
              <input type=\"submit\" class=\"button\" value=\"Je créer mon compte\">
             <a href=\"/connexion\">Vous avez déjà un compte ?</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "MonzaBundle:Default:inscription.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 5,  59 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block title %} Keman Racing{% endblock %}
{% block body %}

  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Inscription</li>
        </ul>
        <h2>INSCRIPTION</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>
  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
        <div class=\"col-lg-4 col-md-4 col-sm-12\">
          <h4 class=\"title\">
             <span>Pourquoi s'inscrire ?</span>
          </h4>
          <p>Pour pouvoir choisir votre voiture ou le circuit sur lequel vous voulez rouler, il faut vous s'inscrire, mais ce n'est pas tout ! En vous inscrivant chez nous, vous pourrez bénéficier de nombreuses réduction sur l'ensemble de nos produits en boutique !</p>
          <p>Alors, qu'attendez-vous ? Ça ne prends que quelques minutes et le tour est joué !</p>
        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-12\">
          <h4 class=\"title\">
             <span>Avantages</span>
          </h4>
          <ul class=\"check\">
            <li>Choix multiples de voitures</li>
            <li>Des réductions sur notre boutique</li>
            <li>Inscription à la Newsletter </li>
            <li>Soirées privées pour tenter de gagner des prix !</li>
          </ul>
        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-12\">
          <h4 class=\"title\">
             <span>Je m'enregistre</span>
          </h4>
          <form id=\"registerform\" method=\"post\" name=\"registerform\" action=\"\">
            <div class=\"form-group\">
              <input type=\"text\" class=\"form-control\" placeholder=\"Prénom\">
            </div>
            <div class=\"form-group\">
              <input type=\"text\" class=\"form-control\" placeholder=\"Nom\">
            </div>
            <div class=\"form-group\">
              <input type=\"email\" class=\"form-control\" placeholder=\"Email\">
            </div>
            <div class=\"form-group\">
              <input type=\"password\" class=\"form-control\" placeholder=\"Mot de passe\">
            </div>
            <div class=\"form-group\">
              <input type=\"text\" class=\"form-control\" placeholder=\"Confirmez votre mot de passe\">
            </div>
            <div class=\"form-group\">
              <input type=\"submit\" class=\"button\" value=\"Je créer mon compte\">
             <a href=\"/connexion\">Vous avez déjà un compte ?</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

{% endblock %}", "MonzaBundle:Default:inscription.html.twig", "C:\\Monza\\src\\MonzaBundle/Resources/views/Default/inscription.html.twig");
    }
}
