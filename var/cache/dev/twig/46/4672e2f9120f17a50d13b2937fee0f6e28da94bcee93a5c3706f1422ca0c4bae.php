<?php

/* MonzaBundle:Default:circuits.html.twig */
class __TwigTemplate_d34eaac2ffea7cd148d628b951d0cb9ef183d20d356890c42606c660b9ae5c0e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "MonzaBundle:Default:circuits.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MonzaBundle:Default:circuits.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MonzaBundle:Default:circuits.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Nos circuits</li>
        </ul>
        <h2>NOS CIRCUITS</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>


  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\" col-lg-12 col-md-12 col-sm-12 clearfix\">
        <div class=\"divider\">
        </div>
          <h1>JE CHOISIS D'ABORD MON CIRCUIT !</h1>
        <nav class=\"portfolio-filter clearfix\">
          <ul>
            <li><a href=\"#\" class=\"dmbutton2\" data-filter=\"*\">Tous les pays</a></li>
              ";
        // line 36
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["filteredCircuits"] ?? $this->getContext($context, "filteredCircuits")));
        foreach ($context['_seq'] as $context["_key"] => $context["circuit"]) {
            // line 37
            echo "            <li><a href=\"#\" class=\"dmbutton2\" data-filter=\".";
            echo twig_escape_filter($this->env, $context["circuit"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["circuit"], "html", null, true);
            echo "</a></li>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['circuit'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "          </ul>
        </nav>
        <div class=\"portfolio-centered\">
          <div class=\"recentitems portfolio\">

            ";
        // line 44
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["circuits"] ?? $this->getContext($context, "circuits")));
        foreach ($context['_seq'] as $context["_key"] => $context["circuit"]) {
            // line 45
            echo "            <div class=\"portfolio-item col-lg-6 col-md-6 col-sm-6 col-xs-12 ";
            echo $this->getAttribute($context["circuit"], "getCircuitPays", array(), "method");
            echo "\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"";
            // line 47
            echo $this->getAttribute($context["circuit"], "getCircuitImage", array());
            echo "\" alt=\"";
            echo $this->getAttribute($context["circuit"], "getCircuitName", array(), "method");
            echo "\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                  <h3 class=\"big a1\" data-animate=\"fadeInDown\">";
            // line 50
            echo $this->getAttribute($context["circuit"], "getCircuitName", array(), "method");
            echo "</h3>
                  <a data-rel=\"prettyPhoto\" href=\"";
            // line 51
            echo $this->getAttribute($context["circuit"], "getCircuitImage", array());
            echo "\"></a>
                  <a href=\"circuits/";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($context["circuit"], "getId", array(), "method"), "html", null, true);
            echo "\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                  <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                  <a href=\"circuits/";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($context["circuit"], "getId", array(), "method"), "html", null, true);
            echo "\">";
            echo $this->getAttribute($context["circuit"], "getCircuitTitre", array(), "method");
            echo "</a>
                  </div>
                </div>
              </div>
            </div>
            <h3 class=\"title\">";
            // line 59
            echo $this->getAttribute($context["circuit"], "getCircuitName", array(), "method");
            echo "</h3>
            </div>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['circuit'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "        <div class=\"divider\"></div>
      </div>
    </div></div></div>
  </section>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "MonzaBundle:Default:circuits.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 62,  146 => 59,  136 => 54,  131 => 52,  127 => 51,  123 => 50,  115 => 47,  109 => 45,  105 => 44,  98 => 39,  87 => 37,  83 => 36,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Nos circuits</li>
        </ul>
        <h2>NOS CIRCUITS</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>


  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\" col-lg-12 col-md-12 col-sm-12 clearfix\">
        <div class=\"divider\">
        </div>
          <h1>JE CHOISIS D'ABORD MON CIRCUIT !</h1>
        <nav class=\"portfolio-filter clearfix\">
          <ul>
            <li><a href=\"#\" class=\"dmbutton2\" data-filter=\"*\">Tous les pays</a></li>
              {% for circuit in filteredCircuits %}
            <li><a href=\"#\" class=\"dmbutton2\" data-filter=\".{{ circuit }}\">{{ circuit }}</a></li>
              {% endfor %}
          </ul>
        </nav>
        <div class=\"portfolio-centered\">
          <div class=\"recentitems portfolio\">

            {% for circuit in circuits %}
            <div class=\"portfolio-item col-lg-6 col-md-6 col-sm-6 col-xs-12 {{ circuit.getCircuitPays() | raw }}\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"{{circuit.getCircuitImage | raw }}\" alt=\"{{ circuit.getCircuitName() | raw }}\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                  <h3 class=\"big a1\" data-animate=\"fadeInDown\">{{ circuit.getCircuitName() | raw }}</h3>
                  <a data-rel=\"prettyPhoto\" href=\"{{circuit.getCircuitImage | raw }}\"></a>
                  <a href=\"circuits/{{circuit.getId()}}\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                  <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                  <a href=\"circuits/{{circuit.getId()}}\">{{ circuit.getCircuitTitre() | raw }}</a>
                  </div>
                </div>
              </div>
            </div>
            <h3 class=\"title\">{{ circuit.getCircuitName() | raw }}</h3>
            </div>
          {% endfor %}
        <div class=\"divider\"></div>
      </div>
    </div></div></div>
  </section>

{% endblock %}", "MonzaBundle:Default:circuits.html.twig", "C:\\Monza\\src\\MonzaBundle/Resources/views/Default/circuits.html.twig");
    }
}
