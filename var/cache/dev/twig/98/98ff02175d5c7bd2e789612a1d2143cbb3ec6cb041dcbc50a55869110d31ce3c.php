<?php

/* MonzaBundle:Default:faq.html.twig */
class __TwigTemplate_c060287c31bef14bda929df172bce97d0c8110083cdfb22ab83533c8896abb30 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "MonzaBundle:Default:faq.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MonzaBundle:Default:faq.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MonzaBundle:Default:faq.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Foire aux questions</li>
        </ul>
        <h2>F.A.Q.</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Search...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>
  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
        <div class=\"col-lg-4 col-md-4 col-sm-12\">
          <div class=\"dmbox\">
            <div class=\"service-icon\">
              <div class=\"dm-icon-effect-1\" data-effect=\"slide-bottom\">
                <i class=\"dm-icon fa fa-question fa-3x\"></i>
              </div>
            </div>
            <h4>1. Vous avez une question à nous poser ?</h4>
            <p>Notre équipe de communication mettra tout en oeuvre pour vous répondre dans les délais les plus courts et de manière la plus précise.</p>
          </div>
        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-12\">
          <div class=\"dmbox\">
            <div class=\"service-icon\">
              <div class=\"dm-icon-effect-1\" data-effect=\"slide-bottom\">
                <i class=\"dm-icon fa fa-envelope-o fa-3x\"></i>
              </div>
            </div>
            <h4>2. Un message à nous faire parvenir ?</h4>
            <p>Que ce soit une critique ou des remerciements, nous sommes joignable par tous les biais : Courrier, téléphones, mails, réseaux sociaux...</p>
          </div>
        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-12\">
          <div class=\"dmbox\">
            <div class=\"service-icon\">
              <div class=\"dm-icon-effect-1\" data-effect=\"slide-bottom\">
                <i class=\"dm-icon fa fa-random fa-3x\"></i>
              </div>
            </div>
            <h4>3. Une réponse à vos messages !</h4>
            <p>Nous répondons aux messages que vous nous envoyés, peu importe la demande, nous faisons tout notre possible pour vous répondre dans les plus brefs délais.</p>
          </div>
        </div>
        <div class=\"clearfix\"></div>
        <div class=\"divider\"></div>
        <div class=\"general-title text-center\">
          <h3>LES QUESTIONS LES PLUS SOUVENT POSÉES</h3>
          <hr>
        </div>
        <div class=\"clearfix\"></div>
        <div class=\"divider\"></div>
        <div class=\"row\">
          <div class=\"col-lg-7\">
            <div class=\"accordion\" id=\"accordion2\">
              <div class=\"accordion-group\">
                <div class=\"accordion-heading\">
                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\" href=\"faq.html#collapseOne\">
                            <em class=\"glyphicon glyphicon-chevron-right icon-fixed-width\"></em>L'assurance est-elle comprise dans le forfait, même amateur ?
                          </a>
                </div>
                <div id=\"collapseOne\" class=\"accordion-body collapse in\">
                  <div class=\"accordion-inner\">
                    <p>Chaque personne est assurée entièrement. Nous avons une assurance tout risques, pour le stagiaire, le tuteur et la voiture.</p>
                  </div>
                </div>
              </div>
              <div class=\"accordion-group\">
                <div class=\"accordion-heading\">
                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\" href=\"faq.html#collapseTwo\">
                            <em class=\"glyphicon glyphicon-chevron-right icon-fixed-width\"></em>Il y a-t- il un délai minimum pour annuler ?
                          </a>
                </div>
                <div id=\"collapseTwo\" class=\"accordion-body collapse\">
                  <div class=\"accordion-inner\">
                    <p>Oui, il y a un délai de rétractation, c'est minimum 24 heures avant le jour qui a été choisi.</p>
                  </div>
                </div>
              </div>
              <div class=\"accordion-group\">
                <div class=\"accordion-heading\">
                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\" href=\"faq.html#collapseThree\">
                            <em class=\"glyphicon glyphicon-chevron-right icon-fixed-width\"></em>Peut-on rouler sur tous les circuits toute l'année ?
                          </a>
                </div>
                <div id=\"collapseThree\" class=\"accordion-body collapse\">
                  <div class=\"accordion-inner\">
                    <p>Il arrive que certains circuits refont peaux neuves et doîvent être fermés le temps des travaux (Monza en 2022 vont supprimer une chicane pour garder que le circuit soit encore plus rapide). Nous le signalons par mails, sur les calendriers et sur notre site.</p>
                  </div>
                </div>
              </div>
              <div class=\"accordion-group\">
                <div class=\"accordion-heading\">
                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\" href=\"faq.html#collapseFour\">
                            <em class=\"glyphicon glyphicon-chevron-right icon-fixed-width\"></em>Est-il possible de privatiser un circuit pour un groupe ?
                          </a>
                </div>
                <div id=\"collapseFour\" class=\"accordion-body collapse\">
                  <div class=\"accordion-inner\">
                    <p>Oui, il est possible de privatiser le circuit, nous avons l'habitude de ce genre de demandes.</p>
                  </div>
                </div>
              </div>
              <div class=\"accordion-group\">
                <div class=\"accordion-heading\">
                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\" href=\"faq.html#collapseFive\">
                                <em class=\"glyphicon glyphicon-chevron-right icon-fixed-width\"></em>Peut-on rouler la nuit ?
                              </a>
                </div>
                <div id=\"collapseFive\" class=\"accordion-body collapse\">
                  <div class=\"accordion-inner\">
                    <p>Oui, mais selon certaines voitures uniquement, la plupart des voitures sont équipées de phares, mais certaines ne sont pas équipées (Formule 1 par exemple). Seules les voitures équipées de phares peuvent rouler, car la plupart des circuits ne sont pas éclairés.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class=\"col-lg-5 text-center\">
            <h4>Besoin d'un renseignement ?</h4>
            <img src=\"";
        // line 136
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/support.png"), "html", null, true);
        echo "\" width=\"120\" alt=\"\">
            <p>Nous pouvons vous répondre 24h / 24, 7j / 7<br/>N'hésitez à nous contacter.</p>
            <p>
              <i class=\"fa fa-mobile\"></i> 01 12 23 45 56<br/>
              <i class=\"fa fa-envelope-o\"></i> info@kemanracing.com<br/>
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "MonzaBundle:Default:faq.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 136,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Foire aux questions</li>
        </ul>
        <h2>F.A.Q.</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Search...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>
  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
        <div class=\"col-lg-4 col-md-4 col-sm-12\">
          <div class=\"dmbox\">
            <div class=\"service-icon\">
              <div class=\"dm-icon-effect-1\" data-effect=\"slide-bottom\">
                <i class=\"dm-icon fa fa-question fa-3x\"></i>
              </div>
            </div>
            <h4>1. Vous avez une question à nous poser ?</h4>
            <p>Notre équipe de communication mettra tout en oeuvre pour vous répondre dans les délais les plus courts et de manière la plus précise.</p>
          </div>
        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-12\">
          <div class=\"dmbox\">
            <div class=\"service-icon\">
              <div class=\"dm-icon-effect-1\" data-effect=\"slide-bottom\">
                <i class=\"dm-icon fa fa-envelope-o fa-3x\"></i>
              </div>
            </div>
            <h4>2. Un message à nous faire parvenir ?</h4>
            <p>Que ce soit une critique ou des remerciements, nous sommes joignable par tous les biais : Courrier, téléphones, mails, réseaux sociaux...</p>
          </div>
        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-12\">
          <div class=\"dmbox\">
            <div class=\"service-icon\">
              <div class=\"dm-icon-effect-1\" data-effect=\"slide-bottom\">
                <i class=\"dm-icon fa fa-random fa-3x\"></i>
              </div>
            </div>
            <h4>3. Une réponse à vos messages !</h4>
            <p>Nous répondons aux messages que vous nous envoyés, peu importe la demande, nous faisons tout notre possible pour vous répondre dans les plus brefs délais.</p>
          </div>
        </div>
        <div class=\"clearfix\"></div>
        <div class=\"divider\"></div>
        <div class=\"general-title text-center\">
          <h3>LES QUESTIONS LES PLUS SOUVENT POSÉES</h3>
          <hr>
        </div>
        <div class=\"clearfix\"></div>
        <div class=\"divider\"></div>
        <div class=\"row\">
          <div class=\"col-lg-7\">
            <div class=\"accordion\" id=\"accordion2\">
              <div class=\"accordion-group\">
                <div class=\"accordion-heading\">
                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\" href=\"faq.html#collapseOne\">
                            <em class=\"glyphicon glyphicon-chevron-right icon-fixed-width\"></em>L'assurance est-elle comprise dans le forfait, même amateur ?
                          </a>
                </div>
                <div id=\"collapseOne\" class=\"accordion-body collapse in\">
                  <div class=\"accordion-inner\">
                    <p>Chaque personne est assurée entièrement. Nous avons une assurance tout risques, pour le stagiaire, le tuteur et la voiture.</p>
                  </div>
                </div>
              </div>
              <div class=\"accordion-group\">
                <div class=\"accordion-heading\">
                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\" href=\"faq.html#collapseTwo\">
                            <em class=\"glyphicon glyphicon-chevron-right icon-fixed-width\"></em>Il y a-t- il un délai minimum pour annuler ?
                          </a>
                </div>
                <div id=\"collapseTwo\" class=\"accordion-body collapse\">
                  <div class=\"accordion-inner\">
                    <p>Oui, il y a un délai de rétractation, c'est minimum 24 heures avant le jour qui a été choisi.</p>
                  </div>
                </div>
              </div>
              <div class=\"accordion-group\">
                <div class=\"accordion-heading\">
                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\" href=\"faq.html#collapseThree\">
                            <em class=\"glyphicon glyphicon-chevron-right icon-fixed-width\"></em>Peut-on rouler sur tous les circuits toute l'année ?
                          </a>
                </div>
                <div id=\"collapseThree\" class=\"accordion-body collapse\">
                  <div class=\"accordion-inner\">
                    <p>Il arrive que certains circuits refont peaux neuves et doîvent être fermés le temps des travaux (Monza en 2022 vont supprimer une chicane pour garder que le circuit soit encore plus rapide). Nous le signalons par mails, sur les calendriers et sur notre site.</p>
                  </div>
                </div>
              </div>
              <div class=\"accordion-group\">
                <div class=\"accordion-heading\">
                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\" href=\"faq.html#collapseFour\">
                            <em class=\"glyphicon glyphicon-chevron-right icon-fixed-width\"></em>Est-il possible de privatiser un circuit pour un groupe ?
                          </a>
                </div>
                <div id=\"collapseFour\" class=\"accordion-body collapse\">
                  <div class=\"accordion-inner\">
                    <p>Oui, il est possible de privatiser le circuit, nous avons l'habitude de ce genre de demandes.</p>
                  </div>
                </div>
              </div>
              <div class=\"accordion-group\">
                <div class=\"accordion-heading\">
                  <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\" href=\"faq.html#collapseFive\">
                                <em class=\"glyphicon glyphicon-chevron-right icon-fixed-width\"></em>Peut-on rouler la nuit ?
                              </a>
                </div>
                <div id=\"collapseFive\" class=\"accordion-body collapse\">
                  <div class=\"accordion-inner\">
                    <p>Oui, mais selon certaines voitures uniquement, la plupart des voitures sont équipées de phares, mais certaines ne sont pas équipées (Formule 1 par exemple). Seules les voitures équipées de phares peuvent rouler, car la plupart des circuits ne sont pas éclairés.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class=\"col-lg-5 text-center\">
            <h4>Besoin d'un renseignement ?</h4>
            <img src=\"{{ asset('assets/img/support.png') }}\" width=\"120\" alt=\"\">
            <p>Nous pouvons vous répondre 24h / 24, 7j / 7<br/>N'hésitez à nous contacter.</p>
            <p>
              <i class=\"fa fa-mobile\"></i> 01 12 23 45 56<br/>
              <i class=\"fa fa-envelope-o\"></i> info@kemanracing.com<br/>
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>

{% endblock %}", "MonzaBundle:Default:faq.html.twig", "C:\\Monza\\src\\MonzaBundle/Resources/views/Default/faq.html.twig");
    }
}
