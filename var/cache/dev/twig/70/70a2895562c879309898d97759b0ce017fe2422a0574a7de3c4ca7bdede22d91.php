<?php

/* @FOSUser/Security/login_content.html.twig */
class __TwigTemplate_7999c4e03065cc88cbf494dd0459b6eff60337c20812df2666db36499dbb6dbc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        // line 2
        echo "

";
        // line 4
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 5
            echo "    <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 6
        echo " 



<form id=\"loginform\" method=\"post\" name=\"loginform\" action=\"";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_check");
        echo "\">
     ";
        // line 11
        if (($context["csrf_token"] ?? $this->getContext($context, "csrf_token"))) {
            // line 12
            echo "        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            echo twig_escape_filter($this->env, ($context["csrf_token"] ?? $this->getContext($context, "csrf_token")), "html", null, true);
            echo "\" />
    ";
        }
        // line 14
        echo "
    <div class=\"form-group\">
      <div class=\"input-group\">
        <span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>
        <input type=\"text\" class=\"form-control\" placeholder=\"Nom\" id=\"username\" name=\"_username\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, ($context["last_username"] ?? $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\" autocomplete=\"username\" >
      </div>
    </div>
    <div class=\"form-group\">
      <div class=\"input-group\">
        <span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>
        <input type=\"password\" class=\"form-control\" placeholder=\"Mot de passe\" type=\"password\" id=\"password\" name=\"_password\" required=\"required\" autocomplete=\"curent-password\" >
      </div>
    </div>
    <div class=\"form-group\">
      <div class=\"checkbox\">
        <label>
          <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" > Se souvenir de moi
        </label>
      </div>
    </div>
    <div class=\"form-group\">
      <!-- <button type=\"submit\" class=\"button\">Je me connecte</button> -->
      <input type=\"submit\" id=\"_submit\" class=\"button\" name=\"_submit\" value=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
</form>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 36,  61 => 18,  55 => 14,  49 => 12,  47 => 11,  43 => 10,  37 => 6,  31 => 5,  29 => 4,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}


{% if error %}
    <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
{% endif %} 



<form id=\"loginform\" method=\"post\" name=\"loginform\" action=\"{{ path(\"fos_user_security_check\") }}\">
     {% if csrf_token %}
        <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\" />
    {% endif %}

    <div class=\"form-group\">
      <div class=\"input-group\">
        <span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>
        <input type=\"text\" class=\"form-control\" placeholder=\"Nom\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" required=\"required\" autocomplete=\"username\" >
      </div>
    </div>
    <div class=\"form-group\">
      <div class=\"input-group\">
        <span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>
        <input type=\"password\" class=\"form-control\" placeholder=\"Mot de passe\" type=\"password\" id=\"password\" name=\"_password\" required=\"required\" autocomplete=\"curent-password\" >
      </div>
    </div>
    <div class=\"form-group\">
      <div class=\"checkbox\">
        <label>
          <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" > Se souvenir de moi
        </label>
      </div>
    </div>
    <div class=\"form-group\">
      <!-- <button type=\"submit\" class=\"button\">Je me connecte</button> -->
      <input type=\"submit\" id=\"_submit\" class=\"button\" name=\"_submit\" value=\"{{ 'security.login.submit'|trans }}\" />
    </div>
</form>
", "@FOSUser/Security/login_content.html.twig", "C:\\Monza\\app\\Resources\\FOSUserBundle\\views\\Security\\login_content.html.twig");
    }
}
