<?php

/* MonzaBundle:Default:nouvelles.html.twig */
class __TwigTemplate_90c963c8fd57e020308a58ec9e2457af717b7479bd86ccde8307daaeba1b343d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "MonzaBundle:Default:nouvelles.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MonzaBundle:Default:nouvelles.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MonzaBundle:Default:nouvelles.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Nouvelles voitures</li>
        </ul>
        <h2>NOUVELLES VOITURES</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>

            <div class=\"portfolio-item col-lg-4 col-md-4 col-sm-4 col-xs-12 mockups\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"img/portfolio_01.jpg\" alt=\"\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                    <h3 class=\"big a1\" data-animate=\"fadeInDown\">Project Name Here</h3>
                    <a data-rel=\"prettyPhoto\" href=\"img/portfolio_01.jpg\" class=\"dmbutton a2\" data-animate=\"bounceInLeft\"><i class=\"fa fa-search\"></i></a>
                    <a href=\"#\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                    <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                    </div>
                    <!-- portfolio_category -->
                  </div>
                  <!-- he bg -->
                </div>
                <!-- he view -->
              </div>
              <!-- he wrap -->
              <h3 class=\"title\">Toyota Eagle GTP Mk II</h3>

            </div>
            <!-- end col-4 -->

            <div class=\"portfolio-item col-lg-4 col-md-4 col-sm-4 col-xs-12 web-design graphic-design\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"img/portfolio_02.jpg\" alt=\"\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                    <h3 class=\"big a1\" data-animate=\"fadeInDown\">Project Name Here</h3>
                    <a data-rel=\"prettyPhoto\" href=\"img/portfolio_02.jpg\" class=\"dmbutton a2\" data-animate=\"bounceInLeft\"><i class=\"fa fa-search\"></i></a>
                    <a href=\"#\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                    <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                    </div>
                    <!-- portfolio_category -->
                  </div>
                  <!-- he bg -->
                </div>
                <!-- he view -->
              </div>
              <!-- he wrap -->
              <h3 class=\"title\">Project Name - 2</h3>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\"s standard dummy..</p>
            </div>
            <!-- end col-4 -->

            <div class=\"portfolio-item col-lg-4 col-md-4 col-sm-4 col-xs-12 graphic-design\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"img/portfolio_03.jpg\" alt=\"\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                    <h3 class=\"big a1\" data-animate=\"fadeInDown\">Project Name Here</h3>
                    <a data-rel=\"prettyPhoto\" href=\"img/portfolio_03.jpg\" class=\"dmbutton a2\" data-animate=\"bounceInLeft\"><i class=\"fa fa-search\"></i></a>
                    <a href=\"#\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                    <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                    </div>
                    <!-- portfolio_category -->
                  </div>
                  <!-- he bg -->
                </div>
                <!-- he view -->
              </div>
              <!-- he wrap -->
              <h3 class=\"title\">Project Name - 3</h3>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\"s standard dummy..</p>
            </div>
            <!-- end col-4 -->

            <div class=\"portfolio-item col-lg-4 col-md-4 col-sm-4 col-xs-12 mockups\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"img/portfolio_04.jpg\" alt=\"\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                    <h3 class=\"big a1\" data-animate=\"fadeInDown\">Project Name Here</h3>
                    <a data-rel=\"prettyPhoto\" href=\"img/portfolio_04.jpg\" class=\"dmbutton a2\" data-animate=\"bounceInLeft\"><i class=\"fa fa-search\"></i></a>
                    <a href=\"#\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                    <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                    </div>
                    <!-- portfolio_category -->
                  </div>
                  <!-- he bg -->
                </div>
                <!-- he view -->
              </div>
              <!-- he wrap -->
              <h3 class=\"title\">Project Name - 4</h3>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\"s standard dummy..</p>
            </div>
            <!-- end col-12 -->

            <div class=\"portfolio-item col-lg-4 col-md-4 col-sm-4 col-xs-12 web-design\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"img/portfolio_05.jpg\" alt=\"\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                    <h3 class=\"big a1\" data-animate=\"fadeInDown\">Project Name Here</h3>
                    <a data-rel=\"prettyPhoto\" href=\"img/portfolio_05.jpg\" class=\"dmbutton a2\" data-animate=\"bounceInLeft\"><i class=\"fa fa-search\"></i></a>
                    <a href=\"#\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                    <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                    </div>
                    <!-- portfolio_category -->
                  </div>
                  <!-- he bg -->
                </div>
                <!-- he view -->
              </div>
              <!-- he wrap -->
              <h3 class=\"title\">Project Name - 5</h3>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\"s standard dummy..</p>
            </div>
            <!-- end col-12 -->

            <div class=\"portfolio-item col-lg-4 col-md-4 col-sm-4 col-xs-12 web-design\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"img/portfolio_06.jpg\" alt=\"\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                    <h3 class=\"big a1\" data-animate=\"fadeInDown\">Project Name Here</h3>
                    <a data-rel=\"prettyPhoto\" href=\"img/portfolio_06.jpg\" class=\"dmbutton a2\" data-animate=\"bounceInLeft\"><i class=\"fa fa-search\"></i></a>
                    <a href=\"#\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                    <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                    </div>
                    <!-- portfolio_category -->
                  </div>
                  <!-- he bg -->
                </div>
                <!-- he view -->
              </div>
              <!-- he wrap -->
              <h3 class=\"title\">Project Name - 6</h3>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\"s standard dummy..</p>
            </div>
            <!-- end col-12 -->

          </div>
          <!-- portfolio -->
        </div>
        <!-- portfolio container -->
        <div class=\"divider\"></div>
      </div>
      <!-- end container -->
  </section>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "MonzaBundle:Default:nouvelles.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Nouvelles voitures</li>
        </ul>
        <h2>NOUVELLES VOITURES</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>

            <div class=\"portfolio-item col-lg-4 col-md-4 col-sm-4 col-xs-12 mockups\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"img/portfolio_01.jpg\" alt=\"\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                    <h3 class=\"big a1\" data-animate=\"fadeInDown\">Project Name Here</h3>
                    <a data-rel=\"prettyPhoto\" href=\"img/portfolio_01.jpg\" class=\"dmbutton a2\" data-animate=\"bounceInLeft\"><i class=\"fa fa-search\"></i></a>
                    <a href=\"#\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                    <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                    </div>
                    <!-- portfolio_category -->
                  </div>
                  <!-- he bg -->
                </div>
                <!-- he view -->
              </div>
              <!-- he wrap -->
              <h3 class=\"title\">Toyota Eagle GTP Mk II</h3>

            </div>
            <!-- end col-4 -->

            <div class=\"portfolio-item col-lg-4 col-md-4 col-sm-4 col-xs-12 web-design graphic-design\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"img/portfolio_02.jpg\" alt=\"\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                    <h3 class=\"big a1\" data-animate=\"fadeInDown\">Project Name Here</h3>
                    <a data-rel=\"prettyPhoto\" href=\"img/portfolio_02.jpg\" class=\"dmbutton a2\" data-animate=\"bounceInLeft\"><i class=\"fa fa-search\"></i></a>
                    <a href=\"#\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                    <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                    </div>
                    <!-- portfolio_category -->
                  </div>
                  <!-- he bg -->
                </div>
                <!-- he view -->
              </div>
              <!-- he wrap -->
              <h3 class=\"title\">Project Name - 2</h3>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\"s standard dummy..</p>
            </div>
            <!-- end col-4 -->

            <div class=\"portfolio-item col-lg-4 col-md-4 col-sm-4 col-xs-12 graphic-design\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"img/portfolio_03.jpg\" alt=\"\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                    <h3 class=\"big a1\" data-animate=\"fadeInDown\">Project Name Here</h3>
                    <a data-rel=\"prettyPhoto\" href=\"img/portfolio_03.jpg\" class=\"dmbutton a2\" data-animate=\"bounceInLeft\"><i class=\"fa fa-search\"></i></a>
                    <a href=\"#\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                    <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                    </div>
                    <!-- portfolio_category -->
                  </div>
                  <!-- he bg -->
                </div>
                <!-- he view -->
              </div>
              <!-- he wrap -->
              <h3 class=\"title\">Project Name - 3</h3>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\"s standard dummy..</p>
            </div>
            <!-- end col-4 -->

            <div class=\"portfolio-item col-lg-4 col-md-4 col-sm-4 col-xs-12 mockups\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"img/portfolio_04.jpg\" alt=\"\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                    <h3 class=\"big a1\" data-animate=\"fadeInDown\">Project Name Here</h3>
                    <a data-rel=\"prettyPhoto\" href=\"img/portfolio_04.jpg\" class=\"dmbutton a2\" data-animate=\"bounceInLeft\"><i class=\"fa fa-search\"></i></a>
                    <a href=\"#\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                    <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                    </div>
                    <!-- portfolio_category -->
                  </div>
                  <!-- he bg -->
                </div>
                <!-- he view -->
              </div>
              <!-- he wrap -->
              <h3 class=\"title\">Project Name - 4</h3>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\"s standard dummy..</p>
            </div>
            <!-- end col-12 -->

            <div class=\"portfolio-item col-lg-4 col-md-4 col-sm-4 col-xs-12 web-design\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"img/portfolio_05.jpg\" alt=\"\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                    <h3 class=\"big a1\" data-animate=\"fadeInDown\">Project Name Here</h3>
                    <a data-rel=\"prettyPhoto\" href=\"img/portfolio_05.jpg\" class=\"dmbutton a2\" data-animate=\"bounceInLeft\"><i class=\"fa fa-search\"></i></a>
                    <a href=\"#\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                    <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                    </div>
                    <!-- portfolio_category -->
                  </div>
                  <!-- he bg -->
                </div>
                <!-- he view -->
              </div>
              <!-- he wrap -->
              <h3 class=\"title\">Project Name - 5</h3>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\"s standard dummy..</p>
            </div>
            <!-- end col-12 -->

            <div class=\"portfolio-item col-lg-4 col-md-4 col-sm-4 col-xs-12 web-design\">
              <div class=\"he-wrap tpl6 market-item\">
                <img src=\"img/portfolio_06.jpg\" alt=\"\">
                <div class=\"he-view\">
                  <div class=\"bg a0\" data-animate=\"fadeIn\">
                    <h3 class=\"big a1\" data-animate=\"fadeInDown\">Project Name Here</h3>
                    <a data-rel=\"prettyPhoto\" href=\"img/portfolio_06.jpg\" class=\"dmbutton a2\" data-animate=\"bounceInLeft\"><i class=\"fa fa-search\"></i></a>
                    <a href=\"#\" class=\"dmbutton a2\" data-animate=\"bounceInRight\"><i class=\"fa fa-link\"></i></a>
                    <div class=\"portfolio_category text-center a2\" data-animate=\"fadeIn\">
                    </div>
                    <!-- portfolio_category -->
                  </div>
                  <!-- he bg -->
                </div>
                <!-- he view -->
              </div>
              <!-- he wrap -->
              <h3 class=\"title\">Project Name - 6</h3>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\"s standard dummy..</p>
            </div>
            <!-- end col-12 -->

          </div>
          <!-- portfolio -->
        </div>
        <!-- portfolio container -->
        <div class=\"divider\"></div>
      </div>
      <!-- end container -->
  </section>

{% endblock %}", "MonzaBundle:Default:nouvelles.html.twig", "C:\\Monza\\src\\MonzaBundle/Resources/views/Default/nouvelles.html.twig");
    }
}
