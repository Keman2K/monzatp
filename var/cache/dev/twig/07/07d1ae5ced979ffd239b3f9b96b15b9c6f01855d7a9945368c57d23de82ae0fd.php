<?php

/* MonzaBundle:Default:contact.html.twig */
class __TwigTemplate_8aeda26d72ba0abb95a282df397fa11454329d0fc86b4c6d23d5dc444bc49607 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "MonzaBundle:Default:contact.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MonzaBundle:Default:contact.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MonzaBundle:Default:contact.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Contact</li>
        </ul>
        <h2>CONTACT</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>

  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
        <div class=\"col-lg-6 col-md-6 col-sm-6\">
          <h4 class=\"title\">Formulaire de contact</h4>
          <div id=\"message\"></div>
          <form class=\"contact-form php-mail-form\" role=\"form\" action=\"contactform/contactform.php\" method=\"POST\">
            <div class=\"form-group\">
              <input type=\"name\" name=\"name\" class=\"form-control\" id=\"contact-name\" placeholder=\"Votre nom\" data-rule=\"minlen:4\" data-msg=\"Veuillez entrer un nom de 3 caractères minimum\" >
              <div class=\"validate\"></div>
            </div>
            <div class=\"form-group\">
              <input type=\"email\" name=\"email\" class=\"form-control\" id=\"contact-email\" placeholder=\"Votre email\" data-rule=\"email\" data-msg=\"Veuillez entrer une adresse email valide\">
              <div class=\"validate\"></div>
            </div>
            <div class=\"form-group\">
              <input type=\"text\" name=\"subject\" class=\"form-control\" id=\"contact-subject\" placeholder=\"Sujet\" data-rule=\"minlen:8\" data-msg=\"Veuillez entrer un message de 8 caractères minimum\">
              <div class=\"validate\"></div>
            </div>
            <div class=\"form-group\">
              <textarea class=\"form-control\" name=\"message\" id=\"contact-message\" placeholder=\"Votre message\" rows=\"5\" data-rule=\"required\" data-msg=\"Dites nous votre message\"></textarea>
              <div class=\"validate\"></div>
            </div>
            <div class=\"loading\"></div>
            <div class=\"error-message\"></div>
            <div class=\"sent-message\">Votre message à bien été envoyé, Merci !</div>
            <div class=\"form-send\">
              <button type=\"submit\" class=\"btn btn-large btn-primary\">Envoyer message</button>
            </div>
          </form>
        </div>
        <div class=\"col-lg-6 col-md-6 col-sm-6\">
          <h4 class=\"title\">Les détails pour nous contacter</h4>
          <p>Nous sommes ouverts 7 jours sur 7, nos bureaux ouvrent a 9h et fermes à 17h, mais une permanence est présente lorsque les bureaux sont fermés pour vous répondre.</p>
          <o>Vous pouvez nous joindre par mail, par téléphone ou par courrier. Nos informations de contact se trouvent ci-dessous : </o>
          <ul class=\"contact_details\">
            <li><i class=\"fa fa-envelope-o\"></i> info@kemanracing.com</li>
            <li><i class=\"fa fa-phone-square\"></i> 01 12 23 45 56</li>
            <li><i class=\"fa fa-home\"></i>  Siège social, 2B Rue Jean Macé, 78360 Montesson</li>
          </ul>
        </div>
        <div class=\"clearfix\"></div>
        <div class=\"divider\"></div>
        <h4 class=\"title\">Nos réseaux sociaux</h4>
        <div class=\"col-lg-3 col-md-3 col-sm-3\">
          <div class=\"servicebox text-center\">
            <div class=\"service-icon\">
              <div class=\"dm-icon-effect-1\" data-effect=\"slide-bottom\">
                <a href=\"https://www.facebook.com/\" class=\"\"> <i class=\"dm-icon fa fa-facebook fa-3x\"></i> </a>
              </div>
              <div class=\"servicetitle\">
                <h4>Facebook</h4>
              </div>
            </div>
          </div>
        </div>
        <div class=\"col-lg-3 col-md-3 col-sm-3\">
          <div class=\"servicebox text-center\">
            <div class=\"service-icon\">
              <div class=\"dm-icon-effect-1\" data-effect=\"slide-bottom\">
                <a href=\"https://twitter.com/?lang=fr\" class=\"\"> <i class=\"dm-icon fa fa-twitter fa-3x\"></i> </a>
              </div>
              <div class=\"servicetitle\">
                <h4>Twitter</h4>
              </div>
            </div>
          </div>
        </div>
        <div class=\"col-lg-3 col-md-3 col-sm-3\">
          <div class=\"servicebox text-center\">
            <div class=\"service-icon\">
              <div class=\"dm-icon-effect-1\" data-effect=\"slide-bottom\">
                <a href=\"https://plus.google.com\" class=\"\"> <i class=\"dm-icon fa fa-google-plus fa-3x\"></i> </a>
              </div>
              <div class=\"servicetitle\">
                <h4>Google Plus</h4>
              </div>
            </div>
          </div>
        </div>
        <div class=\"col-lg-3 col-md-3 col-sm-3\">
          <div class=\"servicebox text-center\">
            <div class=\"service-icon\">
              <div class=\"dm-icon-effect-1\" data-effect=\"slide-bottom\">
                <a href=\"https://www.youtube.com/\" class=\"\"> <i class=\"dm-icon fa fa-youtube fa-3x\"></i> </a>
              </div>
              <div class=\"servicetitle\">
                <h4>Youtube</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "MonzaBundle:Default:contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}

  <section class=\"post-wrapper-top\">
    <div class=\"container\">
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <ul class=\"breadcrumb\">
          <li><a href=\"/\">Acceuil</a></li>
          <li>Contact</li>
        </ul>
        <h2>CONTACT</h2>
      </div>
      <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
        <div class=\"search-bar\">
          <form action=\"\" method=\"get\">
            <fieldset>
              <input type=\"image\" src=\"img/pixel.gif\" class=\"searchsubmit\" alt=\"\" />
              <input type=\"text\" class=\"search_text showtextback\" name=\"s\" id=\"s\" value=\"Chercher...\" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>

  <section class=\"section1\">
    <div class=\"container clearfix\">
      <div class=\"content col-lg-12 col-md-12 col-sm-12 clearfix\">
        <div class=\"col-lg-6 col-md-6 col-sm-6\">
          <h4 class=\"title\">Formulaire de contact</h4>
          <div id=\"message\"></div>
          <form class=\"contact-form php-mail-form\" role=\"form\" action=\"contactform/contactform.php\" method=\"POST\">
            <div class=\"form-group\">
              <input type=\"name\" name=\"name\" class=\"form-control\" id=\"contact-name\" placeholder=\"Votre nom\" data-rule=\"minlen:4\" data-msg=\"Veuillez entrer un nom de 3 caractères minimum\" >
              <div class=\"validate\"></div>
            </div>
            <div class=\"form-group\">
              <input type=\"email\" name=\"email\" class=\"form-control\" id=\"contact-email\" placeholder=\"Votre email\" data-rule=\"email\" data-msg=\"Veuillez entrer une adresse email valide\">
              <div class=\"validate\"></div>
            </div>
            <div class=\"form-group\">
              <input type=\"text\" name=\"subject\" class=\"form-control\" id=\"contact-subject\" placeholder=\"Sujet\" data-rule=\"minlen:8\" data-msg=\"Veuillez entrer un message de 8 caractères minimum\">
              <div class=\"validate\"></div>
            </div>
            <div class=\"form-group\">
              <textarea class=\"form-control\" name=\"message\" id=\"contact-message\" placeholder=\"Votre message\" rows=\"5\" data-rule=\"required\" data-msg=\"Dites nous votre message\"></textarea>
              <div class=\"validate\"></div>
            </div>
            <div class=\"loading\"></div>
            <div class=\"error-message\"></div>
            <div class=\"sent-message\">Votre message à bien été envoyé, Merci !</div>
            <div class=\"form-send\">
              <button type=\"submit\" class=\"btn btn-large btn-primary\">Envoyer message</button>
            </div>
          </form>
        </div>
        <div class=\"col-lg-6 col-md-6 col-sm-6\">
          <h4 class=\"title\">Les détails pour nous contacter</h4>
          <p>Nous sommes ouverts 7 jours sur 7, nos bureaux ouvrent a 9h et fermes à 17h, mais une permanence est présente lorsque les bureaux sont fermés pour vous répondre.</p>
          <o>Vous pouvez nous joindre par mail, par téléphone ou par courrier. Nos informations de contact se trouvent ci-dessous : </o>
          <ul class=\"contact_details\">
            <li><i class=\"fa fa-envelope-o\"></i> info@kemanracing.com</li>
            <li><i class=\"fa fa-phone-square\"></i> 01 12 23 45 56</li>
            <li><i class=\"fa fa-home\"></i>  Siège social, 2B Rue Jean Macé, 78360 Montesson</li>
          </ul>
        </div>
        <div class=\"clearfix\"></div>
        <div class=\"divider\"></div>
        <h4 class=\"title\">Nos réseaux sociaux</h4>
        <div class=\"col-lg-3 col-md-3 col-sm-3\">
          <div class=\"servicebox text-center\">
            <div class=\"service-icon\">
              <div class=\"dm-icon-effect-1\" data-effect=\"slide-bottom\">
                <a href=\"https://www.facebook.com/\" class=\"\"> <i class=\"dm-icon fa fa-facebook fa-3x\"></i> </a>
              </div>
              <div class=\"servicetitle\">
                <h4>Facebook</h4>
              </div>
            </div>
          </div>
        </div>
        <div class=\"col-lg-3 col-md-3 col-sm-3\">
          <div class=\"servicebox text-center\">
            <div class=\"service-icon\">
              <div class=\"dm-icon-effect-1\" data-effect=\"slide-bottom\">
                <a href=\"https://twitter.com/?lang=fr\" class=\"\"> <i class=\"dm-icon fa fa-twitter fa-3x\"></i> </a>
              </div>
              <div class=\"servicetitle\">
                <h4>Twitter</h4>
              </div>
            </div>
          </div>
        </div>
        <div class=\"col-lg-3 col-md-3 col-sm-3\">
          <div class=\"servicebox text-center\">
            <div class=\"service-icon\">
              <div class=\"dm-icon-effect-1\" data-effect=\"slide-bottom\">
                <a href=\"https://plus.google.com\" class=\"\"> <i class=\"dm-icon fa fa-google-plus fa-3x\"></i> </a>
              </div>
              <div class=\"servicetitle\">
                <h4>Google Plus</h4>
              </div>
            </div>
          </div>
        </div>
        <div class=\"col-lg-3 col-md-3 col-sm-3\">
          <div class=\"servicebox text-center\">
            <div class=\"service-icon\">
              <div class=\"dm-icon-effect-1\" data-effect=\"slide-bottom\">
                <a href=\"https://www.youtube.com/\" class=\"\"> <i class=\"dm-icon fa fa-youtube fa-3x\"></i> </a>
              </div>
              <div class=\"servicetitle\">
                <h4>Youtube</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  {% endblock %}", "MonzaBundle:Default:contact.html.twig", "C:\\Monza\\src\\MonzaBundle/Resources/views/Default/contact.html.twig");
    }
}
